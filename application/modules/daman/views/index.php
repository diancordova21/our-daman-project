<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <meta name="description" content="">
  <meta name="author" content="">

  <title>Daman Revitalisasi</title>

  <!-- Custom fonts for this template-->
  <link href="<?= base_url()?>asset/vendor/fontawesome-free/css/all.min.css" rel="stylesheet" type="text/css">
  <link href="<?= base_url()?>https://fonts.googleapis.com/css?family=Nunito:200,200i,300,300i,400,400i,600,600i,700,700i,800,800i,900,900i" rel="stylesheet">
  <link rel="icon" href="<?= base_url()?>asset/img/icon-telkom.ico" type="image/x-icon">

  <!-- Custom styles for this template-->
  <link href="<?= base_url()?>asset/css/sb-admin-2.min.css" rel="stylesheet">

</head>

<body id="page-top">

  <!-- Page Wrapper -->
  <div id="wrapper">
  <?php include_once("partials/sidebar.php") ?>
    <!-- Content Wrapper -->
    <div id="content-wrapper" class="d-flex flex-column">

      <!-- Main Content -->
      <div id="content">
        <?php include_once("partials/topbar.php") ?>

        <!-- Begin Page Content -->
        <div class="container-fluid">

        <!-- Page Heading -->
        <div class="d-sm-flex align-items-center justify-content-between mb-4">
          <h1 class="h3 mb-0 text-gray-800">Dashboard</h1>
          <!-- <a href="#" class="d-none d-sm-inline-block btn btn-sm btn-primary shadow-sm"><i class="fas fa-download fa-sm text-white-50"></i> Generate Report</a> -->
        </div>

        <!-- Content Row -->
        <div class="row">

          <!-- Earnings (Monthly) Card Example -->
          <div class="col-xl-3 col-md-6 mb-4">
            <div class="card border-left-primary shadow h-100 py-2">
              <div class="card-body">
                <div class="row no-gutters align-items-center">
                  <div class="col mr-2">
                    <div class="text-xs font-weight-bold text-primary text-uppercase mb-1">Jumlah Data Masuk</div>
                    <div class="h5 mb-0 font-weight-bold text-gray-800"><?php echo $total_data?></div>
                  </div>
                  <div class="col-auto">
                    <i class="fas fa-database fa-2x text-gray-300"></i>
                  </div>
                </div>
              </div>
            </div>
          </div>

          <!-- Earnings (Monthly) Card Example -->
          <div class="col-xl-3 col-md-6 mb-4">
            <div class="card border-left-success shadow h-100 py-2">
              <div class="card-body">
                <div class="row no-gutters align-items-center">
                  <div class="col mr-2">
                    <div class="text-xs font-weight-bold text-success text-uppercase mb-1">Jumlah Sudah Update UIM</div>
                    <div class="h5 mb-0 font-weight-bold text-gray-800"><?php echo $total_update_uim?></div>
                  </div>
                  <div class="col-auto">
                    <i class="fas fa-clipboard-list fa-2x text-gray-300"></i>
                  </div>
                </div>
              </div>
            </div>
          </div>

          <!-- Earnings (Monthly) Card Example -->
          <div class="col-xl-3 col-md-6 mb-4">
            <div class="card border-left-info shadow h-100 py-2">
              <div class="card-body">
                <div class="row no-gutters align-items-center">
                  <div class="col mr-2">
                    <div class="text-xs font-weight-bold text-info text-uppercase mb-1">Jumlah Input Dava</div>
                    <div class="row no-gutters align-items-center">
                      <div class="col-auto">
                        <div class="h5 mb-0 mr-3 font-weight-bold text-gray-800"><?php echo $total_input_dava?></div>
                      </div>
                    </div>
                  </div>
                  <div class="col-auto">
                    <i class="fas fa-clipboard-list fa-2x text-gray-300"></i>
                  </div>
                </div>
              </div>
            </div>
          </div>

          <!-- Pending Requests Card Example -->
          <div class="col-xl-3 col-md-6 mb-4">
            <div class="card border-left-warning shadow h-100 py-2">
              <div class="card-body">
                <div class="row no-gutters align-items-center">
                  <div class="col mr-2">
                    <div class="text-xs font-weight-bold text-warning text-uppercase mb-1">Jumlah STO</div>
                    <div class="h5 mb-0 font-weight-bold text-gray-800">24</div>
                  </div>
                  <div class="col-auto">
                    <i class="fas fa-calendar fa-2x text-gray-300"></i>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>

        <!-- Content Row -->

        <div class="row">

      

            <div class="col-12">
                <div class="card shadow mb-4">
                  <!-- Card Header - Dropdown -->
                  <div class="card-header py-3 d-flex flex-row align-items-center justify-content-between">
                    <h6 class="m-0 font-weight-bold text-primary">Jumlah Kendala</h6>
                    <div class="dropdown no-arrow">
                      <a class="dropdown-toggle" href="#" role="button" id="dropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                        <i class="fas fa-ellipsis-v fa-sm fa-fw text-gray-400"></i>
                      </a>
                      <div class="dropdown-menu dropdown-menu-right shadow animated--fade-in" aria-labelledby="dropdownMenuLink">
                        <div class="dropdown-header">Dropdown Header:</div>
                        <a class="dropdown-item" href="#">Action</a>
                        <a class="dropdown-item" href="#">Another action</a>
                        <div class="dropdown-divider"></div>
                        <a class="dropdown-item" href="#">Something else here</a>
                      </div>
                    </div>
                  </div>
                  <!-- Card Body -->
                  <div class="card-body">
                    <div class="chart-pie pt-4 pb-2">
                      <canvas id="myPieChart"></canvas>
                    </div>
                    <div class="mt-4 text-center small">
                      <span class="mr-2">
                        <i class="fas fa-circle text-5"></i> Disconnect
                      </span>
                      <span class="mr-2">
                        <i class="fas fa-circle text-warning"></i> Redaman Tinggi
                      </span>
                      <span class="mr-2">
                        <i class="fas fa-circle text-secondary"></i> Service Stuck BI
                      </span>
                      <span class="mr-2">
                        <i class="fas fa-circle text-info"></i> Tidak Detek
                      </span>
                      <span class="mr-2">
                        <i class="fas fa-circle text-danger"></i> Splitter
                      </span>
                      <span class="mr-2">
                        <i class="fas fa-circle text-4"></i> Kapasitas
                      </span>
                      <span class="mr-2">
                        <i class="fas fa-circle text-1"></i> Tidak Bisa Create New Version
                      </span>
                      <span class="mr-2">
                        <i class="fas fa-circle text-2"></i> Service Suspend
                      </span>
                      <span class="mr-2">
                        <i class="fas fa-circle text-3"></i> Service Canceled
                      </span>
                      <span class="mr-2">
                        <i class="fas fa-circle text-7"></i> ONT Detect No Service
                      </span>
                      <span class="mr-2">
                        <i class="fas fa-circle text-8"></i> ODP Susah Di Jangkau
                      </span>
                      <!-- <div class="row">
                        <h4> Jumlah Total :</h4>
                      </div> -->
                    </div>
                  </div>
                </div>
              </div>
            </div>
          
          </div>
            

      
        </div>

      

        </div>
        <!-- /.container-fluid -->

      </div>
      <!-- End of Main Content -->
    </div>
    <!-- End of Content Wrapper -->

  </div>
  <!-- End of Page Wrapper -->

  <!-- Scroll to Top Button-->
  <a class="scroll-to-top rounded" href="<?= base_url()?>#page-top">
    <i class="fas fa-angle-up"></i>
  </a>

  <!-- Logout Modal-->
  <div class="modal fade" id="logoutModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title" id="exampleModalLabel">Ready to Leave?</h5>
          <button class="close" type="button" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">×</span>
          </button>
        </div>
        <div class="modal-body">Select "Logout" below if you are ready to end your current session.</div>
        <div class="modal-footer">
          <button class="btn btn-secondary" type="button" data-dismiss="modal">Cancel</button>
          <a class="btn btn-danger" href="<?= site_url('auth/admin/logout') ?>" class="item">Logout</a>
        </div>
      </div>
    </div>
  </div>
  
  <!-- Bootstrap core JavaScript-->
  <script src="<?= base_url()?>asset/vendor/jquery/jquery.min.js"></script>
  <script src="<?= base_url()?>asset/vendor/bootstrap/js/bootstrap.bundle.min.js"></script>

  <!-- Core plugin JavaScript-->
  <script src="<?= base_url()?>asset/vendor/jquery-easing/jquery.easing.min.js"></script>

  <!-- Custom scripts for all pages-->
  <script src="<?= base_url()?>asset/js/sb-admin-2.min.js"></script>

  <!-- Page level plugins -->
  <script src="<?= base_url()?>asset/vendor/chart.js/Chart.min.js"></script>

  <!-- Page level custom scripts -->
  <script src="<?= base_url()?>asset/js/demo/chart-area-demo.js"></script>
  <script>
    // Set new default font family and font color to mimic Bootstrap's default styling

    Chart.defaults.global.defaultFontFamily = 'Nunito', '-apple-system,system-ui,BlinkMacSystemFont,"Segoe UI",Roboto,"Helvetica Neue",Arial,sans-serif';
Chart.defaults.global.defaultFontColor = '#858796';

// Pie Chart Example
var ctx = document.getElementById("myPieChart");


var myPieChart = new Chart(ctx, {
  type: 'doughnut',
  data: {
    labels: ["Disconnect", "Redaman Tinggi", "Service Stuck BI", "Service Tidak Detek", "Splitter", "Kapasitas", "Tidak Bisa Create New Version","Service Suspend","Service Canceled","ONT Detect No Service","ODP Susah Di Jangkau"],
    datasets: [{
      data: [<?php echo @$total_disconnect.",".@$total_redaman_tinggi.",".@$total_stuck_bi.",".@$total_tidak_detek.",".@$total_splitter.",".@$total_kapasitas.",".@$total_tidak_bisa_create_new_version.",".@$total_service_suspend.",".@$total_service_canceled.",".@$total_ont_detect_no_service.",".@$total_odp_susah_di_jangkau.","; ?>],
      backgroundColor: ['#000000', '#ffc100', '#8b8a84','#36b9cc','#ec4700','#50e105','#107435','#e79417','#590b9c','#edd3bb','#b31004'],
      hoverBackgroundColor: ['#fffff', '#ffff', '#ffff', '#ffff', '#ffff','#ffff', '#ffff', '#ffff','#ffff','#ffff'],
      hoverBorderColor: "rgba(234, 236, 244, 1)",
    }],
  },
  options: {
    maintainAspectRatio: false,
    tooltips: {
      backgroundColor: "rgb(255,255,255)",
      bodyFontColor: "#858796",
      borderColor: '#dddfeb',
      borderWidth: 1,
      xPadding: 15,
      yPadding: 15,
      displayColors: false,
      caretPadding: 10,
    },
    legend: {
      display: false
    },
    cutoutPercentage: 80,
  },
  
});
ctx.onclick = function(e) {
   var slice = myPieChart.getElementAtEvent(e);
   if (!slice.length) return; // return if not clicked on slice
   var label = slice[0]._model.label;
   switch (label) {
      // add case for each label/slice
      case 'Disconnect':
         window.open('kendala/disconnect');
         break;
      case 'Redaman Tinggi':
         window.open('kendala/redti');
         break;
      case 'Service Stuck BI':
         window.open('kendala/stuckbi');
         break;
      case 'Service Tidak Detek':
         window.open('kendala/notdetek');
         break;
      case 'Splitter':
         window.open('kendala/splitter');
         break;
      case 'Tidak Bisa Create New Version':
         window.open('kendala/cantcreate');
         break;
      case 'Service Suspend':
         window.open('kendala/suspend');
         break;
      case 'Service Canceled':
         window.open('kendala/canceled');
         break;
      case 'ONT Detect No Service':
         window.open('kendala/noservice');
         break;
      case 'ODP Susah Di Jangkau':
         window.open('kendala/susahjangkau');
         break;
      case 'Kapasitas':
        window.open('kendala/kapasitas');
         break;
        
         
      // add rests ...
   }
}

  </script>

</body>

</html>