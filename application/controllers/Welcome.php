<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Welcome extends CI_Controller {

	public function __construct(){
		parent::__construct();
		$this->load->model('masterodp');
	}

	 function index()
	{
		// $data['pageName']= 'main-dashboard';
		$masterodp = $this->masterodp;
		// $data["jumlah_odp"] = $masterodp->jumlah_odp_persto();
		$data["total_data"] = $masterodp->jumlah_data();
		$data["total_input_dava"] = $masterodp->jumlah_input_dava();
		$data["total_sto"] = $masterodp->jumlah_sto();
		$data["total_update_uim"] = $masterodp->jumlah_update_uim();
		$data["total_disconnect"] = $masterodp->jumlah_disconnect();
		$data["total_tidak_detek"] = $masterodp->jumlah_tidak_detek();
		$data["total_stuck_bi"] = $masterodp->jumlah_stuck_bi();
		$data["total_odp_penuh"] = $masterodp->jumlah_odp_penuh();
		$data["total_node_b"] = $masterodp->jumlah_node_b();
		$data["total_ccan"] = $masterodp->jumlah_ccan();
		$data["total_wifi_id"] = $masterodp->jumlah_wifi_id();
		$data["total_astinet"] = $masterodp->jumlah_astinet();
		$data["total_vpn"] = $masterodp->jumlah_vpn();

		
		// print_r($data['total_node_b']);exit();
		


 		$this->load->view('index', $data);
	}

	

}
