<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Golive_Batu extends Modular {

	public function __construct(){
		authenticated();
        sessionAsAOM();
		parent::__construct();
		$this->load->model('Master_odp');
		
	}
	
	public function index()
	{	
		
		$this->load->helper('url');
		$Master_odp = $this->Master_odp;
		$data["jumlah_karangploso"] = $Master_odp->jumlah_karangploso();
		$data["jumlah_batu"] = $Master_odp->jumlah_batu ();
		$data["jumlah_ngantang"] = $Master_odp->jumlah_ngantang();
		

		$this->load->view('golive_batu', $data);
	}

	function sto($sto = null){

		$data["odp"] = $this->Master_odp->getBySTO($sto);
		$data["sto"] = $this->Master_odp->ambil_sto($sto);		

		$this->load->view('sto_lihat', $data);
	}
}