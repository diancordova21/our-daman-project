<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Kendala extends Modular {

	public function __construct(){
		authenticated();
        sessionAsDaman();
		parent::__construct();
		$this->load->model('Daman_models');
	}
	
	public function index()
	{	
		$this->load->helper('url');
		$Daman_models = $this->Daman_models;
		// $data["total_dampit"] = $Daman_models->jumlah_dampit();
		// $data["total_kepanjen"] = $Daman_models->jumlah_kepanjen();
		// $data["total_turen"] = $Daman_models->jumlah_turen();
		// $data["total_gondanglegi"] = $Daman_models->jumlah_gondanglegi();
		// $data["total_sumbermanjing"] = $Daman_models->jumlah_sumbermanjing();
		// $data["total_sumberpucung"] = $Daman_models->jumlah_sumberpucung();
		// $data["total_pagak"] = $Daman_models->jumlah_pagak();
		// $data["total_ampelgading"] = $Daman_models->jumlah_ampelgading();
		// $data["total_gunungkawi"] = $Daman_models->jumlah_gunungkawi();
		// $data["total_bantur"] = $Daman_models->jumlah_bantur();
		// // $data["total_donomulyo"] = $Daman_models->jumlah_donomulyo();
		// $data["odp"] = $this->Daman_models->getBySTO($trouble);
		// $data["trouble"] = $this->Daman_models->ambil_sto($trouble);
		// $data["sto"] = $this->Daman_models->ambil_disconnect();		

		
		$this->load->view('kendala_lihat');
	}

	function disconnect(){

		// $data["odp"] = $Daman_models->getByODP($id);
		$data["port"] = $this->Daman_models->ambil_trouble();		
		$data["trouble"] = $this->Daman_models->ambil_disconnect();		

		$this->load->view('kendala_lihat', $data);
	}

	function redti(){

		$data["port"] = $this->Daman_models->ambil_trouble();		
		$data["trouble"] = $this->Daman_models->ambil_redti();		

		$this->load->view('kendala_lihat', $data);
	}

	function stuckbi(){

		$data["port"] = $this->Daman_models->ambil_trouble();		
		$data["trouble"] = $this->Daman_models->ambil_stuckbi();		

		$this->load->view('kendala_lihat', $data);
	}

	function notdetek(){

		$data["port"] = $this->Daman_models->ambil_trouble();		
		$data["trouble"] = $this->Daman_models->ambil_notdetek();		

		$this->load->view('kendala_lihat', $data);
	}

	function splitter(){

		$data["port"] = $this->Daman_models->ambil_trouble();		
		$data["trouble"] = $this->Daman_models->ambil_splitter();		

		$this->load->view('kendala_lihat', $data);
	}

	function cantcreate(){

		$data["port"] = $this->Daman_models->ambil_trouble();		
		$data["trouble"] = $this->Daman_models->ambil_cantcreate();		

		$this->load->view('kendala_lihat', $data);
	}

	function suspend(){

		$data["port"] = $this->Daman_models->ambil_trouble();		
		$data["trouble"] = $this->Daman_models->ambil_suspend();		

		$this->load->view('kendala_lihat', $data);
	}

	function canceled(){

		$data["port"] = $this->Daman_models->ambil_trouble();		
		$data["trouble"] = $this->Daman_models->ambil_canceled();		

		$this->load->view('kendala_lihat', $data);
	}

	function noservice(){

		$data["port"] = $this->Daman_models->ambil_trouble();		
		$data["trouble"] = $this->Daman_models->ambil_noservice();		

		$this->load->view('kendala_lihat', $data);
	}

	function susahjangkau(){

		$data["port"] = $this->Daman_models->ambil_trouble();		
		$data["trouble"] = $this->Daman_models->ambil_susahjangkau();		

		$this->load->view('kendala_lihat', $data);
	}

	function kapasitas(){

		$data["port"] = $this->Daman_models->ambil_trouble();		
		$data["trouble"] = $this->Daman_models->ambil_kapasitas();		

		$this->load->view('kendala_lihat', $data);
	}

	
}