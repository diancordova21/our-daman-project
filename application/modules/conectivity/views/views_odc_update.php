<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <meta name="description" content="">
  <meta name="author" content="">

  <title>Update ODC</title>

  <!-- Custom fonts for this template-->
  <link href="<?= base_url()?>asset/vendor/fontawesome-free/css/all.min.css" rel="stylesheet" type="text/css">
  <link href="<?= base_url()?>https://fonts.googleapis.com/css?family=Nunito:200,200i,300,300i,400,400i,600,600i,700,700i,800,800i,900,900i" rel="stylesheet">
  <link rel="icon" href="<?= base_url()?>asset/img/icon-telkom.ico" type="image/x-icon">

  <!-- Custom styles for this template-->
  <link href="<?= base_url()?>asset/css/sb-admin-2.min.css" rel="stylesheet">

</head>

<body id="page-top">

  <!-- Page Wrapper -->
  <div id="wrapper">

    <!-- Content Wrapper -->
    <div id="content-wrapper" class="d-flex flex-column">

      <!-- Main Content -->
      <div id="content">
        <?php include_once("partials/topbar.php") ?>

        <!-- Begin Page Content -->
        <div class="container-fluid">
          
          <!-- DataTales Example -->
          <div class="card shadow mb-4">
                <div class="card-header py-3">
                  <h3 class="m-0 font-weight-bold text-primary">Update </h3>
                </div>
              <div class="card-body">
                <form role="form" method="post" action="<?= site_url('conectivity/odc/update/'. $this->uri->segment(4)) ?>">
                  <div class="row">
                    <div class="col-xl-6">
                      <div class="p-7">
                        <!-- <div class="container"> -->
                        <div class="form-group row">
                          <div class="col-sm-10 mb-3 mb-sm-0">
                            <h6>Dari Feeder</h6>
                            <input type="text" class="form-control form-control-user" name="fk_feeder" value="<?= $details_odc[0]->fk_feeder?>">
                          </div>
                        </div>
                        <div class="form-group row">
                          <div class="col-sm-10 mb-3 mb-sm-0">
                            <h6>Nama ODC</h6>
                            <input type="text" class="form-control form-control-user" name="nama_odc" value="<?= $details_odc[0]->nama_odc?>">
                          </div>
                        </div>
                        <div class="form-group row">
                          <div class="col-sm-10 mb-3 mb-sm-0">
                            <h6>Location</h6>
                            <input type="text" class="form-control form-control-user" name="nama_location_odc" value="<?= $details_odc[0]->nama_location_odc?>">
                          </div>
                        </div>
                        <div class="form-group row">
                          <div class="col-sm-10 mb-3 mb-sm-0">
                            <h6>Frame</h6>
                            <input type="text" class="form-control form-control-user" name="nama_frame_odc" value="<?= $details_odc[0]->nama_frame_odc?>">
                          </div>
                        </div>
                        <div class="form-group row">
                          <div class="col-sm-10 mb-3 mb-sm-0">
                            <h6>Panel in Panel/Port/Core</h6>
                            <input type="text" class="form-control form-control-user" name="panel_in_panel_port_core" value="<?= $details_odc[0]->panel_in_panel_port_core?>">
                          </div>
                        </div>
                        <div class="form-group row">
                          <div class="col-sm-10 mb-3 mb-sm-0">
                            <h6>Splitter</h6>
                            <input type="text" class="form-control form-control-user" name="splitter_name" value="<?= $details_odc[0]->splitter_name?>">
                          </div>
                        </div>
                        <div class="form-group row">
                          <div class="col-sm-10 mb-3 mb-sm-0">
                            <h6>Splitter Out</h6>
                            <input type="text" class="form-control form-control-user" name="splitter_out" value="<?= $details_odc[0]->splitter_out?>">
                          </div>
                        </div>
                        <div class="form-group row">
                          <div class="col-sm-10 mb-3 mb-sm-0">
                            <h6>Panel out Panel/Port/Core</h6>
                            <input type="text" class="form-control form-control-user" name="panel_out_panel_port_core" value="<?= $details_odc[0]->panel_out_panel_port_core?>">
                          </div>
                        </div>
                        <div class="form-group row">
                          <div class="col-sm-10 mb-3 mb-sm-0">
                            <h6>Kap</h6>
                            <input type="text" class="form-control form-control-user" name="kap" value="<?= $details_odc[0]->kap?>">
                          </div>
                        </div>
                        <div class="form-group row">
                          <div class="col-sm-10 mb-3 mb-sm-0">
                            <h6>Longitude</h6>
                            <input type="text" class="form-control form-control-user" name="kordinat_long" value="<?= $details_odc[0]->kordinat_long?>">
                          </div>
                        </div>
                        <div class="form-group row">
                          <div class="col-sm-10 mb-3 mb-sm-0">
                            <h6>Latitude</h6>
                            <input type="text" class="form-control form-control-user" name="kordinat_lang" value="<?= $details_odc[0]->kordinat_lang?>">
                          </div>
                        </div>
                      </div>
                      <button name="submit" type="submit" class="btn btn-google btn-user btn-block" style="margin-right:30%;margin-left:30%;margin-top:3%">Update</button>     
                    </div>
                  </div>
                </form>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
      <!-- End of Main Content -->
  </div>
    <!-- End of Content Wrapper -->

  </div>
  <!-- End of Page Wrapper -->

  <!-- Scroll to Top Button-->
  <a class="scroll-to-top rounded" href="<?= base_url()?>#page-top">
    <i class="fas fa-angle-up"></i>
  </a>

  <!-- Logout Modal-->
  <div class="modal fade" id="logoutModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title" id="exampleModalLabel">Ready to Leave?</h5>
          <button class="close" type="button" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">×</span>
          </button>
        </div>
        <div class="modal-body">Select "Logout" below if you are ready to end your current session.</div>
        <div class="modal-footer">
          <button class="btn btn-secondary" type="button" data-dismiss="modal">Cancel</button>
          <a class="btn btn-danger" href="<?= site_url('auth/admin/logout') ?>" class="item">Logout</a>
        </div>
      </div>
    </div>
  </div>



  <!-- Bootstrap core JavaScript-->
  <script src="<?= base_url()?>asset/vendor/jquery/jquery.min.js"></script>
  <script src="<?= base_url()?>asset/vendor/bootstrap/js/bootstrap.bundle.min.js"></script>

  <!-- Core plugin JavaScript-->
  <script src="<?= base_url()?>asset/vendor/jquery-easing/jquery.easing.min.js"></script>

  <!-- Custom scripts for all pages-->
  <script src="<?= base_url()?>asset/js/sb-admin-2.min.js"></script>
  <!-- Page level plugins -->
  <script src="<?= base_url()?>asset/vendor/datatables/jquery.dataTables.min.js"></script>
  <script src="<?= base_url()?>asset/vendor/datatables/dataTables.bootstrap4.min.js"></script>

  <!-- Page level custom scripts -->
  <script src="<?= base_url()?>asset/js/demo/datatables-demo.js"></script>
            
      <script type="text/javascript">
            $(document).ready(function() {
                $('#odp-table').DataTable({
                    
                    "ajax": {
                        url : "<?php echo site_url("table/odp_page") ?>",
                        type : 'GET'
                    },
                });
            });
            </script>

</body>

</html>