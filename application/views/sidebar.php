<!-- Sidebar -->
<ul class="navbar-nav bg-gradient-primary sidebar sidebar-dark accordion" id="accordionSidebar">

<!-- Sidebar - Brand -->
<a class="sidebar-brand d-flex align-items-center justify-content-center" href="<?= base_url()?>index.php">
  <div class="sidebar-brand-icon ">
    <!-- <i class="fas fa-laugh-wink"></i> -->
    <img src="<?= base_url()?>asset/img/logo.png" style="height:auto;width:60px" alt="">
  </div>
  <div class="sidebar-brand-text mx-3">REVITALISASI DATA</div>
</a>

<!-- Divider -->
<hr class="sidebar-divider my-0">

<!-- Nav Item - Dashboard -->
<li class="nav-item active">
  <a class="nav-link" href="<?= base_url()?>index.php">
    <i class="fas fa-fw fa-tachometer-alt"></i>
    <span>Dashboard</span></a>
</li>

<!-- Divider -->
<hr class="sidebar-divider">

<!-- Heading -->
<div class="sidebar-heading">
  DATA ODP
</div>

<!-- Nav Item - Pages Collapse Menu
<li class="nav-item">
  <a class="nav-link collapsed" href="<?= base_url()?>#" data-toggle="collapse" data-target="#collapseTwo" aria-expanded="true" aria-controls="collapseTwo">
    <i class="fas fa-fw fa-cog"></i>
    <span>Components</span>
  </a>
  <div id="collapseTwo" class="collapse" aria-labelledby="headingTwo" data-parent="#accordionSidebar">
    <div class="bg-white py-2 collapse-inner rounded">
      <h6 class="collapse-header">Custom Components:</h6>
      <a class="collapse-item" href="<?= base_url()?>buttons.html">Buttons</a>
      <a class="collapse-item" href="<?= base_url()?>cards.html">Cards</a>
    </div>
  </div>
</li> -->

<!-- Nav Item - ODP -->
<li class="nav-item">
  <a class="nav-link" href="<?= base_url()?>index.php/table">
    <i class="fas fa-fw fa-table"></i>
    <span>Data Revitalisasi</span></a>
</li>

<!-- Datel Menu -->
<li class="nav-item">
  <a class="nav-link collapsed" href="<?= base_url()?>#" data-toggle="collapse" data-target="#collapseUtilities" aria-expanded="true" aria-controls="collapseUtilities">
    <i class="fas fa-fw fa-chart-area"></i>
    <span>Datel</span>
  </a>
  <div id="collapseUtilities" class="collapse" aria-labelledby="headingUtilities" data-parent="#accordionSidebar">
    <div class="bg-white py-2 collapse-inner rounded">
      <h6 class="collapse-header">Daftar Datel:</h6>
      <a class="collapse-item" href="<?= base_url()?>index.php/malang">Malang</a>
      <a class="collapse-item" href="<?= base_url()?>index.php/kepanjen">Kepanjen</a>
      <a class="collapse-item" href="<?= base_url()?>index.php/batu">Batu</a>
    </div>
  </div>
</li>

<!-- Divider
<hr class="sidebar-divider"> -->

<!-- Heading
<div class="sidebar-heading">
  Addons
</div> -->

<!-- Nav Item - Pages Collapse Menu
<li class="nav-item">
  <a class="nav-link collapsed" href="<?= base_url()?>#" data-toggle="collapse" data-target="#collapsePages" aria-expanded="true" aria-controls="collapsePages">
    <i class="fas fa-fw fa-folder"></i>
    <span>Pages</span>
  </a>
  <div id="collapsePages" class="collapse" aria-labelledby="headingPages" data-parent="#accordionSidebar">
    <div class="bg-white py-2 collapse-inner rounded">
      <h6 class="collapse-header">Login Screens:</h6>
      <a class="collapse-item" href="<?= base_url()?>login.html">Login</a>
      <a class="collapse-item" href="<?= base_url()?>register.html">Register</a>
      <a class="collapse-item" href="<?= base_url()?>forgot-password.html">Forgot Password</a>
      <div class="collapse-divider"></div>
      <h6 class="collapse-header">Other Pages:</h6>
      <a class="collapse-item" href="<?= base_url()?>404.html">404 Page</a>
      <a class="collapse-item" href="<?= base_url()?>blank.html">Blank Page</a>
    </div>
  </div>
</li> -->

<!-- Nav Item - Charts
<li class="nav-item">
  <a class="nav-link" href="<?= base_url()?>charts.html">
    <i class="fas fa-fw fa-chart-area"></i>
    <span>Charts</span></a>
</li> -->



<!-- Divider -->
<hr class="sidebar-divider d-none d-md-block">

<!-- Sidebar Toggler (Sidebar) -->
<div class="text-center d-none d-md-inline">
  <button class="rounded-circle border-0" id="sidebarToggle"></button>
</div>

</ul>
<!-- End of Sidebar -->