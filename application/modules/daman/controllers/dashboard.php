<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Dashboard extends Modular {

	public function __construct(){
        authenticated();
        sessionAsDaman();
		parent::__construct();
		$this->load->model('Daman_models');
	}

	 function index()
	{
		// $data['pageName']= 'main-dashboard';
		$masterodp = $this->Daman_models;
		// $data["jumlah_odp"] = $masterodp->jumlah_odp_persto();
		$data["total_data"] = $masterodp->jumlah_data();
		$data["total_input_dava"] = $masterodp->jumlah_input_dava();
		$data["total_sto"] = $masterodp->jumlah_sto();
		$data["total_update_uim"] = $masterodp->jumlah_update_uim();
		$data["total_disconnect"] = $masterodp->jumlah_disconnect();
		$data["total_redaman_tinggi"] = $masterodp->jumlah_redaman_tinggi();
		$data["total_stuck_bi"] = $masterodp->jumlah_stuck_bi();
		$data["total_tidak_detek"] = $masterodp->jumlah_tidak_detek();
		$data["total_splitter"] = $masterodp->jumlah_splitter();
		$data["total_kapasitas"] = $masterodp->jumlah_kapasitas();
		$data["total_tidak_bisa_create_new_version"] = $masterodp->jumlah_tidak_bisa_create_new_version();
		$data["total_service_suspend"] = $masterodp->jumlah_service_suspend();
		$data["total_service_canceled"] = $masterodp->jumlah_service_canceled();
		$data["total_ont_detect_no_service"] = $masterodp->jumlah_ont_detect_no_service();
		$data["total_odp_susah_di_jangkau"] = $masterodp->jumlah_odp_susah_di_jangkau();

		
		// print_r($data['total_node_b']);exit();
		


 		$this->load->view('index', $data);
	}

	

}
