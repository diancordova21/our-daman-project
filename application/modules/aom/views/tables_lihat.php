<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <meta name="description" content="">
  <meta name="author" content="">

  <title>Master ODP</title>

  <!-- Custom fonts for this template-->
  <link href="<?= base_url()?>asset/vendor/fontawesome-free/css/all.min.css" rel="stylesheet" type="text/css">
  <link href="<?= base_url()?>https://fonts.googleapis.com/css?family=Nunito:200,200i,300,300i,400,400i,600,600i,700,700i,800,800i,900,900i" rel="stylesheet">
  <link rel="icon" href="<?= base_url()?>asset/img/icon-telkom.ico" type="image/x-icon">

  <!-- Custom styles for this template-->
  <link href="<?= base_url()?>asset/css/sb-admin-2.min.css" rel="stylesheet">

</head>

<body id="page-top">

  <!-- Page Wrapper -->
  <div id="wrapper">
    <?php include_once("sidebar.php") ?>
    <!-- Content Wrapper -->
    <div id="content-wrapper" class="d-flex flex-column"> 

      <!-- Main Content -->
      <div id="content">
        <?php include_once("topbar.php") ?>

        <!-- Begin Page Content -->
        <div class="container-fluid">
          
          <!-- DataTales Example -->
          <div class="card shadow mb-4">
            <div class="card-header py-3">
              <h3 class="m-0 font-weight-bold text-primary">ODP: <?php echo $masterodp->odp ?></h3>
            </div>
            <div class="card-body">
            <form>

              <div class="row">
                <div class="col">

                <div class="card">            
                  <div class="card-body">
                    <div class="row">
                      <div class="col">
                        <p>Datel: </p>
                        <p>STO: </p>
                        <p>Latitude: </p>
                        <p>Longitude: </p>
                        <p>Alamat: </p>
                        <p>Available: </p>
                        <p>Reserved: </p>
                        <p>In Service: </p>
                        <p>Total: </p>
                        <p>Tanggal GoLive: </p>
                        <p>Bulan GoLive: </p>
                      </div>

                      <div class="col">
                        <p><?php echo $masterodp->datel ?></p>
                        <p><?php echo $masterodp->sto ?></p>
                        <p><?php echo $masterodp->latitude ?></p>
                        <p><?php echo $masterodp->longitude ?></p>
                        <p><?php echo $masterodp->alamat ?></p>
                        <p><?php echo $masterodp->avai ?></p>
                        <p><?php echo $masterodp->reserved ?></p>
                        <p><?php echo $masterodp->in_service ?></p>
                        <p><?php echo $masterodp->total ?></p>
                        <p><?php echo $masterodp->tanggal_golive ?></p>
                        <p><?php echo $masterodp->bulan ?></p>
                      </div>
                    </div>
                  </div>
                </div>

                
                </div>
                <div class="col">
                
                  <div class="card">            
                    <div class="card-body">
                      <div class="row">
                        <div class="col">
                          <p>Tahun GoLive: </p>
                          <p>Kelurahan: </p>
                          <p>Kecamatan: </p>
                          <p>Kota/Kab: </p>
                          <p>QR Code ODP: </p>
                          <p>QR Code Port: </p>
                          <p>Project: </p>
                          <p>Vendor: </p>
                          <p>Merk: </p>
                          <p>IP: </p>
                        </div>

                        <div class="col">
                          <p><?php echo $masterodp->tahun ?></p>
                          <p><?php echo $masterodp->kelurahan ?></p>
                          <p><?php echo $masterodp->kecamatan ?></p>
                          <p><?php echo $masterodp->kota_kab ?></p>
                          <p><?php echo $masterodp->qrcode_odp ?></p>
                          <p><?php echo $masterodp->qrcode_port ?></p>
                          <p><?php echo $masterodp->project ?></p>
                          <p><?php echo $masterodp->vendor ?></p>
                          <p><?php echo $masterodp->merk ?></p>
                          <p><?php echo $masterodp->ip ?></p>
                        </div>
                      </div>
                    </div>
                  </div>



                </div>
              </div>

              
              

            </form>

            </div>
          </div>
        </div>


      </div>
      <!-- End of Main Content -->

    </div>
    <!-- End of Content Wrapper -->

  </div>
  <!-- End of Page Wrapper -->

  <!-- Scroll to Top Button-->
  <a class="scroll-to-top rounded" href="<?= base_url()?>#page-top">
    <i class="fas fa-angle-up"></i>
  </a>

  <!-- Logout Modal-->
  <div class="modal fade" id="logoutModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title" id="exampleModalLabel">Ready to Leave?</h5>
          <button class="close" type="button" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">×</span>
          </button>
        </div>
        <div class="modal-body">Select "Logout" below if you are ready to end your current session.</div>
        <div class="modal-footer">
          <button class="btn btn-secondary" type="button" data-dismiss="modal">Cancel</button>
          <a class="btn btn-danger" href="<?= site_url('auth/admin/logout') ?>" class="item">Logout</a>
        </div>
      </div>
    </div>
  </div>



  <!-- Bootstrap core JavaScript-->
  <script src="<?= base_url()?>asset/vendor/jquery/jquery.min.js"></script>
  <script src="<?= base_url()?>asset/vendor/bootstrap/js/bootstrap.bundle.min.js"></script>

  <!-- Core plugin JavaScript-->
  <script src="<?= base_url()?>asset/vendor/jquery-easing/jquery.easing.min.js"></script>

  <!-- Custom scripts for all pages-->
  <script src="<?= base_url()?>asset/js/sb-admin-2.min.js"></script>
  <!-- Page level plugins -->
  <script src="<?= base_url()?>asset/vendor/datatables/jquery.dataTables.min.js"></script>
  <script src="<?= base_url()?>asset/vendor/datatables/dataTables.bootstrap4.min.js"></script>

  <!-- Page level custom scripts -->
  <script src="<?= base_url()?>asset/js/demo/datatables-demo.js"></script>
            
      <script type="text/javascript">
            $(document).ready(function() {
                $('#odp-table').DataTable({
                    
                    "ajax": {
                        url : "<?php echo site_url("table/odp_page") ?>",
                        type : 'GET'
                    },
                });
            });
            </script>

</body>

</html>