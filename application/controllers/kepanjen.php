<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class kepanjen extends CI_Controller {

	public function __construct(){
		parent::__construct();
		$this->load->model('masterodp');
	}
	
	public function index()
	{	
		$this->load->helper('url');
		$masterodp = $this->masterodp;
		$data["total_dampit"] = $masterodp->jumlah_dampit();
		$data["total_kepanjen"] = $masterodp->jumlah_kepanjen();
		$data["total_turen"] = $masterodp->jumlah_turen();
		$data["total_gondanglegi"] = $masterodp->jumlah_gondanglegi();
		$data["total_sumbermanjing"] = $masterodp->jumlah_sumbermanjing();
		$data["total_sumberpucung"] = $masterodp->jumlah_sumberpucung();
		$data["total_pagak"] = $masterodp->jumlah_pagak();
		$data["total_ampelgading"] = $masterodp->jumlah_ampelgading();
		$data["total_gunungkawi"] = $masterodp->jumlah_gunungkawi();
		$data["total_bantur"] = $masterodp->jumlah_bantur();
		$data["total_donomulyo"] = $masterodp->jumlah_donomulyo();
		

		$this->load->view('datel_kepanjen', $data);
	}

	function sto($sto = null){

		$data["odp"] = $this->masterodp->getBySTO($sto);
		$data["sto"] = $this->masterodp->ambil_sto($sto);		

		$this->load->view('sto_lihat', $data);
	}
}