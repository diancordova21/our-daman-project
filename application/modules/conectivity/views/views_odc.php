<!DOCTYPE html>
<html lang="en">

<head>
  <?php include_once("partials/head.php"); ?>
  <link href="<?= base_url()?>asset/vendor/dist/snackbar.min.css" rel="stylesheet">
</head>

<body id="page-top">

  <!-- Page Wrapper -->
  <div id="wrapper">
    <!-- Content Wrapper -->
    <div id="content-wrapper" class="d-flex flex-column">

      <!-- Main Content -->
      <div id="content">
        <?php include_once("partials/topbar.php") ?>

        <!-- Begin Page Content -->
        <div class="container-fluid">

          <!-- Page Heading -->
          <h1 class="h3 mb-2 text-gray-800">Data ODC</h1>

          <!-- DataTales Example -->
          <div class="card shadow mb-4">
            <div class="card-header py-3">
              <div class="row">
                <div class="col-lg-6 col-md-6">
                  <h6 class="m-0 font-weight-bold text-primary"></h6>
                </div>
                <div class="col-lg-6 col-md-6">
                  <button type="button" class="btn btn-success btn-user" style="margin-left:75%" data-toggle="modal" data-target="#exampleModalLong">Tambah</button>
                </div>
              </div>
            </div>
            <div class="card-body">
              <div class="table-responsive">
                <table id="helo" class="table table-bordered table-hover" width="100%" cellspacing="0">
                  <thead>
                    <tr>
                      <th>Dari Feeder</th>
                      <th>Nama</th>
                      <th>Location</th>
                      <th>Frame</th>
                      <th>Panel in Panel/Port/Core</th>
                      <th>Splitter</th>
                      <th>Splitter Out</th>
                      <th>Panel out Panel/Port/Core</th>
                      <th>Kap</th>
                      <th>Longitude</th>
                      <th>Latitude</th>
                      <th>Aksi</th>
                    </tr>
                  </thead>
                  <tbody>
                    <?php foreach ($all_odc as $data) : ?>
                      <tr>
                        <td><a href="<?= base_url('conectivity/feeder/lihat/'.$data->fk_feeder)?>"><?= $data->nama_kabel_feeder ?></a></td>
                        <td><?= $data->nama_odc ?></td>
                        <td><?= $data->nama_location_odc ?></td>
                        <td><?= $data->nama_frame_odc ?></td>
                        <td><?= $data->panel_in_panel_port_core ?></td>
                        <td><?= $data->splitter_name ?></td>
                        <td><?= $data->splitter_out ?></td>
                        <td><?= $data->panel_out_panel_port_core ?></td>
                        <td><?= $data->kap ?></td>
                        <td><?= $data->kordinat_long ?></td>
                        <td><?= $data->kordinat_lang ?></td>
                        <td>
                        <div class="ui buttons mini">
                            <a class="btn btn-sm btn-success" href="<?= site_url('conectivity/odc/lihat/' . $data->id_odc) ?>" title="Lihat"><i class="fa fa-eye" aria-hidden="true"></i> Lihat</a>
                            <a class="btn btn-sm btn-warning" href="<?= site_url('conectivity/odc/update/' . $data->id_odc) ?>" title="Update "><i class="fa fa-pen" aria-hidden="true"></i> Update</a>
                            <a class="btn btn-sm btn-danger" href="<?= site_url('conectivity/odc/delete/' . $data->id_odc) ?>" title="hapus" onclick="return confirm();"> <i class="fa fa-trash" aria-hidden="true"></i>Hapus</a>
                        </div>
                        </td>
                      </tr>
                    <?php endforeach; ?>
                  </tbody>
                </table>
              </div>
            </div>
          </div>
        </div>


      </div>
      <!-- End of Main Content -->

    </div>
    <!-- End of Content Wrapper -->

  </div>
  <!-- End of Page Wrapper -->

  <!-- Scroll to Top Button-->
  <a class="scroll-to-top rounded" href="<?= base_url()?>#page-top">
    <i class="fas fa-angle-up"></i>
  </a>
                  
  <!-- Modal Create  -->
  <form method="post" action="<?= site_url('conectivity/odc/create/') ?>" enctype="multipart/form-data"> 
    <div class="row">
    <!-- Modal -->
    <div class="modal fade" id="exampleModalLong" tabindex="-1" role="dialog" aria-labelledby="exampleModalLongTitle" aria-hidden="true">
      <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
          <div class="modal-header">
            <h5 class="modal-title" id="exampleModalLongTitle">Tambah Feeder</h5>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
              <span aria-hidden="true">&times;</span>
            </button>
          </div>
          <div class="modal-body">
            <div class="row">
              <div class="col-md-8">
                  <label>Dari Feeder</label>
              </div>
              <div class="col-md-8">
                <select class="ui fluid search dropdown" name="fk_feeder">
                  <option value="0">-- Pilih FTM--</option>
                  <?php foreach ($all_feeder as $list) : ?>
                      <option value="<?= $list->id_feeder ?>"><?= $list->nama_kabel_feeder ?></option>
                  <?php endforeach ?>
                </select>
              </div>
            </div>
            <div class="row">
                  <div class="col-md-8">
                      <label>Nama ODC</label>
                      <input type="text" class="form-control form-control-user" name="nama_odc">
                 </div>
            </div>
            <div class="row">
                  <div class="col-md-8">
                      <label>Location</label>
                      <input type="text" class="form-control form-control-user" name="nama_location_odc">
                 </div>
            </div>
            <div class="row">
                  <div class="col-md-8">
                      <label>Frame</label>
                      <input type="text" class="form-control form-control-user" name="nama_frame_odc">
                 </div>
            </div>
            <div class="row">
                  <div class="col-md-8">
                      <label>Panel In Panel/Port/Core</label>
                      <input type="text" class="form-control form-control-user" name="panel_in_panel_port_core">
                 </div>
            </div>
            <div class="row">
                  <div class="col-md-8">
                      <label>Splitter</label>
                      <input type="text" class="form-control form-control-user" name="splitter_name">
                 </div>
            </div>
            <div class="row">
                  <div class="col-md-8">
                      <label>Splitter Out</label>
                      <input type="text" class="form-control form-control-user" name="splitter_out">
                 </div>
            </div>
            <div class="row">
                  <div class="col-md-8">
                      <label>Panel Out Panel/Out/Core</label>
                      <input type="text" class="form-control form-control-user" name="panel_out_panel_port_core">
                 </div>
            </div>
            <div class="row">
                  <div class="col-md-8">
                      <label>Kap</label>
                      <input type="text" class="form-control form-control-user" name="kap">
                 </div>
            </div>
            <div class="row">
                  <div class="col-md-8">
                      <label>Longitude</label>
                      <input type="text" class="form-control form-control-user" name="kordinat_long">
                 </div>
            </div>
            <div class="row">
                  <div class="col-md-8">
                      <label>Latitude</label>
                      <input type="text" class="form-control form-control-user" name="kordinat_lang">
                 </div>
            </div>
            <div class="row">
                  <div class="col-md-4">
                    <br>
                    <button name="submit" type="submit" class="btn btn-success btn-user">Submit</button>
                </div>
            </div>
          </div>
          </form>
        </div>
      </div>
    </div>   
    
    </div>
  </form>
  <!-- End Modal -->
  <!-- Logout Modal-->
  <div class="modal fade" id="logoutModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title" id="exampleModalLabel">Ready to Leave?</h5>
          <button class="close" type="button" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">×</span>
          </button>
        </div>
        <div class="modal-body">Select "Logout" below if you are ready to end your current session.</div>
        <div class="modal-footer">
          <button class="btn btn-secondary" type="button" data-dismiss="modal">Cancel</button>
          <a class="btn btn-danger" href="<?= site_url('auth/admin/logout') ?>" class="item">Logout</a>
        </div>
      </div>
    </div>
  </div>

  <!-- Modal Import -->
  


  <!-- Bootstrap core JavaScript-->
  <script src="<?= base_url()?>asset/vendor/dist/snackbar.min.js"></script>
  <script src="<?= base_url()?>asset/vendor/jquery/jquery.min.js"></script>
  <script src="<?= base_url()?>asset/vendor/bootstrap/js/bootstrap.bundle.min.js"></script>

  <!-- Core plugin JavaScript-->
  <script src="<?= base_url()?>asset/vendor/jquery-easing/jquery.easing.min.js"></script>

  <!-- Custom scripts for all pages-->
  <script src="<?= base_url()?>asset/js/sb-admin-2.min.js"></script>
  <!-- Page level plugins -->
  <script src="<?= base_url()?>asset/vendor/datatables/jquery.dataTables.min.js"></script>
  <script src="<?= base_url()?>asset/vendor/datatables/dataTables.bootstrap4.min.js"></script>

  <!-- Page level custom scripts -->
  <script src="<?= base_url()?>asset/js/demo/datatables-demo.js"></script>
            
  <script type="text/javascript">
          $(document).ready(function() {
              $('#helo').DataTable();
          });
          $(".custom-file-input").on("change", function() {
            var fileName = $(this).val().split("\\").pop();
            $(this).siblings(".custom-file-label").addClass("selected").html(fileName);
          });

          $("#submit").on("click", function() {
            document.getElementById("loadergif").style.display = "block";
          });
          // $('#tes').click(function() {
          //    Snackbar.show({text: 'Example notification text.'});
          // });
          // function confirmation(){
          //     var result = confirm(\'Anda yakin untuk menghapus data ini?\');
          //     if(result){
          //         // Delete logic goes here
          //     }
          // }
      </script>

</body>

</html>



