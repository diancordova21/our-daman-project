<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Feeder extends Modular
{
    public function __construct(){
        authenticated();
        sessionAsRivara();
		parent::__construct();
		$this->load->library('form_validation'); 
		$this->load->model('Conectivity_models');
    }
    
    public function index()
	{	
		$this->load->helper('url');
		$data['all_ftm'] =$this->Conectivity_models->get_ftm();
        $data['all_feeder'] =$this->Conectivity_models->get_feeder();
		$this->load->view('views_feeder', $data);
    }
    
    function create(){
		$this->form_validation->set_rules('fk_ftm','Nama FTM','required');
		$this->form_validation->set_rules('nama_feeder','Nama Feeder','required');

        if (isset($_POST['submit'])) {
			if ($this->form_validation->run() != false){
				$this->Conectivity_models->create_feeder();
				redirect('conectivity/feeder');
			}			
			else{
				redirect('conectivity/feeder');
			}
		} 
		else {
			redirect('conectivity/feeder');
		}
	}

	public function lihat($id)
	{
		$data['details_feeder']=$this->Conectivity_models->get_feeder_detail($id);
		$this->load->view('views_feeder_lihat', $data);
		
	}

	//Update
	function update($id = null){
		if (isset($_POST['submit'])){
			$this->Conectivity_models->UpdateByIdFeeder($id);
			redirect('conectivity/feeder');
		}
	 	else {
		if (!isset($id)) redirect('conectivity/feeder');
		
		$data['details_feeder']=$this->Conectivity_models->get_feeder_detail($id);
		$this->load->view('views_feeder_update', $data);
	 	}
	}

	public function delete($id = null)
	{
		if (!isset($id)) show_404();

	    if ($this->Conectivity_models->hapus_feeder($id)) {
		    redirect(site_url('conectivity/feeder'));
	    }
	}
}