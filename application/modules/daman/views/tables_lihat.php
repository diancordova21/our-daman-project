<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <meta name="description" content="">
  <meta name="author" content="">

  <title>Tambah ODP</title>

  <!-- Custom fonts for this template-->
  <link href="<?= base_url()?>asset/vendor/fontawesome-free/css/all.min.css" rel="stylesheet" type="text/css">
  <link href="<?= base_url()?>https://fonts.googleapis.com/css?family=Nunito:200,200i,300,300i,400,400i,600,600i,700,700i,800,800i,900,900i" rel="stylesheet">
  <link rel="icon" href="<?= base_url()?>asset/img/icon-telkom.ico" type="image/x-icon">
  
  <!-- Custom styles for this template-->
  <link href="<?= base_url()?>asset/css/sb-admin-2.min.css" rel="stylesheet">
  

</head>

<body id="page-top">

  <!-- Page Wrapper -->
  <div id="wrapper">
    <?php include_once("partials/sidebar.php") ?>
    <!-- Content Wrapper -->
    <div id="content-wrapper" class="d-flex flex-column">

      <!-- Main Content -->
      <div id="content">
        <?php include_once("partials/topbar.php") ?>

        <!-- Begin Page Content -->
        <div class="container">         
        
          <div class="card shadow mb-4">
            <div class="card-header py-3">
              <h3 class="m-0 font-weight-bold text-primary">ODP: <?= $odp[0]->odp_name?></h3>
            </div>
            <div class="card-body">
            <form>

                <div class="col">
                
                  <div class="card">            
                    <div class="card-body">
                      <div class="row">
                        <div class="col">
                          <p>Tanggal Revitalisasi</p>
                          <p>Tanggal Update</p>
                          <p>IP Address </p>
                          <p>Slot Port </p>
                          <!-- <p>Status </p>
                          <p>Inet Voice Customer </p>
                          <p>QR Code </p>
                          <p>Description </p>
                          <p>Update UIM </p>
                          <p>Input Dava </p>
                          <p>DC Dismantled </p>
                          <p>Trouble </p> -->

                        </div>
                        <div>
                          <div class="col" style>
                          <p>:</p>
                          <p>:</p>
                          <p>:</p>
                          <p>:</p>
                          <!-- <p>:</p>
                          <p>:</p>
                          <p>:</p>
                          <p>:</p>
                          <p>:</p>
                          <p>:</p>
                          <p>:</p>
                          <p>:</p> -->
                          
                          
                          </div>
                        </div>

                        <div class="col">
                          <p><?= $odp[0]->date ?></p>
                          <p><?= $odp[0]->date ?></p>
                          <p><?= $odp[0]->ip_address ?></p>
                          <p><?= $odp[0]->slot_port ?></p>
                         

                        </div>
                      </div>
                    </div>
                  </div>



                </div>
              </div>

              
              

            </form>

            </div>
          </div>
          </div>


          <div class="container"> 
          <div class="card shadow mb-4">
              <div class="card-header py-3">
                  <h3 class="m-0 font-weight-bold text-primary">Table Port</h3>
                  <!-- <button name="submit" type="submit" class="btn btn-success btn-user" style="margin-left:75%">Tambah</button> -->
                  <button type="button" class="btn btn-success btn-user" style="margin-left:75%" data-toggle="modal" data-target="#exampleModalLong">Tambah</button>
              </div>
              <div class="card-body">
                  <div class="table-responsive">
                    <table id="odp-table" class="table table-bordered table-hover" width="100%" cellspacing="0">
                      <thead>
                        <tr>
                          <th>No.Port</th>
                          <th>Status</th>
                          <th>No Inet / Voice</th>
                          <th>QR Code</th>
                          <th>Keterangan</th>
                          <th>Update UIM</th>
                          <th>Input Dava</th>
                          <th>Port UIM</th>
                          <th>DC Dismantled</th>
                          <th>Kendala</th>
                          <th>Aksi</th>
                        </tr>
                      </thead>
                      <tbody>
                        <?php foreach (@$port_odp as $data) : ?>
                            <tr>
                                <td><?= $data->no_port ?></td>
                                <td><?= $data->status ?></td>
                                <td><?= $data->inet_voice_customer ?></td>
                                <td><?= $data->qr_code ?></td>
                                <td><?= $data->description ?></td>
                                <td><?= $data->update_uim ?></td>
                                <td><?= $data->input_dava ?></td>
                                <td><?= $data->port_uim ?></td>
                                <td><?= $data->dc_dismantled ?></td>
                                <td><?= $data->trouble ?></td>
                                <td>
                                <div class="ui buttons mini">
                                    <!-- <a class="btn btn-sm btn-success" href="<// site_url('daman/table/lihat/' . $data->id_port) ?>" title="Lihat"><i class="glyphicon glyphicon-folder"></i> Lihat</a> -->
                                    <a class="btn btn-sm btn-warning" href="<?= site_url('daman/table/update/' . $data->fk_odp) ?>" title="Lihat"><i class="fa fa-pen" aria-hidden="true"></i>Update</a>
                                    <button class="btn btn-sm btn-danger" onclick="return confirm(\'Anda yakin untuk menghapus data ini?\');"><i class="fa fa-trash" aria-hidden="true"></i>Hapus</button>
                                </div>
                                </td>
                            </tr>
                        <?php endforeach; ?> 
                      </tbody>
                    </table>
                  </div>
              </div>


             
                <form method="post" action="<?= site_url('daman/table/tambah_port/'.$this->uri->segment(4)) ?>" enctype="multipart/form-data"> 
                  <div class="row">
                  <!-- Modal -->
                  <div class="modal fade" id="exampleModalLong" tabindex="-1" role="dialog" aria-labelledby="exampleModalLongTitle" aria-hidden="true">
                    <div class="modal-dialog modal-lg" role="document">
                      <div class="modal-content">
                        <div class="modal-header">
                          <h5 class="modal-title" id="exampleModalLongTitle">Tambah Port ODP </h5>
                          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                          </button>
                        </div>
                        <div class="modal-body">
                          <div class="row">
                            <!-- mulai -->
                                <div class="col-md-4">
                                  <input type="hidden" class="form-control form-control-user" name= "id_odp" value="<?= @$odp[0]->id_revitalisasi_odp ?>" readonly="readonly">

                                    <label>Nama ODP</label>
                                    <input type="text" class="form-control form-control-user" value="<?= $odp[0]->odp_name?>" readonly="readonly">
                                </div>
                                <div class="col-md-4">
                                    <label>No. Port</label>
                                    <input type="text" class="form-control form-control-user" name="no_port">
                                </div>
                                <div class="col-md-4">
                                    <label for="status">Status</label>
                                    <select class="form-control" id="status" name="status">
                                      <option value="">-</option>
                                      <option value="ON">ON</option>
                                      <option value="OFF">OFF</option>
                                      <option value="Rusak">Rusak</option>
                                      <option value="Free">Free</option>
                                      <option value="Passive Splitter">Passive Splitter</option>
                                    </select>
                                </div>
                          </div>
                          <div class="row">
                                <div class="col-md-4">
                                    <label for="ownership">Ownership</label>
                                    <select class="form-control" id="ownership" name="ownership">
                                      <option value="">-</option>
                                      <option value="CCAN VPN">CCAN VPN</option>
                                      <option value="CCAN Astinet">CCAN Astinet</option>
                                      <option value="Node B">Node B</option>
                                      <option value="CS/Retail">CS/Retail</option>
                                      <option value="Wifi ID">Wifi ID</option>
                                    </select>
                                </div>
                                <div class="col-md-4">
                                    <label>No Inet / Voice</label>
                                    <input type="text" class="form-control form-control-user" name="inet_voice_customer">
                                </div>
                                <div class="col-md-4">
                                    <label>QR Code</label>
                                    <input type="text" class="form-control form-control-user" name="qr_code">
                                </div>
                          </div>
                          <div class="row">
                                <div class="col-md-12">
                                    <label>Keterangan</label>
                                    <textarea type="text" class="form-control form-control-user" name="description"></textarea>
                                </div>
                          </div>

                          <div class="row">
                                <div class="col-md-4">
                                  <label for="update_uim">Update UIM</label>
                                    <select class="form-control" id="update_uim" name="update_uim">
                                      <option value="">-</option>
                                      <option value="Done">Done</option>
                                      <option value="Inputan">Inputan</option>
                                      <option value="Disconnect">Disconnect</option>
                                      <option value="Nomor Tidak Detek">Nomor Tidak Detek</option>
                                      <option value="Node B">Node B</option>
                                      <option value="CCAN">CCAN</option>
                                      <option value="Wifi ID">Wifi ID</option>
                                      <option value="ASTINET">ASTINET</option>
                                      <option value="VPN">VPN</option>
                                    </select>
                                </div>
                                <div class="col-md-4">
                                  <label for="update_dava">Update Dava</label>
                                    <select class="form-control" id="update_dava" name="input_dava">
                                      <option value="">-</option>
                                      <option value="Done">Done</option>
                                      <option value="Qrcode Not Found">Qrcode Not Found</option>
                                      <option value="Relabel">Relabel</option>
                                      <option value="Qrcode Sudah Terpakai">Qrcode Sudah Terpakai</option>
                                      <option value="Sudah Terlabel">Sudah Terlabel</option>
                                    </select>
                                </div>
                                <div class="col-md-4">
                                    <label>Port UIM</label>
                                    <input type="text" class="form-control form-control-user" name="port_uim">
                                </div>
                                </div>
                          <div class="row">
                                <div class="col-md-4">
                                    <label>DC Dismantled</label>
                                    <input type="text" class="form-control form-control-user" name="dc_dismantled">
                                </div>                          
                                <div class="col-md-4">
                                  <label for="trouble">Kendala</label>
                                    <select class="form-control" id="trouble" name="trouble">
                                      <option value="">-</option>
                                      <option value="Disconnect">Disconnect</option>
                                      <option value="Service Tidak Detek">Service Tidak Detek</option>
                                      <option value="Service Stuck BI">Service Stuck BI</option>
                                      <option value="ODP Penuh">ODP Penuh</option>
                                      <option value="Node B">Node B</option>
                                      <option value="CCAN">CCAN</option>
                                      <option value="WIFI ID">WIFI ID</option>
                                      <option value="ASTINET">ASTINET</option>
                                      <option value="VPN">VPN</option>
                                    </select>
                                </div>
                                <div class="col-md-4">
                                  <br>
                                  <button name="submit" type="submit" class="btn btn-success btn-user">Submit</button>
                                </div>
                            </div>
                          </div>
                          </div>
                          </form>
                        <!-- <div class="modal-footer">
                          <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                          <button name="submit" type="submit" class="btn btn-success btn-user">Submit</button>
                        </div> -->
                      </div>
                    </div>
                  </div>   
                 
                  </div>
                </form>
             
            
          </div>

        </div>


      </div>
      <!-- End of Main Content -->

    </div>
    <!-- End of Content Wrapper -->

  </div>
  <!-- Form Isian Dalam Modal-->
  

  <!-- Scroll to Top Button-->
  <a class="scroll-to-top rounded" href="<?= base_url()?>#page-top">
    <i class="fas fa-angle-up"></i>
  </a>

  <!-- Logout Modal-->
  <div class="modal fade" id="logoutModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title" id="exampleModalLabel">Ready to Leave?</h5>
          <button class="close" type="button" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">×</span>
          </button>
        </div>
        <div class="modal-body">Select "Logout" below if you are ready to end your current session.</div>
        <div class="modal-footer">
          <button class="btn btn-secondary" type="button" data-dismiss="modal">Cancel</button>
          <a class="btn btn-danger" href="<?= site_url('auth/admin/logout') ?>" class="item">Logout</a>
        </div>
      </div>
    </div>
  </div>


  <!-- Bootstrap core JavaScript-->
  <script src="<?= base_url()?>asset/vendor/jquery/jquery.min.js"></script>
  <script src="<?= base_url()?>asset/vendor/bootstrap/js/bootstrap.bundle.min.js"></script>

  <!-- Core plugin JavaScript-->
  <script src="<?= base_url()?>asset/vendor/jquery-easing/jquery.easing.min.js"></script>

  <!-- Custom scripts for all pages-->
  <script src="<?= base_url()?>asset/js/sb-admin-2.min.js"></script>
  <!-- Page level plugins -->
  <script src="<?= base_url()?>asset/vendor/datatables/jquery.dataTables.min.js"></script>
  <script src="<?= base_url()?>asset/vendor/datatables/dataTables.bootstrap4.min.js"></script>

  <!-- Page level custom scripts -->
  <script src="<?= base_url()?>asset/js/demo/datatables-demo.js"></script>
            
      <!-- <script type="text/javascript">
            $(document).ready(function() {
                $('#odp-table').DataTable({
                    
                    "ajax": {
                        url : "<?php echo site_url("table/port_page")?>",
                        type : 'GET'
                    },
                });
            });
            </script> -->

      <script type="text/javascript">
          $(document).ready(function() {
              $('#odp-table').DataTable({
                // autoWidth : false,
                // deferRender:    true,
                // scrollY:        600,
                // scrollCollapse: true,
                // scroller:       true,
                // stateSave:      true
              });
          });
      </script>

</body>

</html>