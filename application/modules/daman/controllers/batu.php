<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Batu extends Modular {

	public function __construct(){
		authenticated();
        sessionAsDaman();
		parent::__construct();
		$this->load->model('Daman_models');
	}
	
	public function index()
	{	
		$this->load->helper('url');
		$Daman_models = $this->Daman_models;
		$data["total_karangploso"] = $Daman_models->jumlah_karangploso();
		$data["total_batu"] = $Daman_models->jumlah_batu ();
		$data["total_ngantang"] = $Daman_models->jumlah_ngantang();
		
		$this->load->view('datel_batu', $data);
	}

	function sto($sto = null){

		$data["odp"] = $this->Daman_models->getBySTO($sto);
		$data["sto"] = $this->Daman_models->ambil_sto($sto);		
		// print_r($data);exit();

		$this->load->view('sto_lihat', $data);
	}
}