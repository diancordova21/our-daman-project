<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <meta name="description" content="">
  <meta name="author" content="">

  <title>Master ODP - Dashboard</title>

  <!-- Custom fonts for this template-->
  <link href="<?= base_url()?>asset/vendor/fontawesome-free/css/all.min.css" rel="stylesheet" type="text/css">
  <link href="<?= base_url()?>https://fonts.googleapis.com/css?family=Nunito:200,200i,300,300i,400,400i,600,600i,700,700i,800,800i,900,900i" rel="stylesheet">
  <link rel="icon" href="<?= base_url()?>asset/img/icon-telkom.ico" type="image/x-icon">

  <!-- Custom styles for this template-->
  <link href="<?= base_url()?>asset/css/sb-admin-2.min.css" rel="stylesheet">

</head>

<body id="page-top">

  <!-- Page Wrapper -->
  <div id="wrapper">
  <?php include_once("sidebar.php") ?>
    <!-- Content Wrapper -->
    <div id="content-wrapper" class="d-flex flex-column">

      <!-- Main Content -->
      <div id="content">
        <?php include_once("topbar.php") ?>

        <!-- Begin Page Content -->
        <div class="container-fluid">

        <!-- Page Heading -->
        <div class="d-sm-flex align-items-center justify-content-between mb-4">
          <h1 class="h3 mb-0 text-gray-800">Dashboard</h1>
          <!-- <a href="#" class="d-none d-sm-inline-block btn btn-sm btn-primary shadow-sm"><i class="fas fa-download fa-sm text-white-50"></i> Generate Report</a> -->
        </div>

        <!-- Content Row -->
        <div class="row">

          <!-- Earnings (Monthly) Card Example -->
          <div class="col-xl-3 col-md-6 mb-4">
            <div class="card border-left-primary shadow h-100 py-2">
              <div class="card-body">
                <div class="row no-gutters align-items-center">
                  <div class="col mr-2">
                    <div class="text-xs font-weight-bold text-primary text-uppercase mb-1">Jumlah ODP</div>
                    <div class="h5 mb-0 font-weight-bold text-gray-800"><?php echo $total_odp?></div>
                  </div>
                  <div class="col-auto">
                    <i class="fas fa-calendar fa-2x text-gray-300"></i>
                  </div>
                </div>
              </div>
            </div>
          </div>

          <!-- Earnings (Monthly) Card Example -->
          <div class="col-xl-3 col-md-6 mb-4">
            <div class="card border-left-success shadow h-100 py-2">
              <div class="card-body">
                <div class="row no-gutters align-items-center">
                  <div class="col mr-2">
                    <div class="text-xs font-weight-bold text-success text-uppercase mb-1">Jumlah ODP golive</div>
                    <div class="h5 mb-0 font-weight-bold text-gray-800"><?php echo $total_golive?></div>
                  </div>
                  <div class="col-auto">
                    <i class="fas fa-dollar-sign fa-2x text-gray-300"></i>
                  </div>
                </div>
              </div>
            </div>
          </div>

          <!-- Earnings (Monthly) Card Example -->
          <div class="col-xl-3 col-md-6 mb-4">
            <div class="card border-left-info shadow h-100 py-2">
              <div class="card-body">
                <div class="row no-gutters align-items-center">
                  <div class="col mr-2">
                    <div class="text-xs font-weight-bold text-info text-uppercase mb-1">Jumlah datel</div>
                    <div class="row no-gutters align-items-center">
                      <div class="col-auto">
                        <div class="h5 mb-0 mr-3 font-weight-bold text-gray-800"><?php echo $total_datel?></div>
                      </div>
                    </div>
                  </div>
                  <div class="col-auto">
                    <i class="fas fa-clipboard-list fa-2x text-gray-300"></i>
                  </div>
                </div>
              </div>
            </div>
          </div>

          <!-- Pending Requests Card Example -->
          <div class="col-xl-3 col-md-6 mb-4">
            <div class="card border-left-warning shadow h-100 py-2">
              <div class="card-body">
                <div class="row no-gutters align-items-center">
                  <div class="col mr-2">
                    <div class="text-xs font-weight-bold text-warning text-uppercase mb-1">Jumlah STO</div>
                    <div class="h5 mb-0 font-weight-bold text-gray-800"><?php echo $total_sto?></div>
                  </div>
                  <div class="col-auto">
                    <i class="fas fa-comments fa-2x text-gray-300"></i>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>

        <!-- Content Row -->

        <div class="row">

          <!-- Area Chart -->
          <!-- <div class="col-xl-8 col-lg-7">
            <div class="card shadow mb-4"> -->
              <!-- Card Header - Dropdown -->
              <!-- <div class="card-header py-3 d-flex flex-row align-items-center justify-content-between">
                <h6 class="m-0 font-weight-bold text-primary">Earnings Overview</h6>
                <div class="dropdown no-arrow">
                  <a class="dropdown-toggle" href="<?= base_url()?>#" role="button" id="dropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                    <i class="fas fa-ellipsis-v fa-sm fa-fw text-gray-400"></i>
                  </a>
                  <div class="dropdown-menu dropdown-menu-right shadow animated--fade-in" aria-labelledby="dropdownMenuLink">
                    <div class="dropdown-header">Dropdown Header:</div>
                    <a class="dropdown-item" href="<?= base_url()?>#">Action</a>
                    <a class="dropdown-item" href="<?= base_url()?>#">Another action</a>
                    <div class="dropdown-divider"></div>
                    <a class="dropdown-item" href="<?= base_url()?>#">Something else here</a>
                  </div>
                </div>
              </div> -->
              <!-- Card Body -->
              <!-- <div class="card-body">
                <div class="chart-area">
                  <canvas id="myAreaChart"></canvas>
                </div>
              </div>
            </div>
          </div> -->

          <!-- Pie Chart -->
          <div class="col">
            <div class="card shadow mb-4">
              <!-- Card Header - Dropdown -->
              <div class="card-header py-3 d-flex flex-row align-items-center justify-content-between">
                <!-- <h6 class="m-0 font-weight-bold text-primary">Jumlah ODP</h6> -->
              </div>
              <!-- Card Body -->
              <div class="card-body">
                <div class="col-12">
                  <canvas id="myPieChart"></canvas>
                </div>
                <div class="mt-4 text-center small">
                  <span class="mr-2">
                    <i class="fas fa-circle text-primary"></i> MALANG
                  </span>
                  <span class="mr-2">
                    <i class="fas fa-circle text-success"></i> KEPANJEN
                  </span>
                  <span class="mr-2">
                    <i class="fas fa-circle text-warning"></i> BATU
                  </span>
                </div>
              </div>
            </div>
          </div>

      
        </div>

        <!-- Content Row -->
        <div class="row">

          <!-- Content Column -->
          <div class="col-lg-6 mb-4">

            
          </div>
        </div>

        </div>
        <!-- /.container-fluid -->

      </div>
      <!-- End of Main Content -->
    </div>
    <!-- End of Content Wrapper -->

  </div>
  <!-- End of Page Wrapper -->

  <!-- Scroll to Top Button-->
  <a class="scroll-to-top rounded" href="<?= base_url()?>#page-top">
    <i class="fas fa-angle-up"></i>
  </a>

  <!-- Logout Modal-->
  <div class="modal fade" id="logoutModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title" id="exampleModalLabel">Ready to Leave?</h5>
          <button class="close" type="button" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">×</span>
          </button>
        </div>
        <div class="modal-body">Select "Logout" below if you are ready to end your current session.</div>
        <div class="modal-footer">
          <button class="btn btn-secondary" type="button" data-dismiss="modal">Cancel</button>
          <a class="btn btn-danger" href="<?= site_url('auth/admin/logout') ?>" class="item">Logout</a>
        </div>
      </div>
    </div>
  </div>

  <?php 
foreach($jumlah_odp as $jumlah_odp){
  $sto = $jumlah_odp->sto;
  $odp = $jumlah_odp->jumlah_odp;
  
  if ($sto=="LWG") {
    $satu =  $odp;
  }elseif ($sto=="SGS") {
    $dua =  $odp;
  }elseif ($sto=="PKS") {
    $tiga =  $odp;
  }elseif ($sto=="SWJ") {
    $empat =  $odp;
  }elseif ($sto=="TMP") {
    $lima =  $odp;
  }elseif ($sto=="BLB") {
    $enam =  $odp;
  }elseif ($sto=="KLJ") {
    $tujuh =  $odp;
  }elseif ($sto=="MLG") {
    $delapan =  $odp;
  }elseif ($sto=="GDG") {
    $sembilan =  $odp;
  }elseif ($sto=="BRG") {
    $sepuluh =  $odp;
  }elseif ($sto=="DPT") {
    $sebelas =  $odp;
  }elseif ($sto=="KEP") {
    $duabelas =  $odp;
  }elseif ($sto=="TUR") {
    $tigabelas =  $odp;
  }elseif ($sto=="GDI") {
    $empatbelas =  $odp;
  }elseif ($sto=="SBM") {
    $limabelas =  $odp;
  }elseif ($sto=="SBP") {
    $enambelas =  $odp;
  }elseif ($sto=="PGK") {
    $tujuhbelas =  $odp;
  }elseif ($sto=="APG") {
    $delapanbelas =  $odp;
  }elseif ($sto=="GKW") {
    $sembilanbelas =  $odp;
  }elseif ($sto=="BNR") {
    $duapuluh =  $odp;
  }elseif ($sto=="DNO") {
    $duapuluhsatu =  $odp;
  }elseif ($sto=="KPO") {
    $duapuluhdua =  $odp;
  }elseif ($sto=="BTU") {
    $duapuluhtiga =  $odp;
  }elseif ($sto=="NTG") {
    $duapuluhempat =  $odp;
  }


}

?>
  <!-- Bootstrap core JavaScript-->
  <script src="<?= base_url()?>asset/vendor/jquery/jquery.min.js"></script>
  <script src="<?= base_url()?>asset/vendor/bootstrap/js/bootstrap.bundle.min.js"></script>

  <!-- Core plugin JavaScript-->
  <script src="<?= base_url()?>asset/vendor/jquery-easing/jquery.easing.min.js"></script>

  <!-- Custom scripts for all pages-->
  <script src="<?= base_url()?>asset/js/sb-admin-2.min.js"></script>

  <!-- Page level plugins -->
  <script src="<?= base_url()?>asset/vendor/chart.js/Chart.min.js"></script>

  <!-- Page level custom scripts -->
  <script src="<?= base_url()?>asset/js/demo/chart-area-demo.js"></script>
  <script>
    // Set new default font family and font color to mimic Bootstrap's default styling
Chart.defaults.global.defaultFontFamily = 'Nunito', '-apple-system,system-ui,BlinkMacSystemFont,"Segoe UI",Roboto,"Helvetica Neue",Arial,sans-serif';


var ctx = document.getElementById("myPieChart").getContext('2d');
var myChart = new Chart(ctx, {
  type: 'bar',
  data: {
    labels: ["Lawang", "Singosari","Pakis","Sawojajar", "Tumpang","Blimbing", "Klojen", "Malang", "Gadang","Buring","Dampit","Kepanjen","Turen","Gondang Legi","Sumbermanjing","Sumberpucung","Pagak","Ampelgading","Gunung Kawi","Bantur", "Donomulyo", "Karangploso", "Batu", "Ngantang"],
    datasets: [{
      label: 'Jumlah ODP ',
      data: [<?php echo $satu.",".$dua.",".$tiga.",".$empat.",".$lima.
",".$enam.",".$tujuh.",".$delapan.",".$sembilan.",".$sepuluh.
",".$sebelas.",".$duabelas.",".$tigabelas.",".$empatbelas.",".$limabelas.
",".$enambelas.",".$tujuhbelas.",".$delapanbelas.",".$sembilanbelas.",".$duapuluh.
",".$duapuluhsatu.",".$duapuluhdua.",".$duapuluhtiga.",".$duapuluhempat; ?>],
      backgroundColor: [
        '#5555ff',
        '#5555ff',
        '#5555ff',
        '#5555ff',
        '#5555ff',
        '#5555ff',
        '#5555ff',
        '#5555ff',
        '#5555ff',
        '#0f9d58',
        '#0f9d58',
        '#0f9d58',
        '#0f9d58',
        '#0f9d58',
        '#0f9d58',
        '#0f9d58',
        '#0f9d58',
        '#0f9d58',
        '#0f9d58',
        '#0f9d58',
        '#0f9d58',
        '#f4b400',
        '#f4b400',
        '#f4b400',
      ],
      
      borderWidth: 1
    }]
  },
  options: {
    scales: {
      yAxes: [{
        ticks: {
          beginAtZero: true
        }
      }]
    }
  }
});

  </script>

</body>

</html>