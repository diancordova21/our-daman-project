<!DOCTYPE html>
<html lang="en">

<head>
  <?php include_once("head.php"); ?>
</head>

<body id="page-top">

  <!-- Page Wrapper -->
  <div id="wrapper">
    <?php include_once("sidebar.php") ?>
    <!-- Content Wrapper -->
    <div id="content-wrapper" class="d-flex flex-column">

      <!-- Main Content -->
      <div id="content">
        <?php include_once("topbar.php") ?>

        <!-- Begin Page Content -->
        <div class="container-fluid">

          <!-- Page Heading -->
          <h1 class="h3 mb-2 text-gray-800">Tables</h1>

          <!-- DataTales Example -->
          <div class="card shadow mb-4">
            <div class="card-header py-3">
              <div class="row">
                <div class="col-lg-6 col-md-6">
                  <h6 class="m-0 font-weight-bold text-primary">Master ODP</h6>
                </div>
                <div class="col-lg-6 col-md-6">
                  <button type="button" class="btn btn-sm btn-success btn-left" style="margin-left:75%" data-toggle="modal" data-target="#importFile"><i class="fa fa-plus" aria-hidden="true"></i>Import</button>
                  <a class="btn btn-sm btn-success btn-left" href="table/create" title="Lihat"><i class="fa fa-plus" aria-hidden="true"></i>Add New</a>
                </div>
              </div>
            </div>
            <div class="card-body">
              <div class="table-responsive">
                <table id="odp-table" class="table table-bordered table-hover" width="100%" cellspacing="0">
                  <thead>
                    <tr>
                      <th>Tanggal</th>
                      <th>ODP</th>
                      <th>IP</th>
                      <th>Slot</th>
                      <th>No.Port</th>
                      <th>Status</th>
                      <th>No Inet / Voice</th>
                      <th>QR Code</th>
                      <th>Keterangan</th>
                      <th>Update UIM</th>
                      <th>Input Dava</th>
                      <th>Port UIM</th>
                      <th>DC Dismantled</th>
                      <th>Kendala</th>
                      <th>Aksi</th>
                    </tr>
                  </thead>

                  </tbody>
                </table>
              </div>
            </div>
          </div>
        </div>


      </div>
      <!-- End of Main Content -->

    </div>
    <!-- End of Content Wrapper -->

  </div>
  <!-- End of Page Wrapper -->

  <!-- Scroll to Top Button-->
  <a class="scroll-to-top rounded" href="<?= base_url()?>#page-top">
    <i class="fas fa-angle-up"></i>
  </a>

  <!-- Logout Modal-->
  <div class="modal fade" id="logoutModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
      <div class="modal-content">
        <div class="modal-header">  
          <h5 class="modal-title" id="exampleModalLabel">Ready to Leave?</h5>
          <button class="close" type="button" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">×</span>
          </button>
        </div>
        <div class="modal-body">Select "Logout" below if you are ready to end your current session.</div>
        <div class="modal-footer">
          <button class="btn btn-secondary" type="button" data-dismiss="modal">Cancel</button>
          <a class="btn btn-primary" href="<?= base_url()?>login.html">Logout</a>
        </div>
      </div>
    </div>
  </div>


  <!-- Bootstrap core JavaScript-->
  <script src="<?= base_url()?>asset/vendor/jquery/jquery.min.js"></script>
  <script src="<?= base_url()?>asset/vendor/bootstrap/js/bootstrap.bundle.min.js"></script>

  <!-- Core plugin JavaScript-->
  <script src="<?= base_url()?>asset/vendor/jquery-easing/jquery.easing.min.js"></script>

  <!-- Custom scripts for all pages-->
  <script src="<?= base_url()?>asset/js/sb-admin-2.min.js"></script>
  <!-- Page level plugins -->
  <script src="<?= base_url()?>asset/vendor/datatables/jquery.dataTables.min.js"></script>
  <script src="<?= base_url()?>asset/vendor/datatables/dataTables.bootstrap4.min.js"></script>

  <!-- Page level custom scripts -->
  <script src="<?= base_url()?>asset/js/demo/datatables-demo.js"></script>
            
      <script type="text/javascript">
            $(document).ready(function() {
                $('#odp-table').DataTable({
                    
                    "ajax": {
                        url : "<?php echo site_url("table/data_page") ?>",
                        type : 'GET'
                    },
                });
            });
      </script>


</body>

</html>



