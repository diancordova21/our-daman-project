<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <meta name="description" content="">
  <meta name="author" content="">

  <title>Master ODP - Datel Kepanjen</title>

  <!-- Custom fonts for this template-->
  <link href="<?= base_url()?>asset/vendor/fontawesome-free/css/all.min.css" rel="stylesheet" type="text/css">
  <link href="https://fonts.googleapis.com/css?family=Nunito:200,200i,300,300i,400,400i,600,600i,700,700i,800,800i,900,900i" rel="stylesheet">
  <link rel="icon" href="<?= base_url()?>asset/img/icon-telkom.ico" type="image/x-icon">

  <!-- Custom styles for this template-->
  <link href="<?= base_url()?>asset/css/sb-admin-2.min.css" rel="stylesheet">

</head>

<body id="page-top">

  <!-- Page Wrapper -->
  <div id="wrapper">
    <?php include_once("sidebar.php") ?>
    <!-- Content Wrapper -->
    <div id="content-wrapper" class="d-flex flex-column">

      <!-- Main Content -->
      <div id="content">
        <?php include_once("topbar.php") ?>

        <!-- Begin Page Content -->
        <div class="container-fluid">

          <!-- Page Heading -->
          <h1 class="h3 mb-2 text-gray-800">ODP GOLIVE</h1>

          <!-- DataTales Example -->
          <div class="card shadow mb-4">
            <div class="card-header py-3">
              <h6 class="m-0 font-weight-bold text-primary">Datel Kepanjen</h6>
            </div>
            <div class="card-body">
            <div class="table-responsive">
              <table id="helo" class="ui table stripe" style="border-radius: 0px;">
                  <thead>
                    <tr>
                      <th>STO</th>
                      <th>Jumlah ODP</th>
                      <th>2014</th>
                      <th>2015</th>
                      <th>2016</th>
                      <th>2017</th>
                      <th>2018</th>
                      <th>2019</th>
                      <th>2020</th>
                    </tr>
                  </thead>
                    <tbody>
                        <tr>
                          <td><a href="malang/sto/dpt"> Dampit </a></td>
                          <td><?php echo $jumlah_dampit->total_odp?></td>
                          <td><?php echo $jumlah_dampit->golive_2014?></td>
                          <td><?php echo $jumlah_dampit->golive_2015?></td>
                          <td><?php echo $jumlah_dampit->golive_2016?></td>
                          <td><?php echo $jumlah_dampit->golive_2017?></td>
                          <td><?php echo $jumlah_dampit->golive_2018?></td>
                          <td><?php echo $jumlah_dampit->golive_2019?></td>
                          <td><?php echo $jumlah_dampit->golive_2020?></td>                        
                        </tr>
                        <tr>
                          <td><a href="malang/sto/kep"> Kepanjen </a></td>
                          <td><?php echo $jumlah_kepanjen->total_odp?></td>
                          <td><?php echo $jumlah_kepanjen->golive_2014?></td>
                          <td><?php echo $jumlah_kepanjen->golive_2015?></td>
                          <td><?php echo $jumlah_kepanjen->golive_2016?></td>
                          <td><?php echo $jumlah_kepanjen->golive_2017?></td>
                          <td><?php echo $jumlah_kepanjen->golive_2018?></td>
                          <td><?php echo $jumlah_kepanjen->golive_2019?></td>
                          <td><?php echo $jumlah_kepanjen->golive_2020?></td>                         
                        </tr>
                        <tr>
                          <td><a href="malang/sto/tur"> Turen </a></td>
                          <td><?php echo $jumlah_turen->total_odp?></td>
                          <td><?php echo $jumlah_turen->golive_2014?></td>
                          <td><?php echo $jumlah_turen->golive_2015?></td>
                          <td><?php echo $jumlah_turen->golive_2016?></td>
                          <td><?php echo $jumlah_turen->golive_2017?></td>
                          <td><?php echo $jumlah_turen->golive_2018?></td>
                          <td><?php echo $jumlah_turen->golive_2019?></td>
                          <td><?php echo $jumlah_turen->golive_2020?></td>                         
                        </tr>
                        <tr>
                          <td><a href="malang/sto/gdi"> Gondanglegi </a></td>
                          <td><?php echo $jumlah_gondanglegi->total_odp?></td>
                          <td><?php echo $jumlah_gondanglegi->golive_2014?></td>
                          <td><?php echo $jumlah_gondanglegi->golive_2015?></td>
                          <td><?php echo $jumlah_gondanglegi->golive_2016?></td>
                          <td><?php echo $jumlah_gondanglegi->golive_2017?></td>
                          <td><?php echo $jumlah_gondanglegi->golive_2018?></td>
                          <td><?php echo $jumlah_gondanglegi->golive_2019?></td>
                          <td><?php echo $jumlah_gondanglegi->golive_2020?></td>                         
                        </tr>
                        <tr>
                          <td><a href="malang/sto/sbm"> Sumbermanjing </a></td>
                          <td><?php echo $jumlah_sumbermanjing->total_odp?></td>
                          <td><?php echo $jumlah_sumbermanjing->golive_2014?></td>
                          <td><?php echo $jumlah_sumbermanjing->golive_2015?></td>
                          <td><?php echo $jumlah_sumbermanjing->golive_2016?></td>
                          <td><?php echo $jumlah_sumbermanjing->golive_2017?></td>
                          <td><?php echo $jumlah_sumbermanjing->golive_2018?></td>
                          <td><?php echo $jumlah_sumbermanjing->golive_2019?></td>
                          <td><?php echo $jumlah_sumbermanjing->golive_2020?></td>                         
                        </tr>
                        <tr>
                          <td><a href="malang/sto/sbp"> Sumberpucung </a></td>
                          <td><?php echo $jumlah_sumberpucung->total_odp?></td>
                          <td><?php echo $jumlah_sumberpucung->golive_2014?></td>
                          <td><?php echo $jumlah_sumberpucung->golive_2015?></td>
                          <td><?php echo $jumlah_sumberpucung->golive_2016?></td>
                          <td><?php echo $jumlah_sumberpucung->golive_2017?></td>
                          <td><?php echo $jumlah_sumberpucung->golive_2018?></td>
                          <td><?php echo $jumlah_sumberpucung->golive_2019?></td>
                          <td><?php echo $jumlah_sumberpucung->golive_2020?></td>                         
                        </tr>
                        <tr>
                          <td><a href="malang/sto/pgk"> Pagak </a></td>
                          <td><?php echo $jumlah_pagak->total_odp?></td>
                          <td><?php echo $jumlah_pagak->golive_2014?></td>
                          <td><?php echo $jumlah_pagak->golive_2015?></td>
                          <td><?php echo $jumlah_pagak->golive_2016?></td>
                          <td><?php echo $jumlah_pagak->golive_2017?></td>
                          <td><?php echo $jumlah_pagak->golive_2018?></td>
                          <td><?php echo $jumlah_pagak->golive_2019?></td>
                          <td><?php echo $jumlah_pagak->golive_2020?></td>                         
                        </tr>
                        <tr>
                          <td><a href="malang/sto/apg"> Ampelgading </a></td>
                          <td><?php echo $jumlah_ampelgading->total_odp?></td>
                          <td><?php echo $jumlah_ampelgading->golive_2014?></td>
                          <td><?php echo $jumlah_ampelgading->golive_2015?></td>
                          <td><?php echo $jumlah_ampelgading->golive_2016?></td>
                          <td><?php echo $jumlah_ampelgading->golive_2017?></td>
                          <td><?php echo $jumlah_ampelgading->golive_2018?></td>
                          <td><?php echo $jumlah_ampelgading->golive_2019?></td>
                          <td><?php echo $jumlah_ampelgading->golive_2020?></td>                         
                        </tr>
                        <tr>
                          <td><a href="malang/sto/gkw"> Gunungkawi </a></td>
                          <td><?php echo $jumlah_gunungkawi->total_odp?></td>
                          <td><?php echo $jumlah_gunungkawi->golive_2014?></td>
                          <td><?php echo $jumlah_gunungkawi->golive_2015?></td>
                          <td><?php echo $jumlah_gunungkawi->golive_2016?></td>
                          <td><?php echo $jumlah_gunungkawi->golive_2017?></td>
                          <td><?php echo $jumlah_gunungkawi->golive_2018?></td>
                          <td><?php echo $jumlah_gunungkawi->golive_2019?></td>
                          <td><?php echo $jumlah_gunungkawi->golive_2020?></td>                        
                        </tr>
                        <tr>
                          <td><a href="malang/sto/btr"> Bantur </a></td>
                          <td><?php echo $jumlah_bantur->total_odp?></td>
                          <td><?php echo $jumlah_bantur->golive_2014?></td>
                          <td><?php echo $jumlah_bantur->golive_2015?></td>
                          <td><?php echo $jumlah_bantur->golive_2016?></td>
                          <td><?php echo $jumlah_bantur->golive_2017?></td>
                          <td><?php echo $jumlah_bantur->golive_2018?></td>
                          <td><?php echo $jumlah_bantur->golive_2019?></td>
                          <td><?php echo $jumlah_bantur->golive_2020?></td>                         
                        </tr>    
                        <tr>
                          <td><a href="malang/sto/dno"> Donomulyo </a></td>
                          <td><?php echo $jumlah_donomulyo->total_odp?></td>
                          <td><?php echo $jumlah_donomulyo->golive_2014?></td>
                          <td><?php echo $jumlah_donomulyo->golive_2015?></td>
                          <td><?php echo $jumlah_donomulyo->golive_2016?></td>
                          <td><?php echo $jumlah_donomulyo->golive_2017?></td>
                          <td><?php echo $jumlah_donomulyo->golive_2018?></td>
                          <td><?php echo $jumlah_donomulyo->golive_2019?></td>
                          <td><?php echo $jumlah_donomulyo->golive_2020?></td>                         
                        </tr>                   
                    </tbody>
                </table>
              </div>
              
            


            </div>
          </div>
        </div>


      </div>
      <!-- End of Main Content -->

    </div>
    <!-- End of Content Wrapper -->

  </div>
  <!-- End of Page Wrapper -->

  <!-- Scroll to Top Button-->
  <a class="scroll-to-top rounded" href="<?= base_url()?>#page-top">
    <i class="fas fa-angle-up"></i>
  </a>

  <!-- Logout Modal-->
  <div class="modal fade" id="logoutModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title" id="exampleModalLabel">Ready to Leave?</h5>
          <button class="close" type="button" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">×</span>
          </button>
        </div>
        <div class="modal-body">Select "Logout" below if you are ready to end your current session.</div>
        <div class="modal-footer">
          <button class="btn btn-secondary" type="button" data-dismiss="modal">Cancel</button>
          <a class="btn btn-danger" href="<?= site_url('auth/admin/logout') ?>" class="item">Logout</a>
        </div>
      </div>
    </div>
  </div>



  <!-- Bootstrap core JavaScript-->
  <script src="<?= base_url()?>asset/vendor/jquery/jquery.min.js"></script>
  <script src="<?= base_url()?>asset/vendor/bootstrap/js/bootstrap.bundle.min.js"></script>

  <!-- Core plugin JavaScript-->
  <script src="<?= base_url()?>asset/vendor/jquery-easing/jquery.easing.min.js"></script>

  <!-- Custom scripts for all pages-->
  <script src="<?= base_url()?>asset/js/sb-admin-2.min.js"></script>
  <!-- Page level plugins -->
  <script src="<?= base_url()?>asset/vendor/datatables/jquery.dataTables.min.js"></script>
  <script src="<?= base_url()?>asset/vendor/datatables/dataTables.bootstrap4.min.js"></script>

  <!-- Page level custom scripts -->
  <script src="<?= base_url()?>asset/js/demo/datatables-demo.js"></script>
            
      <script type="text/javascript">
            $(document).ready(function() {
                $('#odp-table').DataTable({
                    
                    "ajax": {
                        url : "<?php echo site_url("table/odp_page") ?>",
                        type : 'GET'
                    },
                });
            });
      </script>


</body>

</html>



