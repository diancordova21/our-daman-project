<?php

class Register extends Modular
{
    public function __construct(){
		authenticated();
        sessionAsDaman();
		parent::__construct(); 
		$this->load->model('auth/Admin_model');
	}

    function index()
    {
        $this->load->view('daman/register');
    }

    public function addUser(){
        redirectIfAuthenticated();
        $this->load->model('auth/Admin_model');
        $username = $this->input->post('username');
        $fullname = $this->input->post('fullname');
        $password = $this->input->post('password');
        $role = $this->input->post('role');
    

        if (isset($_POST['submit'])) {
            // $result = $this->Admin_model->loginByUsername($username,$password);
            $result = $this->Admin_model->register($username,$fullname,$password,$role);
            if ($result) {
                echo "<script>alert('Berhasil mendaftar')</script>";
                $this->load->view('daman/register');
                return true;
             }else {
                $this->load->view('daman/register');
                echo "<script>alert('Gagal mendaftar pada sistem harap cek kembali')</script>";
                return false;
            } 
        } else {
            $this->load->view('daman/register');
            return false;
        }
    }

    public function logout(){
        $this->session->unset_userdata('logged_in');
        $this->session->sess_destroy();
        redirect('auth/admin/login');
    }

}
