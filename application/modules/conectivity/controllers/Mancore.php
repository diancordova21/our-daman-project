<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Mancore extends Modular
{
    public function __construct(){
        authenticated();
        sessionAsRivara();
		parent::__construct();
		$this->load->library('form_validation'); 
		$this->load->model('Conectivity_models');
    }

    public function index()
	{	
        $this->load->helper('url');
		$data['mancore'] =$this->Conectivity_models->get_all();
		$this->load->view('views_mancore', $data);
    }
}