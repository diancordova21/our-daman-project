<?php
ini_set('max_execution_time', 0); 
ini_set('memory_limit','2048M');
defined('BASEPATH') OR exit('No direct script access allowed');

class Table extends Modular {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */

	public function __construct(){
		authenticated();
        sessionAsAOM();
		parent::__construct();
		$this->load->model('Master_odp');
		$this->load->library('form_validation');
	}
	  
	public function index()
	{	
		$this->load->helper('url');
		// $data['pageName'] = "tables";
		// $this->load->view('index', $data);
		$this->load->view('tables', array());
		
	}

	public function odp_page()
     {

          // Datatables Variables
          $draw = intval($this->input->get("draw"));
          $start = intval($this->input->get("start"));
          $length = intval($this->input->get("length")); 


          $odp = $this->Master_odp->get_odp();

          $data = array();

          foreach($odp->result() as $r) {

				$row = array();
				$row[] = $r->datel;
				$row[] = $r->sto;
				$row[] = $r->odp;
				$row[] = $r->latitude;
				$row[] = $r->longitude;
				$row[] = $r->alamat;
				$row[] = $r->avai;
				$row[] = $r->reserved;
				$row[] = $r->in_service;
				$row[] = $r->total;
				$row[] = $r->tanggal_golive;
				$row[] = $r->bulan;
				$row[] = $r->tahun;
				$row[] = $r->kelurahan;
				$row[] = $r->kecamatan; 
				$row[] = $r->kota_kab;
				$row[] = $r->qrcode_odp;
				$row[] = $r->qrcode_port;
				$row[] = $r->project;
				$row[] = $r->vendor;
				$row[] = $r->merk;
				$row[] = $r->ip;

				//add html for action
				$row[] = '<a class="btn btn-sm btn-success" href="table/lihat/'.$r->id_odp.'" title="Lihat"><i class="glyphicon glyphicon-folder"></i> Lihat</a>
				<a class="btn btn-sm btn-warning" href="table/update/'.$r->id_odp.'" title="Lihat"><i class="glyphicon glyphicon-folder"></i> Update</a>
				<a href="table/delete/'.$r->id_odp.'" class="btn btn-sm btn-danger" onclick="return confirm(\'Anda yakin untuk menghapus data ini?\');"> Hapus</a>
				';

				$data[] = $row;
          }

          $output = array(
               "draw" => $draw,
                 "recordsTotal" => $odp->num_rows(),
                 "recordsFiltered" => $odp->num_rows(),
                 "data" => $data
            );
          echo json_encode($output);
          exit();
    }
	//Lihat
	function lihat($id = null){
		if (!isset($id)) redirect('table/index');

		$Master_odp = $this->Master_odp;
		$data["masterodp"]=$Master_odp->getById($id);
		
		$this->load->view("tables_lihat", $data);
	}

	//Create
	function create(){
        if (isset($_POST['submit'])) {
			$this->Master_odp->create();
			redirect('table/index');
		} else {
			$this->load->view("tables_create");
		}
	}

	//Update
	function update($id = null){
		if (isset($_POST['submit'])){
				// print_r($this->input->post('nama_odp'));exit();
                $this->Master_odp->UpdateById($id);
				redirect('table/index');
			}
         else {
			if (!isset($id)) redirect('table/index');

			$Master_odp = $this->Master_odp;
			$data["masterodp"]=$Master_odp->getById($id);
			
	
			$this->load->view("tables_update", $data);
        }
	
	}

	function delete($id=null)
	{
	    if (!isset($id)) show_404();

	    if ($this->Master_odp->hapus($id)) {
		    redirect(site_url('table/index'));
	    }
	}

	public function view(){
		$search = $_POST['search']['value']; // Ambil data yang di ketik user pada textbox pencarian
		$limit = $_POST['length']; // Ambil data limit per page
		$start = $_POST['start']; // Ambil data start
		$order_index = $_POST['order'][0]['column']; // Untuk mengambil index yg menjadi acuan untuk sorting
		$order_field = $_POST['columns'][$order_index]['data']; // Untuk mengambil nama field yg menjadi acuan untuk sorting
		$order_ascdesc = $_POST['order'][0]['dir']; // Untuk menentukan order by "ASC" atau "DESC"
		$sql_total = $this->Master_odp->count_all(); // Panggil fungsi count_all pada SiswaModel
		$sql_data = $this->Master_odp->filter($search, $limit, $start, $order_field, $order_ascdesc); // Panggil fungsi filter pada SiswaModel
		$sql_filter = $this->Master_odp->count_filter($search); // Panggil fungsi count_filter pada SiswaModel
		$callback = array(
			'draw'=>$_POST['draw'], // Ini dari datatablenya
			'recordsTotal'=>$sql_total,
			'recordsFiltered'=>$sql_filter,
			'data'=>$sql_data
		);
		header('Content-Type: application/json');
		echo json_encode($callback); // Convert array $callback ke json
	}

	public function import() {
        $data = array();
		$this->form_validation->set_rules('fileURL', 'Upload File', 'callback_checkFileValidation');
		
        // If file uploaded
		if(!empty($_FILES['fileURL']['name'])) { 
			// get file extension
			$extension = pathinfo($_FILES['fileURL']['name'], PATHINFO_EXTENSION);
			
			if($extension == 'csv'){
				$reader = new \PhpOffice\PhpSpreadsheet\Reader\Csv();
			} elseif($extension == 'xlsx') {
				$reader = new \PhpOffice\PhpSpreadsheet\Reader\Xlsx();
			} else {
				$reader = new \PhpOffice\PhpSpreadsheet\Reader\Xls();
			}
			// file path
			$spreadsheet = $reader->load($_FILES['fileURL']['tmp_name']);
			$allDataInSheet = $spreadsheet->getActiveSheet()->toArray();
		
			// array Count
			$arrayCount = count($allDataInSheet);

			// delete first row
			unset($allDataInSheet[0]);

			$countSuccess = 0;
			$countFailed = 0;
			
			//  read rows and insert into table
			foreach ($allDataInSheet as $key => $value) {
				$datel		= $value[0];
				$sto  		= $value[1];
				$odp  		= $value[2];
				$latitude 	= $value[3];
				$longitude 	= $value[4];
				$alamat    	= $value[5];
				$avai		= $value[6];
				$reserved  	= $value[7];
				$inService 	= $value[8];
				$total     	= $value[9];
				$tanggalGolive = $value[10];
				$bulan 		= $value[11];
				$tahun 		= $value[12];
				$kelurahan 	= $value[13];
				$kecamatan 	= $value[14];
				$kota 		= $value[15];
				$qrOdp		= $value[16];
				$qrPort 	= $value[17];
				$project 	= $value[18];
				$vendor 	= $value[19];
				$merk 		= $value[20];
				$ip 		= $value[21];
				
				$datel 		= filter_var(trim($datel), FILTER_SANITIZE_STRING);
				$sto 		= filter_var(trim($sto), FILTER_SANITIZE_STRING);
				$odp 		= filter_var(trim($odp), FILTER_SANITIZE_STRING);
				$latitude 	= filter_var(trim($latitude), FILTER_SANITIZE_STRING);
				$longitude 	= filter_var(trim($longitude), FILTER_SANITIZE_STRING);
				$alamat 	= filter_var(trim($alamat), FILTER_SANITIZE_STRING);
				$avai 		= filter_var(trim($avai), FILTER_SANITIZE_STRING);
				$reserved 	= filter_var(trim($reserved), FILTER_SANITIZE_STRING);
				$inService 	= filter_var(trim($inService), FILTER_SANITIZE_STRING);
				$total 		= filter_var(trim($total), FILTER_SANITIZE_STRING);
				$tanggalGolive = filter_var(trim($tanggalGolive), FILTER_SANITIZE_STRING);
				$bulan 		= filter_var(trim($bulan), FILTER_SANITIZE_STRING);
				$tahun 		= filter_var(trim($tahun), FILTER_SANITIZE_STRING);
				$kelurahan 	= filter_var(trim($kelurahan), FILTER_SANITIZE_STRING);
				$kecamatan 	= filter_var(trim($kecamatan), FILTER_SANITIZE_STRING);
				$kota 		= filter_var(trim($kota), FILTER_SANITIZE_STRING);
				$qrOdp 		= filter_var(trim($qrOdp), FILTER_SANITIZE_STRING);
				$qrPort 	= filter_var(trim($qrPort), FILTER_SANITIZE_STRING);
				$project 	= filter_var(trim($project), FILTER_SANITIZE_STRING);
				$vendor 	= filter_var(trim($vendor), FILTER_SANITIZE_STRING);
				$merk 		= filter_var(trim($merk), FILTER_SANITIZE_STRING);
				$ip 		= filter_var(trim($ip), FILTER_SANITIZE_STRING);
				
				$tanggalGolive = (new DateTime($tanggalGolive))->format('Y-m-d');

				$fetchDataMasterODP = array(
					'datel' 	=> $datel,
					'sto' 		=> $sto,
					'odp' 		=> $odp,
					'latitude' 	=> $latitude,
					'longitude' => $longitude,
					'alamat' 	=> $alamat,
					'avai' 		=> $avai,
					'reserved' 	=> $reserved,
					'in_service'=> $inService,
					'total' 	=> $total,
					'tanggal_golive' => $tanggalGolive,
					'bulan' 	=> $bulan,
					'tahun' 	=> $tahun,
					'kelurahan' => $kelurahan,
					'kecamatan' => $kecamatan,
					'kota_kab' 	=> $kota,
					'qrcode_odp'=> $qrOdp,
					'qrcode_port'	=> $qrPort,
					'project' 	=> $project,
					'vendor' 	=> $vendor,
					'merk' 		=> $merk,
					'ip' 		=> $ip
				);

				//query to check odp return id_odp
				$odpId = $this->Master_odp->checkMasterOdp($odp);

				if($odpId){
					//if master odp already exist then update
					$this->Master_odp->updateMasterOdp($odpId, $fetchDataMasterODP);
				}else{
					//if master odp not exist then insert new
					$this->Master_odp->insertMasterOdp($fetchDataMasterODP);
				}
			}
		}
    }
 
    // checkFileValidation
    public function checkFileValidation($string) {
      $file_mimes = array('text/x-comma-separated-values', 
        'text/comma-separated-values', 
        'application/octet-stream', 
        'application/vnd.ms-excel', 
        'application/x-csv', 
        'text/x-csv', 
        'text/csv', 
        'application/csv', 
        'application/excel', 
        'application/vnd.msexcel', 
        'text/plain', 
        'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet'
      );
      if(isset($_FILES['fileURL']['name'])) {
            $arr_file = explode('.', $_FILES['fileURL']['name']);
            $extension = end($arr_file);
            if(($extension == 'xlsx' || $extension == 'xls' || $extension == 'csv') && in_array($_FILES['fileURL']['type'], $file_mimes)){
                return true;
            }else{
                $this->form_validation->set_message('checkFileValidation', 'Please choose correct file.');
                return false;
            }
        }else{
            $this->form_validation->set_message('checkFileValidation', 'Please choose a file.');
            return false;
        }
    }
}