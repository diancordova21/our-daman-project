<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <meta name="description" content="">
  <meta name="author" content="">

  <title>Tambah ODP</title>

  <!-- Custom fonts for this template-->
  <link href="<?= base_url()?>asset/vendor/fontawesome-free/css/all.min.css" rel="stylesheet" type="text/css">
  <link href="<?= base_url()?>https://fonts.googleapis.com/css?family=Nunito:200,200i,300,300i,400,400i,600,600i,700,700i,800,800i,900,900i" rel="stylesheet">
  <link rel="icon" href="<?= base_url()?>asset/img/icon-telkom.ico" type="image/x-icon">

  <!-- Custom styles for this template-->
  <link href="<?= base_url()?>asset/css/sb-admin-2.min.css" rel="stylesheet">

</head>

<body id="page-top">

  <!-- Page Wrapper -->
  <div id="wrapper">
    <?php include_once("sidebar.php") ?>
    <!-- Content Wrapper -->
    <div id="content-wrapper" class="d-flex flex-column">

      <!-- Main Content -->
      <div id="content">
        <?php include_once("topbar.php") ?>

        <!-- Begin Page Content -->
        <div class="container-fluid">
          
          <!-- DataTales Example -->
          <div class="card shadow mb-4">
                <div class="card-header py-3">
                  <h3 class="m-0 font-weight-bold text-primary">Tambahkan</h3>
                </div>
                <div class="card-body">
                <form method="post" action="<?= site_url('table/create') ?>" enctype="multipart/form-data"> 
                  <div class="row">
                  
                    <div class="col-xl-6">
                      <div class="p-7">
                        <!-- <div class="container"> -->
                            <div class="form-group row">
                              <div class="col-sm-10 mb-3 mb-sm-0">
                                <h6>Nama ODP : </h6>
                                <input type="text" class="form-control form-control-user" name="nama_odp">
                              </div>
                  
                            </div>
                            <div class="form-group row">
                              <div class="col-sm-10 mb-3 mb-sm-0">
                                <div class="form-group">
                                  <label for="sel1">Datel :</label>
                                  <select class="form-control" id="sel1" name="datel">
                                    <option value="Kepanjen">Kepanjen</option>
                                    <option value="Malang">Malang</option>
                                    <option value="Batu">Batu</option>
                                  </select>
                                </div> 
                              </div>
                            </div>
                            <div class="form-group row">
                              <div class="col-sm-10 mb-3 mb-sm-0">

                                <h6>STO : </h6>
                                  <input type="text" class="form-control form-control-user" name="sto">
                              </div>
                            </div>
                            <div class="form-group row">
                              <div class="col-sm-10 mb-3 mb-sm-0">

                                <h6>Latitude : </h6>
                                  <input type="text" class="form-control form-control-user" name="latitude">
                              </div>
                            </div>
                            <div class="form-group row">
                              <div class="col-sm-10 mb-3 mb-sm-0">

                                <h6>Longitude : </h6>
                                  <input type="text" class="form-control form-control-user" name="longitude">
                              </div>
                            </div>
                            <div class="form-group row">
                              <div class="col-sm-10 mb-3 mb-sm-0">

                                <h6>Alamat : </h6>
                                  <input type="text" class="form-control form-control-user" name="alamat">
                              </div>
                            </div>
                            <div class="form-group row">
                              <div class="col-sm-10 mb-3 mb-sm-0">

                                <h6>Available : </h6>
                                  <input type="text" class="form-control form-control-user" name="available">
                              </div>
                            </div>
                            <div class="form-group row">
                              <div class="col-sm-10 mb-3 mb-sm-0">

                                <h6>Reserved : </h6>
                                  <input type="text" class="form-control form-control-user" name="reserved">
                              </div>
                            </div>
                            <div class="form-group row">
                              <div class="col-sm-10 mb-3 mb-sm-0">

                                <h6>In Service : </h6>
                                  <input type="text" class="form-control form-control-user" name="in_service">
                              </div>
                            </div>
                            <div class="form-group row">
                              <div class="col-sm-10 mb-3 mb-sm-0">

                                <h6>Total : </h6>
                                  <input type="text" class="form-control form-control-user" name="total">
                              </div>
                            </div>
                            <div class="form-group row">
                              <div class="col-sm-10 mb-3 mb-sm-0">
                                <h6>Tanggal Go Live : </h6>
                                <input type="date" class="form-control form-control-user" name="tanggal_golive">
                              </div>
                            </div>
                            </div>
                          </div>
                          <div class="col">
                            <div class="form-group row">
                              <div class="col-sm-10 mb-3 mb-sm-0">
                                <div class="form-group">
                                  <label for="sel1">Bulan :</label>
                                  <select class="form-control" id="sel1" name="bulan">
                                    <option value="01_Jan">01_Jan</option>
                                    <option value="02_Feb">02_Feb</option>
                                    <option value="03_Mar">03_Mar</option>
                                    <option value="04_Apr">04_Apr</option>
                                    <option value="05_Mei">05_Mei</option>
                                    <option value="06_Jun">06_Jun</option>
                                    <option value="07_Jul">07_Jul</option>
                                    <option value="08_Agu">08_Agu</option>
                                    <option value="09_Sep">09_Sep</option>
                                    <option value="10_Okt">10_Okt</option>
                                    <option value="11_Nov">11_Nov</option>
                                    <option value="12_Des">12_Dev</option>
                                  </select>
                                </div> 
                              </div>
                            </div>
                            <div class="form-group row">
                              <div class="col-sm-10 mb-3 mb-sm-0">

                                <h6>Tahun : </h6>
                                  <input type="text" class="form-control form-control-user" name="tahun">
                              </div>
                            </div>
                            <div class="form-group row">
                              <div class="col-sm-10 mb-3 mb-sm-0">

                                <h6>Kelurahan : </h6>
                                  <input type="text" class="form-control form-control-user" name="kelurahan">
                              </div>
                            </div>
                            <div class="form-group row">
                              <div class="col-sm-10 mb-3 mb-sm-0">

                                <h6>Kecamatan : </h6>
                                  <input type="text" class="form-control form-control-user" name="kecamatan">
                              </div>
                            </div>
                            <div class="form-group row">
                              <div class="col-sm-10 mb-3 mb-sm-0">

                                <h6>Kota/Kab : </h6>
                                  <input type="text" class="form-control form-control-user" name="kota_kab">
                              </div>
                            </div>
                            <div class="form-group row">
                              <div class="col-sm-10 mb-3 mb-sm-0">

                                <h6>QR Code ODP : </h6>
                                  <input type="text" class="form-control form-control-user" name="qrcode_odp">
                              </div>
                            </div>
                            <div class="form-group row">
                              <div class="col-sm-10 mb-3 mb-sm-0">

                                <h6>QR Code Port : </h6>
                                  <input type="text" class="form-control form-control-user" name="qrcode_port">
                              </div>
                            </div>
                            <div class="form-group row">
                              <div class="col-sm-10 mb-3 mb-sm-0">

                                <h6>Project : </h6>
                                  <input type="text" class="form-control form-control-user" name="project">
                              </div>
                            </div>
                            <div class="form-group row">
                              <div class="col-sm-10 mb-3 mb-sm-0">

                                <h6>Vendor : </h6>
                                  <input type="text" class="form-control form-control-user" name="vendor">
                              </div>
                            </div>
                            <div class="form-group row">
                              <div class="col-sm-10 mb-3 mb-sm-0">

                                <h6>Merk : </h6>
                                  <input type="text" class="form-control form-control-user" name="merk">
                              </div>
                            </div>
                            <div class="form-group row">
                              <div class="col-sm-10 mb-3 mb-sm-0">

                                <h6>Ip : </h6>
                                  <input type="text" class="form-control form-control-user" name="ip">
                              </div>
                            </div>
                            <div class="form-group row">
                              <div class="col-sm-10 mb-3 mb-sm-0">
                                <button name="submit" type="submit" class="btn btn-success btn-user">Submit</button>
                              </div>
                            </div>
                        </div>                            
                      </div>
                    </div>
                  </div>
                </form>
                </div>
              </div>
            </div>
          </div>
          
        </div>


      </div>
      <!-- End of Main Content -->

    </div>
    <!-- End of Content Wrapper -->

  </div>
  <!-- End of Page Wrapper -->

  <!-- Scroll to Top Button-->
  <a class="scroll-to-top rounded" href="<?= base_url()?>#page-top">
    <i class="fas fa-angle-up"></i>
  </a>

  <!-- Logout Modal-->
  <div class="modal fade" id="logoutModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title" id="exampleModalLabel">Ready to Leave?</h5>
          <button class="close" type="button" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">×</span>
          </button>
        </div>
        <div class="modal-body">Select "Logout" below if you are ready to end your current session.</div>
        <div class="modal-footer">
          <button class="btn btn-secondary" type="button" data-dismiss="modal">Cancel</button>
          <a class="btn btn-danger" href="<?= site_url('auth/admin/logout') ?>" class="item">Logout</a>
        </div>
      </div>
    </div>
  </div>



  <!-- Bootstrap core JavaScript-->
  <script src="<?= base_url()?>asset/vendor/jquery/jquery.min.js"></script>
  <script src="<?= base_url()?>asset/vendor/bootstrap/js/bootstrap.bundle.min.js"></script>

  <!-- Core plugin JavaScript-->
  <script src="<?= base_url()?>asset/vendor/jquery-easing/jquery.easing.min.js"></script>

  <!-- Custom scripts for all pages-->
  <script src="<?= base_url()?>asset/js/sb-admin-2.min.js"></script>
  <!-- Page level plugins -->
  <script src="<?= base_url()?>asset/vendor/datatables/jquery.dataTables.min.js"></script>
  <script src="<?= base_url()?>asset/vendor/datatables/dataTables.bootstrap4.min.js"></script>

  <!-- Page level custom scripts -->
  <script src="<?= base_url()?>asset/js/demo/datatables-demo.js"></script>
            
      <script type="text/javascript">
            $(document).ready(function() {
                $('#odp-table').DataTable({
                    
                    "ajax": {
                        url : "<?php echo site_url("table/odp_page") ?>",
                        type : 'GET'
                    },
                });
            });
            </script>

</body>

</html>