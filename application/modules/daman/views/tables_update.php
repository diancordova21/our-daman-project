<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <meta name="description" content="">
  <meta name="author" content="">

  <title>Rincian Port ODP </title>

  <!-- Custom fonts for this template-->
  <link href="<?= base_url()?>asset/vendor/fontawesome-free/css/all.min.css" rel="stylesheet" type="text/css">
  <link href="<?= base_url()?>https://fonts.googleapis.com/css?family=Nunito:200,200i,300,300i,400,400i,600,600i,700,700i,800,800i,900,900i" rel="stylesheet">

  <!-- Custom styles for this template-->
  <link href="<?= base_url()?>asset/css/sb-admin-2.min.css" rel="stylesheet">

</head>

<body id="page-top">

  <!-- Page Wrapper -->
  <div id="wrapper">
    <?php include_once("partials/sidebar.php") ?>
    <!-- Content Wrapper -->
    <div id="content-wrapper" class="d-flex flex-column">

      <!-- Main Content -->
      <div id="content">
        <?php include_once("partials/topbar.php") ?>

        <!-- Begin Page Content -->
        <div class="container-fluid">
          
          <!-- DataTales Example -->
          <div class="card shadow mb-4">
            <div class="card-header py-3">
              <h3 class="m-0 font-weight-bold text-primary">ODP: <?= $odp[0]->odp_name?></h3>
            </div>
            <div class="card-body">
            <form>

                <div class="col">
                
                  <div class="card">            
                    <div class="card-body">
                      <div class="row">
                        <div class="col">
                          <p>Tanggal Revitalisasi</p>
                          <p>Tanggal Update</p>
                          <p>IP Address </p>
                          <p>Slot Port </p>
                          <!-- <p>Status </p>
                          <p>Inet Voice Customer </p>
                          <p>QR Code </p>
                          <p>Description </p>
                          <p>Update UIM </p>
                          <p>Input Dava </p>
                          <p>DC Dismantled </p>
                          <p>Trouble </p> -->

                        </div>
                        <div>
                          <div class="col" style>
                          <p>:</p>
                          <p>:</p>
                          <p>:</p>
                          <p>:</p>
                          <!-- <p>:</p>
                          <p>:</p>
                          <p>:</p>
                          <p>:</p>
                          <p>:</p>
                          <p>:</p>
                          <p>:</p>
                          <p>:</p> -->
                          
                          
                          </div>
                        </div>

                        <div class="col">
                          <p><?= $odp[0]->date ?></p>
                          <p><?= $odp[0]->date ?></p>
                          <p><?= $odp[0]->ip_address ?></p>
                          <p><?= $odp[0]->slot_port ?></p>
                         

                        </div>
                      </div>
                    </div>
                  </div>



                </div>
              </div>

              
              

            </form>

            </div>
          </div>
          <div class="card shadow mb-4" style="margin:3%">
              <div class="card-header py-3">
                <div class="row">
                <h3 class="m-0 font-weight-bold text-primary">Table Port</h3>
                <button type="button" class="btn btn-success btn-user" style="margin-left:75%" data-toggle="modal" data-target="#exampleModalLong">Tambah</button>
                  </div>
              </div>
                <div class="card-body">
                  <form method="post" action="<?= site_url('daman/table/update_table/'.$this->uri->segment(4)) ?>" enctype="multipart/form-data"> 
                    <div class="table-responsive">
                      <table id="odp-table" class="table table-bordered table-hover" width="100%" cellspacing="0">
                        <thead>
                          <tr>
                            <th>No Port</th>
                            <th>Status</th>
                            <th>Ownership</th>
                            <th>No Inet & Voice Consumer</th>
                            <th>QR Code</th>
                            <th>Update UIM</th>
                            <th>Input Dava</th>
                            <th>Port UIM</th>
                            <th>DC Dismantled</th>
                            <th>Trouble</th>
                            <th>Keterangan</th>
                          </tr>
                        </thead>
                        <tbody>
                        <?php foreach ($port_odp as $data) : ?>
                                  <tr>
                                      <td><?= $data->no_port ?></td>
                                      <td>
                                      <select name="status[]" id="">
                                          <option <?php if($data->status == '') echo 'selected' ?> value="">-</option>
                                          <option <?php if($data->status == 'ON') echo 'selected' ?> value="ON">ON</option>
                                          <option <?php if($data->status == 'OFF') echo 'selected' ?> value="OFF">OFF</option>
                                          <option <?php if($data->status == 'Rusak') echo 'selected' ?> value="OFF">Rusak</option>
                                          <option <?php if($data->status == 'Free') echo 'selected' ?> value="OFF">Free</option>
                                          <option <?php if($data->status == 'Passive Splitter') echo 'selected' ?> value="OFF">Passive Splitter</option>
                                        </select></td>
                                        <td>
                                      <select name="ownership[]" id="">
                                          <option <?php if($data->ownership == '') echo 'selected' ?> value="">-</option>
                                          <option <?php if($data->ownership == 'CCAN VPN') echo 'selected' ?> value="CCAN VPN">CCAN VPN</option>
                                          <option <?php if($data->ownership == 'CCAN Astinet') echo 'selected' ?> value="CCAN Astinet">CCAN Astinet</option>
                                          <option <?php if($data->ownership == 'Node B') echo 'selected' ?> value="Node B">Node B</option>
                                          <option <?php if($data->ownership == 'CS/Retail') echo 'selected' ?> value="CS/Retail">CS/Retail</option>
                                          <option <?php if($data->ownership == 'Wifi ID') echo 'selected' ?> value="Wifi ID">Wifi ID</option>
                                        </select></td>
                                      
                                      <td><textarea rows="4" name="inet_voice_customer[]" Id="Text" value=""><?= $data->inet_voice_customer?></textarea></td>
                                      <td>
                                      <textarea rows="1" cols="" name="qr_code[]" value=""><?= $data->qr_code?></textarea>
                                      </td>
                                      <td><select name="update_uim[]" id="">
                                          <option <?php if($data->update_uim == '') echo 'selected' ?> value="">-</option>
                                          <option <?php if($data->update_uim == 'Done') echo 'selected' ?> value="Done">Done</option>
                                          <option <?php if($data->update_uim == 'Inputan') echo 'selected' ?> value="Inputan">Inputan</option>
                                          <option <?php if($data->update_uim == 'Disconnect') echo 'selected' ?> value="Disconnect">Disconnect</option>
                                          <option <?php if($data->update_uim == 'Nomor Tidak Detek') echo 'selected' ?> value="Nomor Tidak Detek">Nomor Tidak Detek</option>
                                          <option <?php if($data->update_uim == 'Node B') echo 'selected' ?> value="Node B">Node B</option>
                                          <option <?php if($data->update_uim == 'CCAN') echo 'selected' ?> value="CCAN">CCAN</option>
                                          <option <?php if($data->update_uim == 'WIFI ID') echo 'selected' ?> value="WIFI ID">WIFI ID</option>
                                          <option <?php if($data->update_uim == 'ASTINET') echo 'selected' ?> value="ASTINET">ASTINET</option>
                                          <option <?php if($data->update_uim == 'VPN') echo 'selected' ?> value="VPN">VPN</option>
                                        </select></td>
                                      <td><select name="input_dava[]" id="">
                                          <option <?php if($data->input_dava == 'Done') echo 'selected' ?> value="">-</option>
                                          <option <?php if($data->input_dava == 'Qrcode Not Found') echo 'selected' ?> value="Qrcode Not Found">Qrcode Not Found</option>
                                          <option <?php if($data->input_dava == 'Relabel') echo 'selected' ?> value="Relabel">Relabel</option>
                                          <option <?php if($data->input_dava == 'Qrcode Sudah Terpakai') echo 'selected' ?> value="Qrcode Sudah Terpakai">Qrcode Sudah Terpakai</option>
                                          <option <?php if($data->input_dava == 'Sudah Terlabel') echo 'selected' ?> value="Sudah Terlabel">Sudah Terlabel</option>

                                          
                                        </select></td>
                                      <td><textarea style="height:30px;" rows="1" cols="2" Id="Text" name="port_uim[]"><?= $data->port_uim?></textarea></td>
                                      <td><textarea style="height:30px;" rows="3" Id="Text" name="dc_dismantled[]"><?= $data->dc_dismantled?></textarea></td>
                                      <td><select name="trouble[]" id="">
                                          <option <?php if($data->trouble == '') echo 'selected' ?> value="">-</option>
                                          <option <?php if($data->trouble == 'Disconnect') echo 'selected' ?> value="Disconnect">Disconnect</option>
                                          <option <?php if($data->trouble == 'Redaman Tinggi') echo 'selected' ?> value="Redaman Tinggi">Redaman Tinggi</option>
                                          <option <?php if($data->trouble == 'Service Stuck BI') echo 'selected' ?> value="Service Stuck BI">Service Stuck BI</option>
                                          <option <?php if($data->trouble == 'Tidak Detek Full') echo 'selected' ?> value="Tidak Detek Full">Tidak Detek Full</option>
                                          <option <?php if($data->trouble == 'Tidak Detek Sebagian') echo 'selected' ?> value="Tidak Detek Sebagian">Tidak Detek Sebagian</option>
                                          <option <?php if($data->trouble == 'Splitter') echo 'selected' ?> value="Splitter">Splitter</option>
                                          <option <?php if($data->trouble == 'Kapasitas') echo 'selected' ?> value="Kapasitas">Kapasitas</option>
                                          <option <?php if($data->trouble == 'Tidak Bisa Create New Version') echo 'selected' ?> value="Tidak Bisa Create New Version">Tidak Bisa Create New Version</option>
                                          <option <?php if($data->trouble == 'Service Suspend') echo 'selected' ?> value="Service Suspend">Service Suspend</option>
                                          <option <?php if($data->trouble == 'Service Canceled') echo 'selected' ?> value="Service Canceled">Service Canceled</option>
                                          <option <?php if($data->trouble == 'ONT Detect No Service') echo 'selected' ?> value="ONT Detect No Service">ONT Detect No Service</option>
                                          <option <?php if($data->trouble == 'ODP Susah Dijangkau') echo 'selected' ?> value="ODP Susah Dijangkau">ODP Susah Dijangkau</option>
                                        </select></td>
                                      <td><textarea rows="5" cols="30" Id="Text" name="description[]"><?= $data->description?></textarea></td>


                                  </tr>
                                  <input type="hidden" name="id_port[]" value="<?= $data->id_port?>"> 
                                  <tr></tr>
                              <?php endforeach; ?>
                        </tbody>
                      </table>
                    </div>
                    <button name="submit" type="submit" class="btn btn-primary btn-user" style="">Save</button>
                  </form> 
              
            </div>
                </form>
              </div>
            
          </div>
          <div class="modal fade" id="exampleModalLong" tabindex="-1" role="dialog" aria-labelledby="exampleModalLongTitle" aria-hidden="true">
                    <div class="modal-dialog modal-lg" role="document">
                      <div class="modal-content">
                        <div class="modal-header">
                          <h5 class="modal-title" id="exampleModalLongTitle">Tambah Port ODP</h5>
                          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                          </button>
                        </div>
                        <div class="modal-body">
                          <div class="row">
                            <!-- mulai -->
                                <div class="col-md-4">
                                  <input type="hidden" class="form-control form-control-user" name= "id_odp" value="<?= @$odp[0]->id_odp ?>" readonly="readonly">

                                    <label>Nama ODP</label>
                                    <input type="text" class="form-control form-control-user" value="<?= @$odp[0]->odp_name ?>" readonly="readonly">
                                </div>
                                <div class="col-md-4">
                                    <label>No. Port</label>
                                    <input type="text" class="form-control form-control-user" name="no_port">
                                </div>
                                <div class="col-md-4">
                                    <label for="status">Status</label>
                                    <select class="form-control" id="status" name="status">
                                      <option value="">-</option>
                                      <option value="ON">ON</option>
                                      <option value="OFF">OFF</option>
                                      <option value="Rusak">Rusak</option>
                                      <option value="Free">Free</option>
                                      <option value="Passive Splitter">Passive Splitter</option>
                                    </select>
                                </div>
                          </div>
                          <div class="row">
                                <div class="col-md-4">
                                    <label for="ownership">Ownership</label>
                                    <select class="form-control" id="ownership" name="ownership">
                                      <option value="">-</option>
                                      <option value="CCAN VPN">CCAN VPN</option>
                                      <option value="CCAN Astinet">CCAN Astinet</option>
                                      <option value="Node B">Node B</option>
                                      <option value="CS/Retail">CS/Retail</option>
                                      <option value="Wifi ID">Wifi ID</option>
                                    </select>
                                </div>
                                <div class="col-md-4">
                                    <label>No Inet / Voice</label>
                                    <input type="text" class="form-control form-control-user" name="inet_voice_customer">
                                </div>
                                <div class="col-md-4">
                                    <label>QR Code</label>
                                    <input type="text" class="form-control form-control-user" name="qr_code">
                                </div>
                          </div>
                          <div class="row">
                                <div class="col-md-12">
                                    <label>Keterangan</label>
                                    <textarea type="text" class="form-control form-control-user" name="description"></textarea>
                                </div>
                          </div>

                          <div class="row">
                                <div class="col-md-4">
                                  <label for="update_uim">Update UIM</label>
                                    <select class="form-control" id="update_uim" name="update_uim">
                                      <option value="">-</option>
                                      <option value="Done">Done</option>
                                      <option value="Inputan">Inputan</option>
                                      <option value="Disconnect">Disconnect</option>
                                      <option value="Nomor Tidak Detek">Nomor Tidak Detek</option>
                                      <option value="Node B">Node B</option>
                                      <option value="CCAN">CCAN</option>
                                      <option value="Wifi ID">Wifi ID</option>
                                      <option value="ASTINET">ASTINET</option>
                                      <option value="VPN">VPN</option>
                                    </select>
                                </div>
                                <div class="col-md-4">
                                  <label for="update_dava">Update Dava</label>
                                    <select class="form-control" id="update_dava" name="input_dava">
                                      <option value="">-</option>
                                      <option value="Done">Done</option>
                                      <option value="Qrcode Not Found">Qrcode Not Found</option>
                                      <option value="Relabel">Relabel</option>
                                      <option value="Qrcode Sudah Terpakai">Qrcode Sudah Terpakai</option>
                                      <option value="Sudah Terlabel">Sudah Terlabel</option>
                                    </select>
                                </div>
                                <div class="col-md-4">
                                    <label>Port UIM</label>
                                    <input type="text" class="form-control form-control-user" name="port_uim">
                                </div>
                                </div>
                          <div class="row">
                                <div class="col-md-4">
                                    <label>DC Dismantled</label>
                                    <input type="text" class="form-control form-control-user" name="dc_dismantled">
                                </div>                          
                                <div class="col-md-4">
                                  <label for="trouble">Kendala</label>
                                    <select class="form-control" id="trouble" name="trouble">
                                      <option value="">-</option>
                                      <option value="Disconnect">Disconnect</option>
                                      <option value="Redaman Tinggi">Redaman Tinggi</option>
                                      <option value="Service Stuck BI">Service Stuck BI</option>
                                      <option value="Tidak Detek Full">Tidak Detek Full</option>
                                      <option value="Tidak Detek Sebagian">Tidak Detek Sebagian</option>
                                      <option value="Splitter">Splitter</option>
                                      <option value="Kapasitas">Kapasitas</option>
                                      <option value="Tidak Bisa Create New Version">Tidak Bisa Create New Version</option>
                                      <option value="Service Suspend">Service Suspend</option>
                                      <option value="Service Canceled">Service Canceled</option>
                                      <option value="ONT Detect No Service">ONT Detect No Service</option>
                                      <option value="ODP Susah Di Jangkau">ODP Susah Di Jangkau</option>
                                    </select>
                                </div>
                                <div class="col-md-4">
                                  <br>
                                  <button name="submit" type="submit" class="btn btn-success btn-user">Submit</button>
                                </div>
                            </div>
                          </div>
                          </div>
                          </form>
                        <!-- <div class="modal-footer">
                          <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                          <button name="submit" type="submit" class="btn btn-success btn-user">Submit</button>
                        </div> -->
                      </div>
                    </div>
                  </div>   
        </div>


      </div>
      <!-- End of Main Content -->

    </div>
    <!-- End of Content Wrapper -->

  </div>
  <!-- End of Page Wrapper -->

  <!-- Scroll to Top Button-->
  <a class="scroll-to-top rounded" href="<?= base_url()?>#page-top">
    <i class="fas fa-angle-up"></i>
  </a>

  <!-- Logout Modal-->
  <div class="modal fade" id="logoutModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title" id="exampleModalLabel">Ready to Leave?</h5>
          <button class="close" type="button" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">×</span>
          </button>
        </div>
        <div class="modal-body">Select "Logout" below if you are ready to end your current session.</div>
        <div class="modal-footer">
          <button class="btn btn-secondary" type="button" data-dismiss="modal">Cancel</button>
          <a class="btn btn-danger" href="<?= site_url('auth/admin/logout') ?>" class="item">Logout</a>
        </div>
      </div>
    </div>
  </div>


  <!-- Bootstrap core JavaScript-->
  <script src="<?= base_url()?>asset/vendor/jquery/jquery.min.js"></script>
  <script src="<?= base_url()?>asset/vendor/bootstrap/js/bootstrap.bundle.min.js"></script>

  <!-- Core plugin JavaScript-->
  <script src="<?= base_url()?>asset/vendor/jquery-easing/jquery.easing.min.js"></script>

  <!-- Custom scripts for all pages-->
  <script src="<?= base_url()?>asset/js/sb-admin-2.min.js"></script>
  <!-- Page level plugins -->
  <script src="<?= base_url()?>asset/vendor/datatables/jquery.dataTables.min.js"></script>
  <script src="<?= base_url()?>asset/vendor/datatables/dataTables.bootstrap4.min.js"></script>

  <!-- Page level custom scripts -->
  <script src="<?= base_url()?>asset/js/demo/datatables-demo.js"></script>
            
                    
      <script type="text/javascript">
          $(document).ready(function() {
              $('#odp-table').DataTable();
          });
      </script>

</body>

</html>