<?php

class Admin extends Modular
{

    function index()
    {
        $this->load->view('pages/auth/login');
    }

    public function login(){
        redirectIfAuthenticated();
        $this->load->model('Admin_model');
        $username = $this->input->post('username');
        $password = $this->input->post('password');
        // $date = $this->input->post('date');


        if (isset($_POST['submit'])) {
            $result = $this->Admin_model->loginByUsername($username,$password);
            // $result = $this->Admin_model->register($username,$password);
            if ($result) {
                return true;
             }else {
                $this->load->view('auth/pages/auth/login');
                echo "<script>alert('Username tidak ada dalam sistem')</script>";
                return false;
            } 
        } else {
            $this->load->view('auth/pages/auth/login');
            return false;
        }
    }

    public function logout(){
        $this->session->unset_userdata('logged_in');
        $this->session->sess_destroy();
        redirect('auth/admin/login');
    }

}
