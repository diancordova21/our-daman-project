<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Master_odp extends CI_Model{

    private $table = "t_master_odp";

    public function __construct()
	{
	    parent::__construct();
	    $this->load->database();
	}


    public function get_odp()
    {
         return $this->db->get("t_master_odp");
    }

    function getById($id)
	{
	    return $this->db->get_where($this->table, ["id_odp" => $id])->row();
    }
    
    function UpdateById($id)
    {
        $data = array
            (
                'odp' => $this->input->post('nama_odp'),
                'datel' => $this->input->post('datel'),
                'sto' => $this->input->post('sto'),
                'latitude' => $this->input->post('latitude'),
                'longitude' => $this->input->post('longitude'),
                'alamat' => $this->input->post('alamat'),
                'avai' => $this->input->post('available'),
                'reserved' => $this->input->post('reserved'),
                'in_service' => $this->input->post('in_service'),
                'total' => $this->input->post('total'),
                'tanggal_golive' => $this->input->post('tanggal_golive'),
                'bulan' => $this->input->post('bulan'),
                'tahun' => $this->input->post('tahun'),
                'kelurahan' => $this->input->post('kelurahan'),
                'kecamatan' => $this->input->post('kecamatan'),
                'kota_kab' => $this->input->post('kota_kab'),
                'qrcode_odp' => $this->input->post('qrcode_odp'),
                'qrcode_port' => $this->input->post('qrcode_port'),
                'project' => $this->input->post('project'),
                'vendor' => $this->input->post('vendor'),
                'merk' => $this->input->post('merk'),
                'ip' => $this->input->post('ip')
                
            );
            // print_r($data);exit();
                $this->db->where('id_odp',$id);
                $this->db->update('t_master_odp',$data);
            
    }

    function hapus($id)
	{
		$hasil = $this->db->delete($this->table, array("id_odp" => $id));
		return $hasil;
    }
    
    function getBySTO($sto)
	{
        $this->db->where('sto', $sto);
        $hasil = $this->db->get('t_master_odp');
	    return $hasil->result();
    }

    function ambil_sto($sto)
	{
        $this->db->where('sto', $sto);
        $hasil = $this->db->get('t_master_odp');
        return $hasil->row();        
    }

    function jumlah_odp()
	{
        $query = $this->db->get('t_master_odp');
        return $query->num_rows();
        
    }

    
    function jumlah_datel()
	{
        $this->db->select('id_odp, odp, COUNT(datel) as total');
        $this->db->group_by('datel'); 
        $this->db->order_by('total', 'desc'); 
        $hasil = $this->db->get('t_master_odp');
        return $hasil->num_rows();
    }
    function jumlah_odp_persto()
    {
        $this->db->select('sto, COUNT(*) as jumlah_odp');
        $this->db->group_by('sto');
        // $this->db->order_by('jumlah_odp'); 
        // 
        $query = $this->db->get('t_master_odp');
        // print_r($query ->result());exit();
        
        return $query->result();


       

    }
    function jumlah_sto()
	{
        $this->db->select('id_odp, odp, COUNT(sto) as total');
        $this->db->group_by('sto'); 
        $this->db->order_by('total', 'desc'); 
        $hasil = $this->db->get('t_master_odp');
        return $hasil->num_rows();
    }
    
    function jumlah_golive()
	{
        $belum = '0000-00-00';
        $this->db->select('id_odp, odp, COUNT(tanggal_golive) as total');
        $this->db->where('tanggal_golive !=', $belum);
        $this->db->group_by('odp'); 
        $this->db->order_by('total', 'desc'); 
        $hasil = $this->db->get('t_master_odp');
        return $hasil->num_rows();
    }

    function golive_2014()
	{
        $query   = $this->db->query('SELECT COUNT(tahun) as golive_2014,
                                        WHERE tahun = "2014"');
        return $query->row();
    }

    function golive_2015()
	{
        $query   = $this->db->query('SELECT COUNT(tahun) as golive_2015,
                                        FROM t_master_odp
                                        WHERE tahun = "2015"');
        return $query->row();
    }

    function golive_2016()
	{
        $query   = $this->db->query('SELECT COUNT(tahun) as golive_2016,
                                        FROM t_master_odp
                                        WHERE tahun = "2016"');
        return $query->row();
    }

    function golive_2017()
	{
        $query   = $this->db->query('SELECT COUNT(tahun) as golive_2017,
                                        FROM t_master_odp
                                        WHERE tahun = "2017"');
        return $query->row();
    }

    function golive_2018()
	{
        $query   = $this->db->query('SELECT COUNT(tahun) as golive_2018,
                                        FROM t_master_odp
                                        WHERE tahun = "2018"');
        return $query->row();
    }

    function golive_2019()
	{
        $query   = $this->db->query('SELECT COUNT(tahun) as golive_2019,
                                        FROM t_master_odp
                                        WHERE tahun = "2019"');
        return $query->row();
    }

    function golive_2020()
	{
        $query   = $this->db->query('SELECT COUNT(tahun) as golive_2020,
                                        FROM t_master_odp
                                        WHERE tahun = "2020"');
        return $query->row();
    }

    function jumlah_lawang()
	{
        $query   = $this->db->query('SELECT COUNT(odp) as total_odp,                                        
                                        SUM( avai) total_avai, 
                                        SUM( in_service ) total_in_service, 
                                        SUM( reserved) total_reserved, 
                                        SUM( total) total_total,
                                        (SELECT COUNT(tahun)
                                        FROM t_master_odp
                                        WHERE tahun = "2014" AND sto = "LWG") as golive_2014,
                                        (SELECT COUNT(tahun)
                                        FROM t_master_odp
                                        WHERE tahun = "2015" AND sto = "LWG") as golive_2015,
                                        (SELECT COUNT(tahun)
                                        FROM t_master_odp
                                        WHERE tahun = "2016" AND sto = "LWG") as golive_2016,
                                        (SELECT COUNT(tahun)
                                        FROM t_master_odp
                                        WHERE tahun = "2017" AND sto = "LWG") as golive_2017,
                                        (SELECT COUNT(tahun)
                                        FROM t_master_odp
                                        WHERE tahun = "2018" AND sto = "LWG") as golive_2018,
                                        (SELECT COUNT(tahun)
                                        FROM t_master_odp
                                        WHERE tahun = "2019" AND sto = "LWG") as golive_2019,
                                        (SELECT COUNT(tahun)
                                        FROM t_master_odp
                                        WHERE tahun = "2020" AND sto = "LWG") as golive_2020
                                                                             
                                        FROM t_master_odp
                                        WHERE sto = "LWG"');

                                       
        // print_r($query->row());
        // exit();

        return $query->row();


        // return array(
        //     'query' => $query->row(),
        //     'golive_2020' => $golive_2020->row(),
        // );

    }

    function jumlah_singosari()
	{
        $query   = $this->db->query('SELECT COUNT(odp) as total_odp, 
                                        SUM( avai) total_avai, 
                                        SUM( in_service ) total_in_service, 
                                        SUM( reserved) total_reserved, 
                                        SUM( total) total_total,
                                        (SELECT COUNT(tahun)
                                        FROM t_master_odp
                                        WHERE tahun = "2014" AND sto = "SGS") as golive_2014,
                                        (SELECT COUNT(tahun)
                                        FROM t_master_odp
                                        WHERE tahun = "2015" AND sto = "SGS") as golive_2015,
                                        (SELECT COUNT(tahun)
                                        FROM t_master_odp
                                        WHERE tahun = "2016" AND sto = "SGS") as golive_2016,
                                        (SELECT COUNT(tahun)
                                        FROM t_master_odp
                                        WHERE tahun = "2017" AND sto = "SGS") as golive_2017,
                                        (SELECT COUNT(tahun)
                                        FROM t_master_odp
                                        WHERE tahun = "2018" AND sto = "SGS") as golive_2018,
                                        (SELECT COUNT(tahun)
                                        FROM t_master_odp
                                        WHERE tahun = "2019" AND sto = "SGS") as golive_2019,
                                        (SELECT COUNT(tahun)
                                        FROM t_master_odp
                                        WHERE tahun = "2020" AND sto = "SGS") as golive_2020
                                        
                                        FROM t_master_odp
                                        WHERE sto = "SGS"');
        return $query->row();
    }

    function jumlah_pakis()
	{
        $query   = $this->db->query('SELECT COUNT(odp) as total_odp, 
                                        SUM( avai) total_avai, 
                                        SUM( in_service ) total_in_service, 
                                        SUM( reserved) total_reserved, 
                                        SUM( total) total_total,
                                        (SELECT COUNT(tahun)
                                        FROM t_master_odp
                                        WHERE tahun = "2014" AND sto = "PKS") as golive_2014,
                                        (SELECT COUNT(tahun)
                                        FROM t_master_odp
                                        WHERE tahun = "2015" AND sto = "PKS") as golive_2015,
                                        (SELECT COUNT(tahun)
                                        FROM t_master_odp
                                        WHERE tahun = "2016" AND sto = "PKS") as golive_2016,
                                        (SELECT COUNT(tahun)
                                        FROM t_master_odp
                                        WHERE tahun = "2017" AND sto = "PKS") as golive_2017,
                                        (SELECT COUNT(tahun)
                                        FROM t_master_odp
                                        WHERE tahun = "2018" AND sto = "PKS") as golive_2018,
                                        (SELECT COUNT(tahun)
                                        FROM t_master_odp
                                        WHERE tahun = "2019" AND sto = "PKS") as golive_2019,
                                        (SELECT COUNT(tahun)
                                        FROM t_master_odp
                                        WHERE tahun = "2020" AND sto = "PKS") as golive_2020
                                        FROM t_master_odp
                                        WHERE sto = "PKS"');
        return $query->row();
    }

    function jumlah_sawojajar()
	{
        $query   = $this->db->query('SELECT COUNT(odp) as total_odp, 
                                        SUM( avai) total_avai, 
                                        SUM( in_service ) total_in_service, 
                                        SUM( reserved) total_reserved, 
                                        SUM( total) total_total,
                                        (SELECT COUNT(tahun)
                                        FROM t_master_odp
                                        WHERE tahun = "2014" AND sto = "SWJ") as golive_2014,
                                        (SELECT COUNT(tahun)
                                        FROM t_master_odp
                                        WHERE tahun = "2015" AND sto = "SWJ") as golive_2015,
                                        (SELECT COUNT(tahun)
                                        FROM t_master_odp
                                        WHERE tahun = "2016" AND sto = "SWJ") as golive_2016,
                                        (SELECT COUNT(tahun)
                                        FROM t_master_odp
                                        WHERE tahun = "2017" AND sto = "SWJ") as golive_2017,
                                        (SELECT COUNT(tahun)
                                        FROM t_master_odp
                                        WHERE tahun = "2018" AND sto = "SWJ") as golive_2018,
                                        (SELECT COUNT(tahun)
                                        FROM t_master_odp
                                        WHERE tahun = "2019" AND sto = "SWJ") as golive_2019,
                                        (SELECT COUNT(tahun)
                                        FROM t_master_odp
                                        WHERE tahun = "2020" AND sto = "SWJ") as golive_2020
                                        FROM t_master_odp
                                        WHERE sto = "SWJ"');
        return $query->row();
    }

    function jumlah_tumpang()
	{
        $query   = $this->db->query('SELECT COUNT(odp) as total_odp, 
                                        SUM( avai) total_avai, 
                                        SUM( in_service ) total_in_service, 
                                        SUM( reserved) total_reserved, 
                                        SUM( total) total_total,
                                        (SELECT COUNT(tahun)
                                        FROM t_master_odp
                                        WHERE tahun = "2014" AND sto = "TMP") as golive_2014,
                                        (SELECT COUNT(tahun)
                                        FROM t_master_odp
                                        WHERE tahun = "2015" AND sto = "TMP") as golive_2015,
                                        (SELECT COUNT(tahun)
                                        FROM t_master_odp
                                        WHERE tahun = "2016" AND sto = "TMP") as golive_2016,
                                        (SELECT COUNT(tahun)
                                        FROM t_master_odp
                                        WHERE tahun = "2017" AND sto = "TMP") as golive_2017,
                                        (SELECT COUNT(tahun)
                                        FROM t_master_odp
                                        WHERE tahun = "2018" AND sto = "TMP") as golive_2018,
                                        (SELECT COUNT(tahun)
                                        FROM t_master_odp
                                        WHERE tahun = "2019" AND sto = "TMP") as golive_2019,
                                        (SELECT COUNT(tahun)
                                        FROM t_master_odp
                                        WHERE tahun = "2020" AND sto = "TMP") as golive_2020
                                        FROM t_master_odp
                                        WHERE sto = "TMP"');
        return $query->row();
    }

    function jumlah_blimbing()
	{
        $query   = $this->db->query('SELECT COUNT(odp) as total_odp, 
                                        SUM( avai) total_avai, 
                                        SUM( in_service ) total_in_service, 
                                        SUM( reserved) total_reserved, 
                                        SUM( total) total_total,
                                        (SELECT COUNT(tahun)
                                        FROM t_master_odp
                                        WHERE tahun = "2014" AND sto = "BLB") as golive_2014,
                                        (SELECT COUNT(tahun)
                                        FROM t_master_odp
                                        WHERE tahun = "2015" AND sto = "BLB") as golive_2015,
                                        (SELECT COUNT(tahun)
                                        FROM t_master_odp
                                        WHERE tahun = "2016" AND sto = "BLB") as golive_2016,
                                        (SELECT COUNT(tahun)
                                        FROM t_master_odp
                                        WHERE tahun = "2017" AND sto = "BLB") as golive_2017,
                                        (SELECT COUNT(tahun)
                                        FROM t_master_odp
                                        WHERE tahun = "2018" AND sto = "BLB") as golive_2018,
                                        (SELECT COUNT(tahun)
                                        FROM t_master_odp
                                        WHERE tahun = "2019" AND sto = "BLB") as golive_2019,
                                        (SELECT COUNT(tahun)
                                        FROM t_master_odp
                                        WHERE tahun = "2020" AND sto = "BLB") as golive_2020
                                        FROM t_master_odp
                                        WHERE sto = "BLB"');
        return $query->row();
    }

    function jumlah_klojen()
	{
        $query   = $this->db->query('SELECT COUNT(odp) as total_odp, 
                                        SUM( avai) total_avai, 
                                        SUM( in_service ) total_in_service, 
                                        SUM( reserved) total_reserved, 
                                        SUM( total) total_total,
                                        (SELECT COUNT(tahun)
                                        FROM t_master_odp
                                        WHERE tahun = "2014" AND sto = "KLJ") as golive_2014,
                                        (SELECT COUNT(tahun)
                                        FROM t_master_odp
                                        WHERE tahun = "2015" AND sto = "KLJ") as golive_2015,
                                        (SELECT COUNT(tahun)
                                        FROM t_master_odp
                                        WHERE tahun = "2016" AND sto = "KLJ") as golive_2016,
                                        (SELECT COUNT(tahun)
                                        FROM t_master_odp
                                        WHERE tahun = "2017" AND sto = "KLJ") as golive_2017,
                                        (SELECT COUNT(tahun)
                                        FROM t_master_odp
                                        WHERE tahun = "2018" AND sto = "KLJ") as golive_2018,
                                        (SELECT COUNT(tahun)
                                        FROM t_master_odp
                                        WHERE tahun = "2019" AND sto = "KLJ") as golive_2019,
                                        (SELECT COUNT(tahun)
                                        FROM t_master_odp
                                        WHERE tahun = "2020" AND sto = "KLJ") as golive_2020
                                        FROM t_master_odp
                                        WHERE sto = "KLJ"');
        return $query->row();
    }

    function jumlah_malangkota()
	{
        $query   = $this->db->query('SELECT COUNT(odp) as total_odp, 
                                        SUM( avai) total_avai, 
                                        SUM( in_service ) total_in_service, 
                                        SUM( reserved) total_reserved, 
                                        SUM( total) total_total,
                                        (SELECT COUNT(tahun)
                                        FROM t_master_odp
                                        WHERE tahun = "2014" AND sto = "MLG") as golive_2014,
                                        (SELECT COUNT(tahun)
                                        FROM t_master_odp
                                        WHERE tahun = "2015" AND sto = "MLG") as golive_2015,
                                        (SELECT COUNT(tahun)
                                        FROM t_master_odp
                                        WHERE tahun = "2016" AND sto = "MLG") as golive_2016,
                                        (SELECT COUNT(tahun)
                                        FROM t_master_odp
                                        WHERE tahun = "2017" AND sto = "MLG") as golive_2017,
                                        (SELECT COUNT(tahun)
                                        FROM t_master_odp
                                        WHERE tahun = "2018" AND sto = "MLG") as golive_2018,
                                        (SELECT COUNT(tahun)
                                        FROM t_master_odp
                                        WHERE tahun = "2019" AND sto = "MLG") as golive_2019,
                                        (SELECT COUNT(tahun)
                                        FROM t_master_odp
                                        WHERE tahun = "2020" AND sto = "MLG") as golive_2020
                                        FROM t_master_odp
                                        WHERE sto = "MLG"');
        return $query->row();
    }

    function jumlah_gadang()
	{
        $query   = $this->db->query('SELECT COUNT(odp) as total_odp, 
                                        SUM( avai) total_avai, 
                                        SUM( in_service ) total_in_service, 
                                        SUM( reserved) total_reserved, 
                                        SUM( total) total_total,
                                        (SELECT COUNT(tahun)
                                        FROM t_master_odp
                                        WHERE tahun = "2014" AND sto = "GDG") as golive_2014,
                                        (SELECT COUNT(tahun)
                                        FROM t_master_odp
                                        WHERE tahun = "2015" AND sto = "GDG") as golive_2015,
                                        (SELECT COUNT(tahun)
                                        FROM t_master_odp
                                        WHERE tahun = "2016" AND sto = "GDG") as golive_2016,
                                        (SELECT COUNT(tahun)
                                        FROM t_master_odp
                                        WHERE tahun = "2017" AND sto = "GDG") as golive_2017,
                                        (SELECT COUNT(tahun)
                                        FROM t_master_odp
                                        WHERE tahun = "2018" AND sto = "GDG") as golive_2018,
                                        (SELECT COUNT(tahun)
                                        FROM t_master_odp
                                        WHERE tahun = "2019" AND sto = "GDG") as golive_2019,
                                        (SELECT COUNT(tahun)
                                        FROM t_master_odp
                                        WHERE tahun = "2020" AND sto = "GDG") as golive_2020
                                        FROM t_master_odp
                                        WHERE sto = "GDG"');
        return $query->row();
    }

    function jumlah_buring()
	{
        $query   = $this->db->query('SELECT COUNT(odp) as total_odp, 
                                        SUM( avai) total_avai, 
                                        SUM( in_service ) total_in_service, 
                                        SUM( reserved) total_reserved, 
                                        SUM( total) total_total,
                                        (SELECT COUNT(tahun)
                                        FROM t_master_odp
                                        WHERE tahun = "2014" AND sto = "BRG") as golive_2014,
                                        (SELECT COUNT(tahun)
                                        FROM t_master_odp
                                        WHERE tahun = "2015" AND sto = "BRG") as golive_2015,
                                        (SELECT COUNT(tahun)
                                        FROM t_master_odp
                                        WHERE tahun = "2016" AND sto = "BRG") as golive_2016,
                                        (SELECT COUNT(tahun)
                                        FROM t_master_odp
                                        WHERE tahun = "2017" AND sto = "BRG") as golive_2017,
                                        (SELECT COUNT(tahun)
                                        FROM t_master_odp
                                        WHERE tahun = "2018" AND sto = "BRG") as golive_2018,
                                        (SELECT COUNT(tahun)
                                        FROM t_master_odp
                                        WHERE tahun = "2019" AND sto = "BRG") as golive_2019,
                                        (SELECT COUNT(tahun)
                                        FROM t_master_odp
                                        WHERE tahun = "2020" AND sto = "BRG") as golive_2020
                                        FROM t_master_odp
                                        WHERE sto = "BRG"');
        return $query->row();
    }

    function jumlah_dampit()
	{
        $query   = $this->db->query('SELECT COUNT(odp) as total_odp, 
                                        SUM( avai) total_avai, 
                                        SUM( in_service ) total_in_service, 
                                        SUM( reserved) total_reserved, 
                                        SUM( total) total_total,
                                        (SELECT COUNT(tahun)
                                        FROM t_master_odp
                                        WHERE tahun = "2014" AND sto = "DPT") as golive_2014,
                                        (SELECT COUNT(tahun)
                                        FROM t_master_odp
                                        WHERE tahun = "2015" AND sto = "DPT") as golive_2015,
                                        (SELECT COUNT(tahun)
                                        FROM t_master_odp
                                        WHERE tahun = "2016" AND sto = "DPT") as golive_2016,
                                        (SELECT COUNT(tahun)
                                        FROM t_master_odp
                                        WHERE tahun = "2017" AND sto = "DPT") as golive_2017,
                                        (SELECT COUNT(tahun)
                                        FROM t_master_odp
                                        WHERE tahun = "2018" AND sto = "DPT") as golive_2018,
                                        (SELECT COUNT(tahun)
                                        FROM t_master_odp
                                        WHERE tahun = "2019" AND sto = "DPT") as golive_2019,
                                        (SELECT COUNT(tahun)
                                        FROM t_master_odp
                                        WHERE tahun = "2020" AND sto = "DPT") as golive_2020
                                        FROM t_master_odp
                                        WHERE sto = "DPT"');
        return $query->row();
    }

    function jumlah_kepanjen()
	{
        $query   = $this->db->query('SELECT COUNT(odp) as total_odp, 
                                        SUM( avai) total_avai, 
                                        SUM( in_service ) total_in_service, 
                                        SUM( reserved) total_reserved, 
                                        SUM( total) total_total,
                                        (SELECT COUNT(tahun)
                                        FROM t_master_odp
                                        WHERE tahun = "2014" AND sto = "KEP") as golive_2014,
                                        (SELECT COUNT(tahun)
                                        FROM t_master_odp
                                        WHERE tahun = "2015" AND sto = "KEP") as golive_2015,
                                        (SELECT COUNT(tahun)
                                        FROM t_master_odp
                                        WHERE tahun = "2016" AND sto = "KEP") as golive_2016,
                                        (SELECT COUNT(tahun)
                                        FROM t_master_odp
                                        WHERE tahun = "2017" AND sto = "KEP") as golive_2017,
                                        (SELECT COUNT(tahun)
                                        FROM t_master_odp
                                        WHERE tahun = "2018" AND sto = "KEP") as golive_2018,
                                        (SELECT COUNT(tahun)
                                        FROM t_master_odp
                                        WHERE tahun = "2019" AND sto = "KEP") as golive_2019,
                                        (SELECT COUNT(tahun)
                                        FROM t_master_odp
                                        WHERE tahun = "2020" AND sto = "KEP") as golive_2020
                                        FROM t_master_odp
                                        WHERE sto = "KEP"');
        return $query->row();
    }

    function jumlah_turen()
	{
        $query   = $this->db->query('SELECT COUNT(odp) as total_odp, 
                                        SUM( avai) total_avai, 
                                        SUM( in_service ) total_in_service, 
                                        SUM( reserved) total_reserved, 
                                        SUM( total) total_total,
                                        (SELECT COUNT(tahun)
                                        FROM t_master_odp
                                        WHERE tahun = "2014" AND sto = "TUR") as golive_2014,
                                        (SELECT COUNT(tahun)
                                        FROM t_master_odp
                                        WHERE tahun = "2015" AND sto = "TUR") as golive_2015,
                                        (SELECT COUNT(tahun)
                                        FROM t_master_odp
                                        WHERE tahun = "2016" AND sto = "TUR") as golive_2016,
                                        (SELECT COUNT(tahun)
                                        FROM t_master_odp
                                        WHERE tahun = "2017" AND sto = "TUR") as golive_2017,
                                        (SELECT COUNT(tahun)
                                        FROM t_master_odp
                                        WHERE tahun = "2018" AND sto = "TUR") as golive_2018,
                                        (SELECT COUNT(tahun)
                                        FROM t_master_odp
                                        WHERE tahun = "2019" AND sto = "TUR") as golive_2019,
                                        (SELECT COUNT(tahun)
                                        FROM t_master_odp
                                        WHERE tahun = "2020" AND sto = "TUR") as golive_2020
                                        FROM t_master_odp
                                        WHERE sto = "TUR"');
        return $query->row();
    }

    function jumlah_gondanglegi()
	{
        $query   = $this->db->query('SELECT COUNT(odp) as total_odp, 
                                        SUM( avai) total_avai, 
                                        SUM( in_service ) total_in_service, 
                                        SUM( reserved) total_reserved, 
                                        SUM( total) total_total,
                                        (SELECT COUNT(tahun)
                                        FROM t_master_odp
                                        WHERE tahun = "2014" AND sto = "GDI") as golive_2014,
                                        (SELECT COUNT(tahun)
                                        FROM t_master_odp
                                        WHERE tahun = "2015" AND sto = "GDI") as golive_2015,
                                        (SELECT COUNT(tahun)
                                        FROM t_master_odp
                                        WHERE tahun = "2016" AND sto = "GDI") as golive_2016,
                                        (SELECT COUNT(tahun)
                                        FROM t_master_odp
                                        WHERE tahun = "2017" AND sto = "GDI") as golive_2017,
                                        (SELECT COUNT(tahun)
                                        FROM t_master_odp
                                        WHERE tahun = "2018" AND sto = "GDI") as golive_2018,
                                        (SELECT COUNT(tahun)
                                        FROM t_master_odp
                                        WHERE tahun = "2019" AND sto = "GDI") as golive_2019,
                                        (SELECT COUNT(tahun)
                                        FROM t_master_odp
                                        WHERE tahun = "2020" AND sto = "GDI") as golive_2020
                                        FROM t_master_odp
                                        WHERE sto = "GDI"');
        return $query->row();
    }

    function jumlah_sumbermanjing()
	{
        $query   = $this->db->query('SELECT COUNT(odp) as total_odp, 
                                        SUM( avai) total_avai, 
                                        SUM( in_service ) total_in_service, 
                                        SUM( reserved) total_reserved, 
                                        SUM( total) total_total,
                                        (SELECT COUNT(tahun)
                                        FROM t_master_odp
                                        WHERE tahun = "2014" AND sto = "SBM") as golive_2014,
                                        (SELECT COUNT(tahun)
                                        FROM t_master_odp
                                        WHERE tahun = "2015" AND sto = "SBM") as golive_2015,
                                        (SELECT COUNT(tahun)
                                        FROM t_master_odp
                                        WHERE tahun = "2016" AND sto = "SBM") as golive_2016,
                                        (SELECT COUNT(tahun)
                                        FROM t_master_odp
                                        WHERE tahun = "2017" AND sto = "SBM") as golive_2017,
                                        (SELECT COUNT(tahun)
                                        FROM t_master_odp
                                        WHERE tahun = "2018" AND sto = "SBM") as golive_2018,
                                        (SELECT COUNT(tahun)
                                        FROM t_master_odp
                                        WHERE tahun = "2019" AND sto = "SBM") as golive_2019,
                                        (SELECT COUNT(tahun)
                                        FROM t_master_odp
                                        WHERE tahun = "2020" AND sto = "SBM") as golive_2020
                                        FROM t_master_odp
                                        WHERE sto = "SBM"');
        return $query->row();
    }

    function jumlah_sumberpucung()
	{
        $query   = $this->db->query('SELECT COUNT(odp) as total_odp, 
                                        SUM( avai) total_avai, 
                                        SUM( in_service ) total_in_service, 
                                        SUM( reserved) total_reserved, 
                                        SUM( total) total_total,
                                        (SELECT COUNT(tahun)
                                        FROM t_master_odp
                                        WHERE tahun = "2014" AND sto = "SBP") as golive_2014,
                                        (SELECT COUNT(tahun)
                                        FROM t_master_odp
                                        WHERE tahun = "2015" AND sto = "SBP") as golive_2015,
                                        (SELECT COUNT(tahun)
                                        FROM t_master_odp
                                        WHERE tahun = "2016" AND sto = "SBP") as golive_2016,
                                        (SELECT COUNT(tahun)
                                        FROM t_master_odp
                                        WHERE tahun = "2017" AND sto = "SBP") as golive_2017,
                                        (SELECT COUNT(tahun)
                                        FROM t_master_odp
                                        WHERE tahun = "2018" AND sto = "SBP") as golive_2018,
                                        (SELECT COUNT(tahun)
                                        FROM t_master_odp
                                        WHERE tahun = "2019" AND sto = "SBP") as golive_2019,
                                        (SELECT COUNT(tahun)
                                        FROM t_master_odp
                                        WHERE tahun = "2020" AND sto = "SBP") as golive_2020
                                        FROM t_master_odp
                                        WHERE sto = "SBP"');
        return $query->row();
    }

    function jumlah_pagak()
	{
        $query   = $this->db->query('SELECT COUNT(odp) as total_odp, 
                                        SUM( avai) total_avai, 
                                        SUM( in_service ) total_in_service, 
                                        SUM( reserved) total_reserved, 
                                        SUM( total) total_total,
                                        (SELECT COUNT(tahun)
                                        FROM t_master_odp
                                        WHERE tahun = "2014" AND sto = "PGK") as golive_2014,
                                        (SELECT COUNT(tahun)
                                        FROM t_master_odp
                                        WHERE tahun = "2015" AND sto = "PGK") as golive_2015,
                                        (SELECT COUNT(tahun)
                                        FROM t_master_odp
                                        WHERE tahun = "2016" AND sto = "PGK") as golive_2016,
                                        (SELECT COUNT(tahun)
                                        FROM t_master_odp
                                        WHERE tahun = "2017" AND sto = "PGK") as golive_2017,
                                        (SELECT COUNT(tahun)
                                        FROM t_master_odp
                                        WHERE tahun = "2018" AND sto = "PGK") as golive_2018,
                                        (SELECT COUNT(tahun)
                                        FROM t_master_odp
                                        WHERE tahun = "2019" AND sto = "PGK") as golive_2019,
                                        (SELECT COUNT(tahun)
                                        FROM t_master_odp
                                        WHERE tahun = "2020" AND sto = "PGK") as golive_2020
                                        FROM t_master_odp
                                        WHERE sto = "PGK"');
        return $query->row();
    }
    
    function jumlah_ampelgading()
	{
        $query   = $this->db->query('SELECT COUNT(odp) as total_odp, 
                                        SUM( avai) total_avai, 
                                        SUM( in_service ) total_in_service, 
                                        SUM( reserved) total_reserved, 
                                        SUM( total) total_total,
                                        (SELECT COUNT(tahun)
                                        FROM t_master_odp
                                        WHERE tahun = "2014" AND sto = "APG") as golive_2014,
                                        (SELECT COUNT(tahun)
                                        FROM t_master_odp
                                        WHERE tahun = "2015" AND sto = "APG") as golive_2015,
                                        (SELECT COUNT(tahun)
                                        FROM t_master_odp
                                        WHERE tahun = "2016" AND sto = "APG") as golive_2016,
                                        (SELECT COUNT(tahun)
                                        FROM t_master_odp
                                        WHERE tahun = "2017" AND sto = "APG") as golive_2017,
                                        (SELECT COUNT(tahun)
                                        FROM t_master_odp
                                        WHERE tahun = "2018" AND sto = "APG") as golive_2018,
                                        (SELECT COUNT(tahun)
                                        FROM t_master_odp
                                        WHERE tahun = "2019" AND sto = "APG") as golive_2019,
                                        (SELECT COUNT(tahun)
                                        FROM t_master_odp
                                        WHERE tahun = "2020" AND sto = "APG") as golive_2020
                                        FROM t_master_odp
                                        WHERE sto = "APG"');
        return $query->row();
    }

    function jumlah_gunungkawi()
	{
        $query   = $this->db->query('SELECT COUNT(odp) as total_odp, 
                                        SUM( avai) total_avai, 
                                        SUM( in_service ) total_in_service, 
                                        SUM( reserved) total_reserved, 
                                        SUM( total) total_total,
                                        (SELECT COUNT(tahun)
                                        FROM t_master_odp
                                        WHERE tahun = "2014" AND sto = "GKW") as golive_2014,
                                        (SELECT COUNT(tahun)
                                        FROM t_master_odp
                                        WHERE tahun = "2015" AND sto = "GKW") as golive_2015,
                                        (SELECT COUNT(tahun)
                                        FROM t_master_odp
                                        WHERE tahun = "2016" AND sto = "GKW") as golive_2016,
                                        (SELECT COUNT(tahun)
                                        FROM t_master_odp
                                        WHERE tahun = "2017" AND sto = "GKW") as golive_2017,
                                        (SELECT COUNT(tahun)
                                        FROM t_master_odp
                                        WHERE tahun = "2018" AND sto = "GKW") as golive_2018,
                                        (SELECT COUNT(tahun)
                                        FROM t_master_odp
                                        WHERE tahun = "2019" AND sto = "GKW") as golive_2019,
                                        (SELECT COUNT(tahun)
                                        FROM t_master_odp
                                        WHERE tahun = "2020" AND sto = "GKW") as golive_2020
                                        FROM t_master_odp
                                        WHERE sto = "GKW"');
        return $query->row();
    }

    function jumlah_bantur()
	{
        $query   = $this->db->query('SELECT COUNT(odp) as total_odp, 
                                        SUM( avai) total_avai, 
                                        SUM( in_service ) total_in_service, 
                                        SUM( reserved) total_reserved, 
                                        SUM( total) total_total,
                                        (SELECT COUNT(tahun)
                                        FROM t_master_odp
                                        WHERE tahun = "2014" AND sto = "BNR") as golive_2014,
                                        (SELECT COUNT(tahun)
                                        FROM t_master_odp
                                        WHERE tahun = "2015" AND sto = "BNR") as golive_2015,
                                        (SELECT COUNT(tahun)
                                        FROM t_master_odp
                                        WHERE tahun = "2016" AND sto = "BNR") as golive_2016,
                                        (SELECT COUNT(tahun)
                                        FROM t_master_odp
                                        WHERE tahun = "2017" AND sto = "BNR") as golive_2017,
                                        (SELECT COUNT(tahun)
                                        FROM t_master_odp
                                        WHERE tahun = "2018" AND sto = "BNR") as golive_2018,
                                        (SELECT COUNT(tahun)
                                        FROM t_master_odp
                                        WHERE tahun = "2019" AND sto = "BNR") as golive_2019,
                                        (SELECT COUNT(tahun)
                                        FROM t_master_odp
                                        WHERE tahun = "2020" AND sto = "BNR") as golive_2020
                                        FROM t_master_odp
                                        WHERE sto = "BNR"');
        return $query->row();
    }

    function jumlah_donomulyo()
	{
        $query   = $this->db->query('SELECT COUNT(odp) as total_odp, 
                                        SUM( avai) total_avai, 
                                        SUM( in_service ) total_in_service, 
                                        SUM( reserved) total_reserved, 
                                        SUM( total) total_total,
                                        (SELECT COUNT(tahun)
                                        FROM t_master_odp
                                        WHERE tahun = "2014" AND sto = "DNO") as golive_2014,
                                        (SELECT COUNT(tahun)
                                        FROM t_master_odp
                                        WHERE tahun = "2015" AND sto = "DNO") as golive_2015,
                                        (SELECT COUNT(tahun)
                                        FROM t_master_odp
                                        WHERE tahun = "2016" AND sto = "DNO") as golive_2016,
                                        (SELECT COUNT(tahun)
                                        FROM t_master_odp
                                        WHERE tahun = "2017" AND sto = "DNO") as golive_2017,
                                        (SELECT COUNT(tahun)
                                        FROM t_master_odp
                                        WHERE tahun = "2018" AND sto = "DNO") as golive_2018,
                                        (SELECT COUNT(tahun)
                                        FROM t_master_odp
                                        WHERE tahun = "2019" AND sto = "DNO") as golive_2019,
                                        (SELECT COUNT(tahun)
                                        FROM t_master_odp
                                        WHERE tahun = "2020" AND sto = "DNO") as golive_2020
                                        FROM t_master_odp
                                        WHERE sto = "DNO"');
        return $query->row();
    }

    function jumlah_karangploso()
	{
        $query   = $this->db->query('SELECT COUNT(odp) as total_odp, 
                                        SUM( avai) total_avai, 
                                        SUM( in_service ) total_in_service, 
                                        SUM( reserved) total_reserved, 
                                        SUM( total) total_total,
                                        (SELECT COUNT(tahun)
                                        FROM t_master_odp
                                        WHERE tahun = "2014" AND sto = "KPO") as golive_2014,
                                        (SELECT COUNT(tahun)
                                        FROM t_master_odp
                                        WHERE tahun = "2015" AND sto = "KPO") as golive_2015,
                                        (SELECT COUNT(tahun)
                                        FROM t_master_odp
                                        WHERE tahun = "2016" AND sto = "KPO") as golive_2016,
                                        (SELECT COUNT(tahun)
                                        FROM t_master_odp
                                        WHERE tahun = "2017" AND sto = "KPO") as golive_2017,
                                        (SELECT COUNT(tahun)
                                        FROM t_master_odp
                                        WHERE tahun = "2018" AND sto = "KPO") as golive_2018,
                                        (SELECT COUNT(tahun)
                                        FROM t_master_odp
                                        WHERE tahun = "2019" AND sto = "KPO") as golive_2019,
                                        (SELECT COUNT(tahun)
                                        FROM t_master_odp
                                        WHERE tahun = "2020" AND sto = "KPO") as golive_2020
                                        FROM t_master_odp
                                        WHERE sto = "KPO"');
        return $query->row();
    }

    function jumlah_batu()
	{
        $query   = $this->db->query('SELECT COUNT(odp) as total_odp, 
                                        SUM( avai) total_avai, 
                                        SUM( in_service ) total_in_service, 
                                        SUM( reserved) total_reserved, 
                                        SUM( total) total_total,
                                        (SELECT COUNT(tahun)
                                        FROM t_master_odp
                                        WHERE tahun = "2014" AND sto = "BTU") as golive_2014,
                                        (SELECT COUNT(tahun)
                                        FROM t_master_odp
                                        WHERE tahun = "2015" AND sto = "BTU") as golive_2015,
                                        (SELECT COUNT(tahun)
                                        FROM t_master_odp
                                        WHERE tahun = "2016" AND sto = "BTU") as golive_2016,
                                        (SELECT COUNT(tahun)
                                        FROM t_master_odp
                                        WHERE tahun = "2017" AND sto = "BTU") as golive_2017,
                                        (SELECT COUNT(tahun)
                                        FROM t_master_odp
                                        WHERE tahun = "2018" AND sto = "BTU") as golive_2018,
                                        (SELECT COUNT(tahun)
                                        FROM t_master_odp
                                        WHERE tahun = "2019" AND sto = "BTU") as golive_2019,
                                        (SELECT COUNT(tahun)
                                        FROM t_master_odp
                                        WHERE tahun = "2020" AND sto = "BTU") as golive_2020
                                        FROM t_master_odp
                                        WHERE sto = "BTU"');
        return $query->row();
    }

    function jumlah_ngantang()
	{
        $query   = $this->db->query('SELECT COUNT(odp) as total_odp, 
                                        SUM( avai) total_avai, 
                                        SUM( in_service ) total_in_service, 
                                        SUM( reserved) total_reserved, 
                                        SUM( total) total_total,
                                        (SELECT COUNT(tahun)
                                        FROM t_master_odp
                                        WHERE tahun = "2014" AND sto = "NTG") as golive_2014,
                                        (SELECT COUNT(tahun)
                                        FROM t_master_odp
                                        WHERE tahun = "2015" AND sto = "NTG") as golive_2015,
                                        (SELECT COUNT(tahun)
                                        FROM t_master_odp
                                        WHERE tahun = "2016" AND sto = "NTG") as golive_2016,
                                        (SELECT COUNT(tahun)
                                        FROM t_master_odp
                                        WHERE tahun = "2017" AND sto = "NTG") as golive_2017,
                                        (SELECT COUNT(tahun)
                                        FROM t_master_odp
                                        WHERE tahun = "2018" AND sto = "NTG") as golive_2018,
                                        (SELECT COUNT(tahun)
                                        FROM t_master_odp
                                        WHERE tahun = "2019" AND sto = "NTG") as golive_2019,
                                        (SELECT COUNT(tahun)
                                        FROM t_master_odp
                                        WHERE tahun = "2020" AND sto = "NTG") as golive_2020
                                        FROM t_master_odp
                                        WHERE sto = "NTG"');
        return $query->row();
    }

    

        
    public function pie_chart_malang() {
        $malang = 'malang';
        $this->db->select('id_odp, datel, sto, COUNT(odp) as total');
        $this->db->from('t_master_odp');
        $this->db->where('datel =', $malang);
        $this->db->group_by('sto'); 
        $this->db->order_by('total', 'asc'); 
        
   
        $hasil = $query->result();

        $data = [];
        
        foreach($hasil as $row) {
              $data['label'][] = $row->sto;
              $data['data'][] = (int) $row->total;
        }

        return $hasil;
    }

    // check if master odp already existed
    public function checkMasterOdp($odp) {
        $this->db->select('id_odp');
        $this->db->from('t_master_odp');
        $this->db->where('odp', $odp);
        $query = $this->db->get();
        
        if ($query->num_rows() > 0) {
            return $query->row()->id_odp;
        } else {
            return false;
        }
    }

    // import insert master odp
    public function insertMasterOdp($data){
        $this->db->insert('t_master_odp', $data);

        $insert_id = $this->db->insert_id();

        return  $insert_id;
    }
    
    //import odp port
    public function updateMasterOdp($id, $data){
        $this->db->where('id_odp',$id);
        $this->db->update('t_master_odp',$data);
    }
}
