<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Ftm extends Modular {

	public function __construct(){
        authenticated();
        sessionAsRivara();
		parent::__construct();
		$this->load->library('form_validation'); 
		$this->load->model('Conectivity_models');
	}
	  
	public function index()
	{	
        $this->load->helper('url');
		$data["ftm"] = $this->Conectivity_models->get_ftm();
		$data['bundlecore']=$this->Conectivity_models->get_bundlecore();
		$this->load->view('views_ftm', $data);
	}

	//Update
	function update($id = null){
		$Conectivity_models = $this->Conectivity_models;
		if (isset($_POST['submit'])){
			$this->Conectivity_models->UpdateFTMById($id);
			redirect('conectivity/ftm');
		}
	 	else {
		if (!isset($id)) redirect('conectivity/ftm');
	
		$data['ftm']=$this->Conectivity_models->getFTMbyId($id);
		$data['bundlecore']=$this->Conectivity_models->get_bundlecore();
		// print_r($data);exit();
		$this->load->view('views_ftm_update', $data);
	 }
	}

	//Lihat
	function lihat($id = null){
		if (isset($_POST['submit'])){
				// print_r($this->input->post('nama_odp'));exit();
				$Conectivity_models = $this->Conectivity_models;
				
				$data["ftm"]=$Conectivity_models->getFTMbyId($id);
				// $data["port_odp"]=$Conectivity_models->getPortById($id);
				redirect('ftm/index');
			}
        else {
			if (!isset($id)) redirect('ftm/index');
			$Conectivity_models = $this->Conectivity_models;

			$data["ftm"]=$Conectivity_models->getFTMbyId($id);
			
				
			$this->load->view("views_ftm_lihat", $data);
        }
	
	}

	//Hapus FTM
	public function delete($id = null)
	{
		if (!isset($id)) show_404();

	    if ($this->Conectivity_models->hapusFTM($id)) {
		    redirect(site_url('conectivity/ftm/index'));
	    }
	}

	//Create
	// function create(){
    //     if (isset($_POST['submit'])) {
	// 		$data["ftm"] = $this->Conectivity_models->create_ftm();
	// 		redirect('conectivity/ftm/index');
	// 	} else {
	// 		$this->load->view("views_ftm");
	// 	}
	// }

	function create(){
		$Conectivity_models = $this->Conectivity_models;
		if (isset($_POST['submit'])){
			$this->Conectivity_models->create_ftm();
			redirect('conectivity/ftm');
		}
	 	else {
		if (!isset($id)) redirect('conectivity/ftm');	
		// $data['ftm']=$this->Conectivity_models->getFTMbyId($id);
		redirect('conectivity/ftm');
		// $this->load->view('ftm_update', $data);
	 }
	}

	function create2(){

		$this->form_validation->set_rules('nama_ftm','Nama FTM','required');
		$this->form_validation->set_rules('nama_ruangan','Nama Ruangan','required');
		$this->form_validation->set_rules('bundlecore','Bundle Core','required');
		$this->form_validation->set_rules('nama_location_odf_ea','Lokasi ODF EA','required');
		$this->form_validation->set_rules('otb_port_core_ea','OTB Port Core EA','required');
		$this->form_validation->set_rules('kap_kabel_ea','Kap Kabel EA','required');
		$this->form_validation->set_rules('nama_location_odf_OA','Lokasi ODF OA','required');
		$this->form_validation->set_rules('otb_port_core_OA','OTB Port Core OA','required');
		$this->form_validation->set_rules('kap_kabel_OA','Kap Kabel OA','required');
		$this->form_validation->set_rules('kordinat_sto_lat','Latitude STO','required');
		$this->form_validation->set_rules('kordinat_sto_long','Longitude STO','required');

        if (isset($_POST['submit'])) {
			if ($this->form_validation->run() != false){
				$this->Conectivity_models->create_ftm();
				redirect('conectivity/ftm');
			}			
			else{
				redirect('conectivity/ftm');
			}
		} 
		else {
			redirect('conectivity/ftm');
		}
	}

	function createS(){

		$this->form_validation->set_rules(
			'odp_name', 'ODP',
			'required|is_unique[t_odp.odp_name]',
			array(
					'required'      => 'The %s field is required.',
					'is_unique'     => 'This %s already exists.'
			)
		);

		$this->form_validation->set_rules('date','Tanggal','required');
		// $this->form_validation->set_rules('odp_name','ODP','required|is_unique[t_odp.odp_name]');
		$this->form_validation->set_rules('ip_address','IP Address','required');
		$this->form_validation->set_rules('slot_port','Slot Port','required');

        if (isset($_POST['submit'])) {
			if ($this->form_validation->run() != false){
				$data["odp"] = $this->Daman_models->create_odp();
				redirect('daman/table/tambah_port/'.$data['odp']->id_odp);
			}			
			else{
				$data = array(
					'date' => $this->input->post('date'),//check this
					'odp_name' => $this->input->post('odp_name'),//check this
					'ip_address' => $this->input->post('ip_address'),//check this
					'slot_port' => $this->input->post('slot_port')//check this
				);	
				// print_r($data);exit();
				
				$this->load->view("tables_create", $data);
			}
		} 
		else {
			$this->load->view("tables_create");
		}
	}

	//Tambah Port
	function tambah_port($id = null){
		// print_r($this->input->post('id_odp'));exit();
		// print_r($id);exit();
		if (!isset($id)) redirect('table/index');
		$Daman_models = $this->Daman_models;
		$data["odp"] = $Daman_models->getByODP($id);
		$data["port"] = $Daman_models->get_port($id);
		// print_r($data);exit();
		// $this->load->view('tables_create_port', $data);
		
		if (isset($_POST['submit'])) {
			// print_r($this->input->post('id_odp'));exit();
			$this->Daman_models->create_port();
			redirect('daman/table/lihat/'.$data['odp']->id_revitalisasi_odp);
		} else {
			// $this->load->view("tables_create");
		$this->load->view('tables_create_port', $data);

		}
        // if (isset($_POST['submit'])) {
		// 	$this->Daman_models->create_port();
		// 	redirect('table/tambah_port');
		// } else {
		// 	$this->load->view("tables_create_port", $data);
		// }
	}

	//Update
	function update_table($id){
		if (isset($_POST['submit'])){
				// print_r($this->input->post('nama_odp'));exit();
				// print_r($this->input->post('status'));exit();
				// print_r($this->input->post('id_port'));exit();
				$Conectivity_models = $this->Conectivity_models;

                $this->Conectivity_models->UpdateFTMById($id);
				redirect('ftm/update/'.$id);

			}
         else {
			if (!isset($id)) redirect('ftm/update/'.$id);

        }
	
	}

	// function lihat($id = null){
	// 	if (isset($_POST['submit'])){
	// 			// print_r($this->input->post('nama_odp'));exit();
	// 	$Daman_models = $this->Daman_models;
				
	// 			$data["odp"]=$Daman_models->getById($id);
	// 			$data["port_odp"]=$Daman_models->getPortById($id);
	// 			redirect('daman/table/index');
	// 		}
    //     else {
	// 		if (!isset($id)) redirect('daman/table/index');
	// 		$Daman_models = $this->Daman_models;

	// 		$data["odp"]=$Daman_models->getById($id);
	// 		$data["port_odp"]=$Daman_models->getPortById($id);
	// 		$data["Daman_models"]=$Daman_models->getById($id);
				
	// 		$this->load->view("tables_lihat", $data);
    //     }
	
	// }

	// function delete($id=null)
	// {
	//     if (!isset($id)) show_404();

	//     if ($this->Daman_models->hapus($id)) {
	// 	    redirect(site_url('daman/table/index'));
	//     }
	// }


	public function view(){
		$search = $_POST['search']['value']; // Ambil data yang di ketik user pada textbox pencarian
		$limit = $_POST['length']; // Ambil data limit per page
		$start = $_POST['start']; // Ambil data start
		$order_index = $_POST['order'][0]['column']; // Untuk mengambil index yg menjadi acuan untuk sorting
		$order_field = $_POST['columns'][$order_index]['data']; // Untuk mengambil nama field yg menjadi acuan untuk sorting
		$order_ascdesc = $_POST['order'][0]['dir']; // Untuk menentukan order by "ASC" atau "DESC"
		$sql_total = $this->Daman_models->count_all(); // Panggil fungsi count_all pada SiswaModel
		$sql_data = $this->Daman_models->filter($search, $limit, $start, $order_field, $order_ascdesc); // Panggil fungsi filter pada SiswaModel
		$sql_filter = $this->Daman_models->count_filter($search); // Panggil fungsi count_filter pada SiswaModel
		$callback = array(
			'draw'=>$_POST['draw'], // Ini dari datatablenya
			'recordsTotal'=>$sql_total,
			'recordsFiltered'=>$sql_filter,
			'data'=>$sql_data
		);
		header('Content-Type: application/json');
		echo json_encode($callback); // Convert array $callback ke json
	}

	public function import() {
        $data = array();
		$this->form_validation->set_rules('fileURL', 'Upload File', 'callback_checkFileValidation');
		
        // If file uploaded
		if(!empty($_FILES['fileURL']['name'])) { 
			// get file extension
			$extension = pathinfo($_FILES['fileURL']['name'], PATHINFO_EXTENSION);
			
			if($extension == 'csv'){
				$reader = new \PhpOffice\PhpSpreadsheet\Reader\Csv();
			} elseif($extension == 'xlsx') {
				$reader = new \PhpOffice\PhpSpreadsheet\Reader\Xlsx();
			} else {
				$reader = new \PhpOffice\PhpSpreadsheet\Reader\Xls();
			}
			// file path
			$spreadsheet = $reader->load($_FILES['fileURL']['tmp_name']);
			$allDataInSheet = $spreadsheet->getActiveSheet()->toArray(null, true, true, true);
		
			// array Count
			$arrayCount = count($allDataInSheet);
			
			$flag = 0;
			$createArray = array(
							'Tanggal', 'NAMA_ODP', 'IP', 'SLOT_PORT', 'PORT_ODP', 
							'STATUS', 'ownership', 'NO_INET', 'QR_CODE_DROPCORE', 'Keterangan', 
							'Update_UIM', 'Input_DAVA', 'Port_UIM', 'DC_Dismantled', 'Trouble'
			);
			$makeArray = array(
				'Tanggal'     	=> 'Tanggal',
				'NAMA_ODP'  => 'NAMA_ODP',
				'IP'  => 'IP',
				'SLOT_PORT' => 'SLOT_PORT',
				'PORT_ODP'    => 'PORT_ODP',
				'STATUS'      => 'STATUS',
				'ownership'		=> 'ownership',
				'NO_INET'  => 'NO_INET',
				'QR_CODE_DROPCORE'=> 'QR_CODE_DROPCORE',
				'Keterangan'     => 'Keterangan',
				'Update_UIM' => 'Update_UIM',
				'Input_DAVA'     => 'Input_DAVA',
				'Port_UIM'     => 'Port_UIM',
				'DC_Dismantled' => 'DC_Dismantled',
				'Trouble' => 'Trouble'             
				);
			
			$SheetDataKey = array();
			foreach ($allDataInSheet as $dataInSheet) {
				foreach ($dataInSheet as $key => $value) {
					if (in_array(trim($value), $createArray)) {
						$value = preg_replace('/\s+/', '', $value); 
						$SheetDataKey[trim($value)] = $key;
					} 
				}
				
			}
			
			$dataDiff = array_diff_key($makeArray, $SheetDataKey);
			
			if (empty($dataDiff)) {
				$flag = 1;
			}

			$date = str_replace('/', '-', $SheetDataKey['Tanggal']);
			
			// match excel sheet column
			if ($flag == 1) {
				for ($i = 2; $i <= $arrayCount; $i++) {
					$date = $SheetDataKey['Tanggal'];
					$odp_name = $SheetDataKey['NAMA_ODP'];
					$ip_address = $SheetDataKey['IP'];
					$slot_port = $SheetDataKey['SLOT_PORT'];
					$no_port = $SheetDataKey['PORT_ODP'];
					$status = $SheetDataKey['STATUS'];
					$ownership = $SheetDataKey['ownership'];
					$inet_voice_customer = $SheetDataKey['NO_INET'];
					$qr_code = $SheetDataKey['QR_CODE_DROPCORE'];
					$description = $SheetDataKey['Keterangan'];
					$update_uim = $SheetDataKey['Update_UIM'];
					$input_dava = $SheetDataKey['Input_DAVA'];
					$port_uim = $SheetDataKey['Port_UIM'];
					$dc_dismantled = $SheetDataKey['DC_Dismantled'];
					$trouble = $SheetDataKey['Trouble'];
					
					$date = filter_var(trim($allDataInSheet[$i][$date]), FILTER_SANITIZE_STRING);
					$odp_name = filter_var(trim($allDataInSheet[$i][$odp_name]), FILTER_SANITIZE_STRING);
					$ip_address = filter_var(trim($allDataInSheet[$i][$ip_address]), FILTER_SANITIZE_STRING);
					$slot_port = filter_var(trim($allDataInSheet[$i][$slot_port]), FILTER_SANITIZE_STRING);
					$no_port = filter_var(trim($allDataInSheet[$i][$no_port]), FILTER_SANITIZE_NUMBER_INT);
					$status = filter_var(trim($allDataInSheet[$i][$status]), FILTER_SANITIZE_STRING);
					$ownership = filter_var(trim($allDataInSheet[$i][$ownership]), FILTER_SANITIZE_STRING);
					$inet_voice_customer = filter_var(trim($allDataInSheet[$i][$inet_voice_customer]), FILTER_SANITIZE_STRING);
					$qr_code = filter_var(trim($allDataInSheet[$i][$qr_code]), FILTER_SANITIZE_STRING);
					$description = filter_var(trim($allDataInSheet[$i][$description]), FILTER_SANITIZE_STRING);
					$update_uim = filter_var(trim($allDataInSheet[$i][$update_uim]), FILTER_SANITIZE_STRING);
					$input_dava = filter_var(trim($allDataInSheet[$i][$input_dava]), FILTER_SANITIZE_STRING);
					$port_uim = filter_var(trim($allDataInSheet[$i][$port_uim]), FILTER_SANITIZE_STRING);
					$dc_dismantled = filter_var(trim($allDataInSheet[$i][$dc_dismantled]), FILTER_SANITIZE_STRING);
					$trouble = filter_var(trim($allDataInSheet[$i][$trouble]), FILTER_SANITIZE_STRING);
					
					$new_date = (new DateTime($date))->format('Y-m-d');

					$fetchDataODP = array(
						'date' => $new_date,
						'odp_name' => $odp_name,
						'ip_address' => $ip_address,
						'slot_port' => $slot_port
					);
					
					$checkOdp = $this->Daman_models->checkOdp($odp_name);

					$fetchDataPort = array(
						'fk_odp' => $checkOdp,
						'no_port' => $no_port,
						'status' => $status,
						'ownership' => $ownership,
						'inet_voice_customer' => $inet_voice_customer,
						'qr_code' => $qr_code,
						'description' => $description,
						'update_uim' => $update_uim,
						'input_dava' => $input_dava,
						'port_uim' => $port_uim,
						'dc_dismantled' => $dc_dismantled,
						'trouble' => $trouble
					);
					
					if($checkOdp){							
						$this->Daman_models->insertPort($fetchDataPort);
					}else{
						$id_odp = $this->Daman_models->insertOdp($fetchDataODP);

						$fetchDataPort['fk_odp'] = $id_odp;
						$this->Daman_models->insertPort($fetchDataPort);
					}
				}
				echo "Berhasil";
			} else {
				echo "Please import correct file, did not match excel sheet column";
			}
		}
    }
 
    // checkFileValidation
    public function checkFileValidation($string) {
      $file_mimes = array('text/x-comma-separated-values', 
        'text/comma-separated-values', 
        'application/octet-stream', 
        'application/vnd.ms-excel', 
        'application/x-csv', 
        'text/x-csv', 
        'text/csv', 
        'application/csv', 
        'application/excel', 
        'application/vnd.msexcel', 
        'text/plain', 
        'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet'
      );
      if(isset($_FILES['fileURL']['name'])) {
            $arr_file = explode('.', $_FILES['fileURL']['name']);
            $extension = end($arr_file);
            if(($extension == 'xlsx' || $extension == 'xls' || $extension == 'csv') && in_array($_FILES['fileURL']['type'], $file_mimes)){
                return true;
            }else{
                $this->form_validation->set_message('checkFileValidation', 'Please choose correct file.');
                return false;
            }
        }else{
            $this->form_validation->set_message('checkFileValidation', 'Please choose a file.');
            return false;
        }
    }
}