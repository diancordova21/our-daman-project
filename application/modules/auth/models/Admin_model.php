<?php

class Admin_model extends CI_model 
{
    protected $table = 't_admin';
    protected $primaryKey = 'id_admin';

    public function register(){
        // print_r($this->input->post('fullname'));exit();
        $hash = $this->bcrypt->hash_password($this->input->post('password'));
        $data = array(
            'username' => $this->input->post('username'),
            'fullname' => $this->input->post('username'),
            'password' => $hash,
            'create_at' =>date("Y-m-d"),
            'update_at' =>date("Y-m-d"),

        );
        $this->db->insert($this->table, $data);
    }
    private function hash_password($pass_user) {
        return password_hash($pass_user, PASSWORD_BCRYPT);
       }

    public function loginByUsername($username, $password){
        $this->db->where('username', $username);
        $this->getUsers($password); 
        $this->db->get($this->table);
    }

    public function getUsers($password){
        $q = $this->db->get($this->table);
        if ($q->num_rows() > 0) {
            $result = $q->row_array();
            // True 🌛
            if ($this->bcrypt->check_password($password, $result['password'])) {
                //var_dump($result);
                if ($result) {
                    // Save data array to session
                    $tmp = array(
                        'id_admin' => $result['id_admin'],
                        'username' => $result['username'],
                        'fullname' => $result['fullname'],
                        'role' => $result['role'],
                    );
                    $this->session->set_userdata('logged_in', $tmp);
                    if($result['role'] == 'daman'){
                        redirect('daman/dashboard');
                    }else if($result['role'] == 'aom') {
                        redirect('aom/dashboard');
                    }else if($result['role'] == 'rivara') {
                            redirect('conectivity/dashboard');
                }
            } else {
                // echo 'Username / Password wrong 🌚
                echo "<script>alert('Username atau sandi salah')</script>";
                redirect('auth/admin');
            }
        } else {
            echo "<script>alert('Sandi salah')</script>";
            return array();
        }
    }
    }

    public function getUser(){
        $query = $this->db->get($this->table);
        return $query->result();
    }
    
}
