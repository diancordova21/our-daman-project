<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class batu extends CI_Controller {

	public function __construct(){
		parent::__construct();
		$this->load->model('masterodp');
	}
	
	public function index()
	{	
		$this->load->helper('url');
		$masterodp = $this->masterodp;
		$data["total_karangploso"] = $masterodp->jumlah_karangploso();
		$data["total_batu"] = $masterodp->jumlah_batu ();
		$data["total_ngantang"] = $masterodp->jumlah_ngantang();
		
		$this->load->view('datel_batu', $data);
	}

	function sto($sto = null){

		$data["odp"] = $this->masterodp->getBySTO($sto);
		$data["sto"] = $this->masterodp->ambil_sto($sto);		
		// print_r($data);exit();

		$this->load->view('sto_lihat', $data);
	}
}