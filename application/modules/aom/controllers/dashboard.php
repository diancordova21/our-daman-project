<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Dashboard extends Modular {

	public function __construct(){
		authenticated();
        sessionAsAOM();
		parent::__construct();
		$this->load->model('Master_odp');
	}

	 function index()
	{
		// $data['pageName']= 'main-dashboard';
		$Master_odp = $this->Master_odp;
		$data["jumlah_odp"] = $Master_odp->jumlah_odp_persto();
		$data["total_odp"] = $Master_odp->jumlah_odp();
		$data["total_datel"] = $Master_odp->jumlah_datel();
		$data["total_sto"] = $Master_odp->jumlah_sto();
		$data["total_golive"] = $Master_odp->jumlah_golive();
 		$this->load->view('index', $data);
	}

	

}
