<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Golive_Malang extends Modular {

	public function __construct(){
		authenticated();
        sessionAsAOM();
		parent::__construct();
		$this->load->model('Master_odp');
	}
	
	public function index()
	{	
		$this->load->helper('url');
		$Master_odp = $this->Master_odp;
		$data["jumlah_lawang"] = $Master_odp->jumlah_lawang();
		$data["jumlah_singosari"] = $Master_odp->jumlah_singosari();
		$data["jumlah_pakis"] = $Master_odp->jumlah_pakis();
		$data["jumlah_sawojajar"] = $Master_odp->jumlah_sawojajar();
		$data["jumlah_tumpang"] = $Master_odp->jumlah_tumpang();
		$data["jumlah_blimbing"] = $Master_odp->jumlah_blimbing();
		$data["jumlah_klojen"] = $Master_odp->jumlah_klojen();
		$data["jumlah_malangkota"] = $Master_odp->jumlah_malangkota();
		$data["jumlah_gadang"] = $Master_odp->jumlah_gadang();
		$data["jumlah_buring"] = $Master_odp->jumlah_buring();
		$data["jumlah_dampit"] = $Master_odp->jumlah_dampit();
		$data["jumlah_kepanjen"] = $Master_odp->jumlah_kepanjen();
		$data["jumlah_turen"] = $Master_odp->jumlah_turen();	
		
		$this->load->view('golive_malang', $data);
	}

	public function odp_page()
     {

          // Datatables Variables
          $draw = intval($this->input->get("draw"));
          $start = intval($this->input->get("start"));
          $length = intval($this->input->get("length")); 


          $odp = $this->Master_odp->getBySTO($sto);

          $data = array();

          foreach($odp->result() as $r) {

				$row = array();
				$row[] = $r->datel;
				$row[] = $r->sto;
				$row[] = $r->odp;
				$row[] = $r->latitude;
				$row[] = $r->longitude;
				$row[] = $r->alamat;
				$row[] = $r->avai;
				$row[] = $r->reserved;
				$row[] = $r->in_service;
				$row[] = $r->total;
				$row[] = $r->tanggal_golive;
				$row[] = $r->bulan;
				$row[] = $r->tahun;
				$row[] = $r->kelurahan;
				$row[] = $r->kecamatan;
				$row[] = $r->kota_kab;
				$row[] = $r->qrcode_odp;
				$row[] = $r->qrcode_port;
				$row[] = $r->project;
				$row[] = $r->vendor;
				$row[] = $r->merk;
				$row[] = $r->ip;

				//add html for action
				$row[] = '<a class="btn btn-sm btn-success" href="lihat/'.$r->id_odp.'" title="Lihat"><i class="glyphicon glyphicon-folder"></i> Lihat</a>
				<a class="btn btn-sm btn-warning" href="update/'.$r->id_odp.'" title="Lihat"><i class="glyphicon glyphicon-folder"></i> Update</a>
				<a href="delete/'.$r->id_odp.'" class="btn btn-sm btn-danger" onclick="return confirm(\'Anda yakin untuk menghapus data ini?\');"><i class="fa fa-trash"></i>Hapus</a>
				';

				$data[] = $row;
          }

          $output = array(
               "draw" => $draw,
                 "recordsTotal" => $odp->num_rows(),
                 "recordsFiltered" => $odp->num_rows(),
                 "data" => $data
            );
          echo json_encode($output);
          exit();
    }

	function sto($sto = null){

		$data["odp"] = $this->Master_odp->getBySTO($sto);
		$data["sto"] = $this->Master_odp->ambil_sto($sto);	
		// print_r($data["sto"]);exit();	

		$this->load->view('sto_lihat', $data);
		// print_r($data); exit();



		// // Datatables Variables
		// $draw = intval($this->input->get("draw"));
		// $start = intval($this->input->get("start"));
		// $length = intval($this->input->get("length")); 

		// $odp = $this->Master_odp->getBySTO($sto);
		// // print_r($odp);exit();

        //   $data = array();

        //   foreach($odp->result() as $r) {

		// 		$row = array();
		// 		$row[] = $r->datel;
		// 		$row[] = $r->sto;
		// 		$row[] = $r->odp;
		// 		$row[] = $r->latitude;
		// 		$row[] = $r->longitude;
		// 		$row[] = $r->alamat;
		// 		$row[] = $r->avai;
		// 		$row[] = $r->reserved;
		// 		$row[] = $r->in_service;
		// 		$row[] = $r->total;
		// 		$row[] = $r->tanggal_golive;
		// 		$row[] = $r->bulan;
		// 		$row[] = $r->tahun;
		// 		$row[] = $r->kelurahan;
		// 		$row[] = $r->kecamatan;
		// 		$row[] = $r->kota_kab;
		// 		$row[] = $r->qrcode_odp;
		// 		$row[] = $r->qrcode_port;
		// 		$row[] = $r->project;
		// 		$row[] = $r->vendor;
		// 		$row[] = $r->merk;
		// 		$row[] = $r->ip;

		// 		//add html for action
		// 		$row[] = '<a class="btn btn-sm btn-success" href="lihat/'.$r->id_odp.'" title="Lihat"><i class="glyphicon glyphicon-folder"></i> Lihat</a>
		// 		<a class="btn btn-sm btn-warning" href="update/'.$r->id_odp.'" title="Lihat"><i class="glyphicon glyphicon-folder"></i> Update</a>
		// 		<a href="delete/'.$r->id_odp.'" class="btn btn-sm btn-danger" onclick="return confirm(\'Anda yakin untuk menghapus data ini?\');"><i class="fa fa-trash"></i>Hapus</a>
		// 		';

		// 		$data[] = $row;
		//   }
		  

        //   $output = array(
        //        "draw" => $draw,
        //          "recordsTotal" => $odp->num_rows(),
        //          "recordsFiltered" => $odp->num_rows(),
        //          "data" => $data
        //     );
		//   echo json_encode($output);
		  
		  
	}
}