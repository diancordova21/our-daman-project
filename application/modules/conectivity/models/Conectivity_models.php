<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Conectivity_models extends CI_Model{

    private $t_olt = "t_olt";
    private $t_ftm = "t_ftm";
    private $t_port_odp = "t_port_odp";
    private $t_master_odp = "t_master_odp"; 
    private $t_bundlecore = "t_bundlecore";

    public function __construct()
	{
	    parent::__construct();
	    $this->load->database();
    }
    
    // Model Function OLT

    public function get_olt()
    {
        $hasil = $this->db->get('t_olt');
	    return $hasil->result();
    }

    function create_olt()
    {
        $data = array
            (
                'nama_olt' => $this->input->post('nama_olt'),
                'nama_ruangan' => $this->input->post('ruangan_olt'),
                'ip_olt' => $this->input->post('ip_olt'),
                'subrack_cart_port' => $this->input->post('subrack_cart_port')
            );
            // print_r($data);exit();
    
        $this->db->insert('t_olt',$data);  
	    

    }

    public function get_olt_detail($id)
    {
        return $this->db->get_where($this->t_olt, ["id_olt" => $id])->result();
    }

     function UpdateById($id)
    {
        $data = array
            (
                'nama_olt' => $this->input->post('nama_olt'),
                'nama_ruangan' => $this->input->post('nama_ruangan'),
                'ip_olt' => $this->input->post('ip_olt'),
                'subrack_cart_port' => $this->input->post('subrack_card_port'),
            );
            // print_r($data);exit();
                $this->db->where('id_olt',$id);
                $this->db->update('t_olt',$data);
            
    }

    function hapus($id)
	{
		$hasil = $this->db->delete($this->t_olt, array("id_olt" => $id));
        return $hasil;
    }

    // Model Function Bundlecore

    public function get_bundlecore_detail($id)
    {
        return $this->db->get_where($this->t_bundlecore, ["id_bundlecore" => $id])->result();
    }

    public function get_bundlecore()
    {
        $this->db->select('*');
        $this->db->join('t_olt',' t_olt.id_olt=t_bundlecore.fk_olt');
        $hasil = $this->db->get('t_bundlecore');
	    return $hasil->result();
    }
   
    function create_bundlecore()
    {
        $data = array
            (
                'nama_bundlecore' => $this->input->post('nama_bundlecore'),
                'fk_olt' => $this->input->post('fk_olt'),
            );
            // print_r($data);exit();
    
        $this->db->insert('t_bundlecore',$data);  
	    
    }

    function UpdateByIdBundlecore($id)
    {
        $data = array
            (
                'fk_olt' => $this->input->post('fk_olt'),
                'nama_bundlecore' => $this->input->post('nama_bundlecore'),
            );
            // print_r($data);exit();
                $this->db->where('id_bundlecore',$id);
                $this->db->update('t_bundlecore',$data);
            
    }
    public function hapus_bundlecore($id)
    {
        $hasil = $this->db->delete($this->t_bundlecore, array("id_bundlecore" => $id));
		return $hasil;
    }

    // Model Function FTM
    public function get_ftm()
    {   
        $this->db->select('*');
        $this->db->join('t_bundlecore',' t_bundlecore.id_bundlecore=t_ftm.fk_bundlecore');
        $hasil = $this->db->get('t_ftm');
        return $hasil->result();
    }    

    function getFTMbyId($id)
	{
        $this->db->select('*');
        $this->db->where('id_ftm =', $id);
        $hasil = $this->db->get('t_ftm');
	    return $hasil->row();
    }

    public function UpdateFTMById($id)
    {
        $data = array
            (
                'nama_ftm' => $this->input->post('nama_ftm'),
                'nama_ruangan' => $this->input->post('nama_ruangan'),
                'fk_bundlecore' => $this->input->post('fk_bundlecore'),
                'nama_location_odf_ea' => $this->input->post('nama_location_odf_ea'),
                'otb_port_core_ea' => $this->input->post('otb_port_core_ea'),
                'kap_kabel_ea' => $this->input->post('kap_kabel_ea'),
                'nama_location_odf_oa' => $this->input->post('nama_location_odf_oa'),
                'otb_port_core_oa' => $this->input->post('otb_port_core_oa'),
                'kap_kabel_oa' => $this->input->post('kap_kabel_oa'),
                'kordinat_sto_lat' => $this->input->post('kordinat_sto_lat'),
                'kordinat_sto_long' => $this->input->post('kordinat_sto_long'),
            );
            // print_r($data);exit();
            $this->db->where('id_ftm',$id);
            $this->db->update('t_ftm',$data); 

    } 

    function hapusFTM($id)
	{
		$hasil = $this->db->delete($this->t_ftm, array("id_ftm" => $id));
		return $hasil;
    }

    function create_ftm()
    {
        $data = array
            (
                'nama_ftm' => $this->input->post('nama_ftm'),
                'nama_ruangan' => $this->input->post('nama_ruangan'),
                'fk_bundlecore' => $this->input->post('fk_bundlecore'),
                'nama_location_odf_ea' => $this->input->post('nama_location_odf_ea'),
                'otb_port_core_ea' => $this->input->post('otb_port_core_ea'),
                'kap_kabel_ea' => $this->input->post('kap_kabel_ea'),
                'nama_location_odf_oa' => $this->input->post('nama_location_odf_oa'),
                'otb_port_core_oa' => $this->input->post('otb_port_core_oa'),
                'kap_kabel_oa' => $this->input->post('kap_kabel_oa'),
                'kordinat_sto_lat' => $this->input->post('kordinat_sto_lat'),
                'kordinat_sto_long' => $this->input->post('kordinat_sto_long')
            );
            // return print_r($data);exit();
                
        $this->db->insert('t_ftm',$data);  	    

    }

    // // Model Function Distribusi
    // public function get_odc()
    // {
    //     $hasil = $this->db->get('t_odc');
	//     return $hasil->result();
    // }

    // Model Function Distribusi

    public function get_all_distribusi()
    {
        $this->db->select('*');       
        $hasil = $this->db->get('t_distribusi');
	    return $hasil->result();
    }

 
    public function get_distribusi()
    {
        $this->db->select('*');
        $this->db->join('t_odc',' t_odc.id_odc=t_distribusi.fk_odc');
        $hasil = $this->db->get('t_distribusi');
	    return $hasil->result();
    }

    function getDISTbyId($id)
	{
        $this->db->select('*');
        $this->db->where('id_distribusi =', $id);
        $hasil = $this->db->get('t_distribusi');
	    return $hasil->row();
    }

    // Model Function ODP
    public function get_data_odp()
    {
        $data = array
            (
                'nama_kabel_distribusi' => $this->input->post('nama_kabel_distribusi'),
                'kap_core' => $this->input->post('kap_core'),
                'fk_odc' => $this->input->post('fk_odc')
            );
            // print_r($data);exit();
            $this->db->where('id_distribusi',$id);
            $this->db->update('t_distribusi',$data); 

    } 

    function hapusDIST($id)
	{
		$hasil = $this->db->delete($this->t_distribusi, array("id_distribusi" => $id));
		return $hasil;
    }

    function create_distribusi()
    {
        $data = array
            (
                'nama_kabel_distribusi' => $this->input->post('nama_kabel_distribusi'),
                'kap_core' => $this->input->post('kap_core'),
                'fk_odc' => $this->input->post('fk_odc')
            );
            // return print_r($data);exit();
                
        $this->db->insert('t_distribusi',$data);  	    

    }

    // Model Function ODP
    public function get_odp()
    {   
        $this->db->select('*');
        $this->db->join('t_distribusi',' t_distribusi.id_distribusi=t_odp.fk_distribusi');
        $hasil = $this->db->get('t_odp');
        return $hasil->result();
    }    

    

    function getODPbyId($id)
	{
        $this->db->select('*');
        $this->db->where('id_odp =', $id);
        $hasil = $this->db->get('t_odp');
	    return $hasil->row();
    }

    public function UpdateODPById($id)
    {
        $data = array
            (
                'nama_odp' => $this->input->post('nama_odp'),
                'fk_distribusi' => $this->input->post('fk_distribusi'),
                'nama_frame' => $this->input->post('nama_frame'),
                'splitter_no_nama_kap' => $this->input->post('splitter_no_nama_kap'),
                'odp_qr' => $this->input->post('odp_qr'),
                'odp_qr_laminating' => $this->input->post('odp_qr_laminating'),
                'kelurahan' => $this->input->post('kelurahan'),
                'kecamatan' => $this->input->post('kecamatan'),
                'alamat' => $this->input->post('alamat')
            );
            // print_r($data);exit();
            $this->db->where('id_odp',$id);
            $this->db->update('t_odp',$data); 

    } 

    function hapusODP($id)
	{
		$hasil = $this->db->delete($this->t_odp, array("id_odp" => $id));
		return $hasil;
    }

    function create_odp()
    {
        $data = array
            (
                'nama_odp' => $this->input->post('nama_odp'),
                'fk_distribusi' => $this->input->post('fk_distribusi'),
                'nama_frame' => $this->input->post('nama_frame'),
                'splitter_no_nama_kap' => $this->input->post('splitter_no_nama_kap'),
                'odp_qr' => $this->input->post('odp_qr'),
                'odp_qr_laminating' => $this->input->post('odp_qr_laminating'),
                'kelurahan' => $this->input->post('kelurahan'),
                'kecamatan' => $this->input->post('kecamatan'),
                'alamat' => $this->input->post('alamat')
            );
            // return print_r($data);exit();
                
        $this->db->insert('t_odp',$data);  	    

    }

    //

    public function get_data()
    {
        $this->db->select('t_revitalisasi_odp.id_revitalisasi_odp,t_revitalisasi_odp.odp_name,t_master_odp.datel,t_master_odp.sto,SUM(if(t_port_odp.status = "ON", 1, 0)) as total_on,SUM(if(t_port_odp.status="OFF",1,0)) as total_off,COUNT(t_port_odp.id_port) as total');
        $this->db->join('t_master_odp',' t_revitalisasi_odp.odp_name=t_master_odp.odp');
        $this->db->join('t_port_odp',' t_revitalisasi_odp.id_revitalisasi_odp=t_port_odp.fk_odp');
        $this->db->group_by('t_revitalisasi_odp.id_revitalisasi_odp');

        $hasil = $this->db->get('t_revitalisasi_odp');
	    return $hasil->result();

    }

    function getByODP($id)
	{
	    return $this->db->get_where($this->t_revitalisasi_odp, ["id_revitalisasi_odp" => $id])->row();
    }

    function get_port($id)
	{
	    return $this->db->get_where($this->t_port_odp, ["fk_odp" => $id])->result();
    }

    function getById($id)
	{
        $this->db->select('*');
        $this->db->where('id_revitalisasi_odp =', $id);
        $hasil = $this->db->get('t_revitalisasi_odp');
        
	    return $hasil->result();
    }

    

    function getPortById($id)
	{
        $this->db->select('*');
        $this->db->where('fk_odp =', $id);
        $hasil = $this->db->get('t_port_odp');
	    return $hasil->result();
    }

    // function create_odp()
    // {
    //     $data = array
    //         (
    //             'date' => $this->input->post('date'),
    //             'odp_name' => $this->input->post('odp_name'),
    //             'ip_address' => $this->input->post('ip_address'),
    //             'slot_port' => $this->input->post('slot_port')
    //         );
    //         // print_r($data);exit();
    //         $this->db->insert('t_revitalisasi_odp',$data);  
	//     return $this->db->get_where($this->t_revitalisasi_odp, ["odp_name" => $this->input->post('odp_name')])->row();


    // }

    function create_port()
    {
        $data = array
            (
                'fk_odp' => $this->input->post('id_odp'),
                'no_port' => $this->input->post('no_port'),
                'status' => $this->input->post('status'),
                'ownership' => $this->input->post('ownership'),
                'inet_voice_customer' => $this->input->post('inet_voice_customer'),
                'qr_code' => $this->input->post('qr_code'),
                'description' => $this->input->post('description'),
                'update_uim' => $this->input->post('update_uim'),
                'input_dava' => $this->input->post('input_dava'),
                'port_uim' => $this->input->post('port_uim'),
                'dc_dismantled' => $this->input->post('dc_dismantled'),
                'trouble' => $this->input->post('trouble')
            );
            // print_r($data);exit();
            $this->db->insert('t_port_odp',$data);            
    }

    
    public function UpdateTableById($fk)
    {
        for ($n=0; $n< COUNT($this->input->post('status')); $n++) {
            
            $data = array
                (
                    'status' => $this->input->post('status')[$n],
                    'inet_voice_customer' => $this->input->post('inet_voice_customer')[$n],
                    'update_uim' => $this->input->post('update_uim')[$n],
                    'ownership' => $this->input->post('ownership')[$n],
                    'input_dava' => $this->input->post('input_dava')[$n],
                    'port_uim' => $this->input->post('port_uim')[$n],
                    'dc_dismantled' => $this->input->post('dc_dismantled')[$n],
                    'trouble' => $this->input->post('trouble')[$n],
                    'description' => $this->input->post('description')[$n],
                    'qr_code' => $this->input->post('qr_code')[$n],
                );
                    $this->db->where('id_port',$this->input->post('id_port')[$n]);
                    $this->db->update('t_port_odp',$data);
                    // print_r($data);exit(); 
                    // $data = array();
               }
            //    return 0;    

    } 

    

    // function UpdateById($id)
    // {
    //     $data = array
    //         (
    //             'date' => $this->input->post('date'),
    //             'odp_name' => $this->input->post('odp_name'),
    //             'ip_address' => $this->input->post('ip_address'),
    //             'slot_port' => $this->input->post('slot_port'),
    //             'port_revitalisasi_odp' => $this->input->post('port_revitalisasi_odp'),
    //             'status' => $this->input->post('status'),
    //             'inet_voice_customer' => $this->input->post('inet_voice_customer'),
    //             'qr_code' => $this->input->post('qr_code'),
    //             'description' => $this->input->post('description'),
    //             'update_uim' => $this->input->post('update_uim'),
    //             'input_dava' => $this->input->post('input_dava'),
    //             'port_uim' => $this->input->post('port_uim'),
    //             'dc_dismantled' => $this->input->post('dc_dismantled'),
    //             'trouble' => $this->input->post('trouble')
    //         );
    //         // print_r($data);exit();
    //             $this->db->where('id_revitalisasi',$id);
    //             $this->db->update('t_revitalisasi',$data);
            
    // }

    
    
    function getBySTO($sto)
	{
       $this->db->select('t_revitalisasi_odp.id_revitalisasi_odp,t_revitalisasi_odp.odp_name,t_master_odp.qrcode_odp,t_master_odp.total,t_revitalisasi_odp.ip_address');
       $this->db->join('t_master_odp',' t_revitalisasi_odp.odp_name=t_master_odp.odp');
       $this->db->where('sto =', $sto);
       $this->db->group_by('odp');
       $hasil = $this->db->get('t_revitalisasi_odp');
	    return $hasil->result();
    }

    function ambil_sto($sto)
	{
        $this->db->where('sto', $sto);
        $hasil = $this->db->get('t_master_odp');
        return $hasil->row();        
    }

    function ambil_trouble()
	{
        // $this->db->where('trouble');
        $hasil = $this->db->get('t_port_odp');
        return $hasil->row();      
        // print_r($hasil->row());exit();

    }

    function ambil_disconnect()
	{
        $trouble = 'Disconnect';
        $this->db->join('t_port_odp','t_port_odp.fk_odp = t_revitalisasi_odp.id_revitalisasi_odp');
        $this->db->where('trouble', $trouble);
        $hasil = $this->db->get('t_revitalisasi_odp');
        // print_r($hasil->result());exit();
        // print_r($hasil);exit();

        return $hasil->result();      
        // print_r($hasil->result());exit();

    }

    function ambil_redti()
	{
        $trouble = 'Redaman Tinggi';
        $this->db->join('t_port_odp','t_port_odp.fk_odp = t_revitalisasi_odp.id_revitalisasi_odp');
        $this->db->where('trouble', $trouble);
        $hasil = $this->db->get('t_revitalisasi_odp');

        return $hasil->result();      
        // print_r($hasil->result());exit();

    }

    function ambil_stuckbi()
	{
        $trouble = 'Service Stuck BI';
        $this->db->join('t_port_odp','t_port_odp.fk_odp = t_revitalisasi_odp.id_revitalisasi_odp');
        $this->db->where('trouble', $trouble);
        $hasil = $this->db->get('t_revitalisasi_odp');

        return $hasil->result();      
        // print_r($hasil->result());exit();

    }

    function ambil_notdetek()
	{
        $trouble = 'Tidak Detek';
        $this->db->join('t_port_odp','t_port_odp.fk_odp = t_revitalisasi_odp.id_revitalisasi_odp');
        $this->db->where('trouble', $trouble);
        $hasil = $this->db->get('t_revitalisasi_odp');

        return $hasil->result();      
        // print_r($hasil->result());exit();

    }

    function ambil_splitter()
	{
        $trouble = 'Splitter';
        $this->db->join('t_port_odp','t_port_odp.fk_odp = t_revitalisasi_odp.id_revitalisasi_odp');
        $this->db->where('trouble', $trouble);
        $hasil = $this->db->get('t_revitalisasi_odp');

        return $hasil->result();      
        // print_r($hasil->result());exit();

    }

    function ambil_cantcreate()
	{
        $trouble = 'Tidak Bisa Create New Version';
        $this->db->join('t_port_odp','t_port_odp.fk_odp = t_revitalisasi_odp.id_revitalisasi_odp');
        $this->db->where('trouble', $trouble);
        $hasil = $this->db->get('t_revitalisasi_odp');

        return $hasil->result();      
        // print_r($hasil->result());exit();

    }

    function ambil_suspend()
	{
        $trouble = 'Service Suspend';
        $this->db->join('t_port_odp','t_port_odp.fk_odp = t_revitalisasi_odp.id_revitalisasi_odp');
        $this->db->where('trouble', $trouble);
        $hasil = $this->db->get('t_revitalisasi_odp');

        return $hasil->result();      
        // print_r($hasil->result());exit();

    }

    function ambil_canceled()
	{
        $trouble = 'Service Canceled';
        $this->db->join('t_port_odp','t_port_odp.fk_odp = t_revitalisasi_odp.id_revitalisasi_odp');
        $this->db->where('trouble', $trouble);
        $hasil = $this->db->get('t_revitalisasi_odp');

        return $hasil->result();      
        // print_r($hasil->result());exit();

    }

    function ambil_noservice()
	{
        $trouble = 'ONT Detect No Service';
        $this->db->join('t_port_odp','t_port_odp.fk_odp = t_revitalisasi_odp.id_revitalisasi_odp');
        $this->db->where('trouble', $trouble);
        $hasil = $this->db->get('t_revitalisasi_odp');

        return $hasil->result();      
        // print_r($hasil->result());exit();

    }

    function ambil_susahjangkau()
	{
        $trouble = 'ODP Susah Di Jangkau';
        $this->db->join('t_port_odp','t_port_odp.fk_odp = t_revitalisasi_odp.id_revitalisasi_odp');
        $this->db->where('trouble', $trouble);
        $hasil = $this->db->get('t_revitalisasi_odp');

        return $hasil->result();      
        // print_r($hasil->result());exit();

    }

    function ambil_kapasitas()
	{
        $trouble = 'Kapasitas';
        $this->db->join('t_port_odp','t_port_odp.fk_odp = t_revitalisasi_odp.id_revitalisasi_odp');
        $this->db->where('trouble', $trouble);
        $hasil = $this->db->get('t_revitalisasi_odp');

        return $hasil->result();      
        // print_r($hasil->result());exit();

    }

    function jumlah_data()
	{
        $query = $this->db->get('t_port_odp');
        return $query->num_rows();
        
    }

    
    function jumlah_input_dava()
	{
        $status = 'DONE';
        $this->db->select('input_dava');
        $this->db->where('input_dava =',$status);
        // $this->db->group_by('odp_name'); 
        // $this->db->order_by('total', 'desc'); 
        $hasil = $this->db->get('t_port_odp');
        return $hasil->num_rows();;
    }
    function jumlah_disconnect()
	{
        $status = 'Disconnect';
        $this->db->select('trouble');
        $this->db->where('trouble =',$status);
        // $this->db->order_by('total', 'desc'); 
        $hasil = $this->db->get('t_port_odp');
        return  $hasil->num_rows();
    }
    function jumlah_redaman_tinggi()
	{
        $status = 'Redaman Tinggi';
        $this->db->select('trouble');
        $this->db->where('trouble =',$status);
        // $this->db->order_by('total', 'desc'); 
        $hasil = $this->db->get('t_port_odp');
        return  $hasil->num_rows();
    }
    function jumlah_stuck_bi()
	{
        $status = 'Service Stuck BI';
        $this->db->select('trouble');
        $this->db->where('trouble =',$status);
        // $this->db->order_by('total', 'desc'); 
        $hasil = $this->db->get('t_port_odp');
        return  $hasil->num_rows();
    }
    function jumlah_tidak_detek()
	{
        $status = 'Tidak Detek Full';
        $status1 = 'Tidak Detek Sebagian';
        $this->db->select('trouble');
        $this->db->where('trouble =',$status);
        $this->db->where('trouble =',$status1);
        // $this->db->order_by('total', 'desc'); 
        $hasil = $this->db->get('t_port_odp');
        return  $hasil->num_rows();
    }

    function jumlah_splitter()
	{
        $status = 'Splitter';
        $this->db->select('trouble');
        $this->db->where('trouble =',$status);
        // $this->db->order_by('total', 'desc'); 
        $hasil = $this->db->get('t_port_odp');
        return  $hasil->num_rows();
    }

    function jumlah_kapasitas()
	{
        $status = 'Kapasitas';
        $this->db->select('trouble');
        $this->db->where('trouble =',$status);
        // $this->db->order_by('total', 'desc'); 
        $hasil = $this->db->get('t_port_odp');
        return  $hasil->num_rows();
    }

    function jumlah_tidak_bisa_create_new_version()
	{
        $status = 'Tidak Bisa Create New Version';
        $this->db->select('trouble');
        $this->db->where('trouble =',$status);
        // $this->db->order_by('total', 'desc'); 
        $hasil = $this->db->get('t_port_odp');
        return  $hasil->num_rows();
    }
    function jumlah_service_suspend()
	{
        $status = 'Service Suspend';
        $this->db->select('trouble');
        $this->db->where('trouble =',$status);
        // $this->db->order_by('total', 'desc'); 
        $hasil = $this->db->get('t_port_odp');
        return  $hasil->num_rows();
    }
    function jumlah_service_canceled()
	{
        $status = 'Service Canceled';
        $this->db->select('trouble');
        $this->db->where('trouble =',$status);
        // $this->db->order_by('total', 'desc'); 
        $hasil = $this->db->get('t_port_odp');
        return  $hasil->num_rows();
    }
    function jumlah_ont_detect_no_service()
	{
        $status = 'ONT Detect No Service';
        $this->db->select('trouble');
        $this->db->where('trouble =',$status);
        // $this->db->order_by('total', 'desc'); 
        $hasil = $this->db->get('t_port_odp');
        return  $hasil->num_rows();
    }

    function jumlah_odp_susah_di_jangkau()
	{
        $status = 'ODP Susah Di Jangkau';
        $this->db->select('trouble');
        $this->db->where('trouble =',$status);
        // $this->db->order_by('total', 'desc'); 
        $hasil = $this->db->get('t_port_odp');
        return  $hasil->num_rows();
    }

    
    
    function jumlah_odp_penuh()
	{
        $status = 'ODP Penuh';
        $this->db->select('trouble');
        $this->db->where('trouble =',$status);
        // $this->db->order_by('total', 'desc'); 
        $hasil = $this->db->get('t_port_odp');
        return  $hasil->num_rows();
    }
    function jumlah_node_b()
	{
        $status = 'Node B';
        $this->db->select('odp_name');
        $this->db->where('trouble =',$status);
        $hasil = $this->db->get('t_port_odp');
        return  $hasil->num_rows();
    }
    function jumlah_ccan()
	{
        $status = 'CCAN';
        $this->db->select('trouble');
        $this->db->where('trouble =',$status);
        // $this->db->order_by('total', 'desc'); 
        $hasil = $this->db->get('t_port_odp');
        return  $hasil->num_rows();
    }
    function jumlah_wifi_id()
	{
        $status = 'WIFI ID';
        $this->db->select('trouble');
        $this->db->where('trouble =',$status);
        // $this->db->order_by('total', 'desc'); 
        $hasil = $this->db->get('t_port_odp');
        return  $hasil->num_rows();
    }
    function jumlah_astinet()
	{
        $status = 'ASTINET';
        $this->db->select('trouble');
        $this->db->where('trouble =',$status);
        // $this->db->order_by('total', 'desc'); 
        $hasil = $this->db->get('t_port_odp');
        return  $hasil->num_rows();
    }
    function jumlah_vpn()
	{
        $status = 'VPN';
        $this->db->select('trouble');
        $this->db->where('trouble =',$status);
        // $this->db->order_by('total', 'desc'); 
        $hasil = $this->db->get('t_port_odp');
        return  $hasil->num_rows();
    }
    
    function jumlah_odp_persto()
    {
        $this->db->select('odp_name, COUNT(*) as jumlah_odp');
        $this->db->group_by('odp_name');
        // $this->db->order_by('jumlah_odp'); 
        // 
        $query = $this->db->get('t_port_odp');
        // print_r($query ->result());exit();
        
        return $query->result();


       

    }
    function jumlah_sto()
	{
        $this->db->select('trouble, COUNT(trouble) as total');
        $this->db->group_by('trouble'); 
        $this->db->order_by('total', 'desc'); 
        $hasil = $this->db->get('t_port_odp');
        return $hasil->num_rows();
    }
    
    function jumlah_update_uim()
	{
        $status = 'DONE';
        $this->db->select(' update_uim');
        $this->db->where('update_uim =', $status);
        // $this->db->group_by('update_uim'); 
        // $this->db->order_by('total', 'desc'); 
        $hasil = $this->db->get('t_port_odp');
        return $hasil->num_rows();
    }

    // check if odp already existed
    public function checkOdp($odp) {
        $status = FALSE;
        $this->db->select('id_odp');
        $this->db->from('t_revitalisasi_odp');
        $this->db->where('odp_name', $odp);
        $query = $this->db->get();
        
        if ($query->num_rows() > 0) {
            return $query->row()->id_odp;
        }
        return false;
    }

    // import odp
    public function insertOdp($data){
        $this->db->insert('t_revitalisasi_odp', $data);

        $insert_id = $this->db->insert_id();

        return  $insert_id;
    }

    //import odp port
    public function insertPort($data){
        $this->db->insert('t_port_odp', $data);
    }

    //get all data feeder
    public function get_feeder()
    {
        $this->db->select('*');
        $this->db->join('t_ftm',' t_ftm.id_ftm=t_feeder.fk_ftm');
        $hasil = $this->db->get('t_feeder');
	    return $hasil->result();
    }

    //create new feeder
    function create_feeder()
    {
        $data = array
            (
                'fk_ftm' => $this->input->post('fk_ftm'),
                'nama_kabel_feeder' => $this->input->post('nama_feeder')
            );
    
        $this->db->insert('t_feeder',$data); 
    }

    //get all data odc
    public function get_odc()
    {
        $this->db->select('*');
        $this->db->join('t_feeder',' t_feeder.id_feeder=t_odc.fk_feeder');
        $hasil = $this->db->get('t_odc');
	    return $hasil->result();
    }

    //get detail ftm
    public function get_ftm_detail($id)
    {
        return $this->db->get_where('t_ftm', ["id_ftm" => $id])->result();
    }

    function UpdateByIdFeeder($id)
    {
        $data = array
            (
                'fk_ftm' => $this->input->post('fk_ftm'),
                'nama_kabel_feeder' => $this->input->post('nama_feeder'),
            );
            // print_r($data);exit();
                $this->db->where('id_feeder',$id);
                $this->db->update('t_feeder',$data);
    }

    //get detail ftm
    public function get_feeder_detail($id)
    {
        return $this->db->get_where('t_feeder', ["id_feeder" => $id])->result();
    }

    function hapus_feeder($id)
	{
		$hasil = $this->db->delete('t_feeder', array("id_feeder" => $id));
		return $hasil;
    }

    //create new odc
    function create_odc()
    {
        $data = array
            (
                'fk_feeder' => $this->input->post('fk_feeder'),
                'nama_odc' => $this->input->post('nama_odc'),
                'nama_location_odc' => $this->input->post('nama_location_odc'),
                'nama_frame_odc' => $this->input->post('nama_frame_odc'),
                'panel_in_panel_port_core' => $this->input->post('panel_in_panel_port_core'),
                'splitter_name' => $this->input->post('splitter_name'),
                'splitter_out' => $this->input->post('splitter_out'),
                'panel_out_panel_port_core' => $this->input->post('panel_out_panel_port_core'),
                'kap' => $this->input->post('kap'),
                'kordinat_long' => $this->input->post('kordinat_long'),
                'kordinat_lang' => $this->input->post('kordinat_lang'),
            );
    
        $this->db->insert('t_odc',$data); 
    }

    //get detail ftm
    public function get_odc_detail($id)
    {
        return $this->db->get_where('t_odc', ["id_odc" => $id])->result();
    }

    function UpdateByIdOdc($id)
    {
        $data = array
            (
                'fk_feeder' => $this->input->post('fk_feeder'),
                'nama_odc' => $this->input->post('nama_odc'),
                'nama_location_odc' => $this->input->post('nama_location_odc'),
                'nama_frame_odc' => $this->input->post('nama_frame_odc'),
                'panel_in_panel_port_core' => $this->input->post('panel_in_panel_port_core'),
                'splitter_name' => $this->input->post('splitter_name'),
                'splitter_out' => $this->input->post('splitter_out'),
                'panel_out_panel_port_core' => $this->input->post('panel_out_panel_port_core'),
                'kap' => $this->input->post('kap'),
                'kordinat_long' => $this->input->post('kordinat_long'),
                'kordinat_lang' => $this->input->post('kordinat_lang'),
            );
            // print_r($data);exit();
                $this->db->where('id_odc',$id);
                $this->db->update('t_odc',$data);
    }

    function hapus_odc($id)
	{
		$hasil = $this->db->delete('t_odc', array("id_odc" => $id));
		return $hasil;
    }

    function get_all(){
        $this->db->select('*');
        $this->db->from('t_odp odp');
        $this->db->join('t_distribusi dis','dis.id_distribusi = odp.fk_distribusi');
        $this->db->join('t_odc odc','odc.id_odc = dis.fk_odc');
        $this->db->join('t_feeder fee','fee.id_feeder = odc.fk_feeder');
        $this->db->join('t_ftm ftm','ftm.id_ftm = fee.fk_ftm');
        $this->db->join('t_bundlecore bun','bun.id_bundlecore = ftm.fk_bundlecore');
        $this->db->join('t_olt olt','olt.id_olt=bun.fk_olt');
        $hasil = $this->db->get('t_odp');
	    return $hasil->result();
    }
}
