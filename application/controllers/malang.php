<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class malang extends CI_Controller {

	public function __construct(){
		parent::__construct();
		$this->load->model('masterodp');
	}
	
	public function index()
	{	
		$this->load->helper('url');
		$masterodp = $this->masterodp;
		$data["total_lawang"] = $masterodp->jumlah_lawang();
		$data["total_singosari"] = $masterodp->jumlah_singosari();
		$data["total_pakis"] = $masterodp->jumlah_pakis();
		$data["total_sawojajar"] = $masterodp->jumlah_sawojajar();
		$data["total_tumpang"] = $masterodp->jumlah_tumpang();
		$data["total_blimbing"] = $masterodp->jumlah_blimbing();
		$data["total_klojen"] = $masterodp->jumlah_klojen();
		$data["total_malangkota"] = $masterodp->jumlah_malangkota();
		$data["total_gadang"] = $masterodp->jumlah_gadang();
		$data["total_buring"] = $masterodp->jumlah_buring();
		$data["total_dampit"] = $masterodp->jumlah_dampit();
		$data["total_kepanjen"] = $masterodp->jumlah_kepanjen();
		$data["total_turen"] = $masterodp->jumlah_turen();		

		$this->load->view('datel_malang', $data);
	}

	public function odp_page()
     {

          // Datatables Variables
          $draw = intval($this->input->get("draw"));
          $start = intval($this->input->get("start"));
          $length = intval($this->input->get("length")); 


          $odp = $this->masterodp->getBySTO($sto);

          $data = array();

          foreach($odp->result() as $r) {

				$row = array();
				$row[] = $r->datel;
				$row[] = $r->sto;
				$row[] = $r->odp;
				$row[] = $r->latitude;
				$row[] = $r->longitude;
				$row[] = $r->alamat;
				$row[] = $r->avai;
				$row[] = $r->reserved;
				$row[] = $r->in_service;
				$row[] = $r->total;
				$row[] = $r->tanggal_golive;
				$row[] = $r->bulan;
				$row[] = $r->tahun;
				$row[] = $r->kelurahan;
				$row[] = $r->kecamatan;
				$row[] = $r->kota_kab;
				$row[] = $r->qrcode_odp;
				$row[] = $r->qrcode_port;
				$row[] = $r->project;
				$row[] = $r->vendor;
				$row[] = $r->merk;
				$row[] = $r->ip;

				//add html for action
				$row[] = '<a class="btn btn-sm btn-success" href="lihat/'.$r->id_odp.'" title="Lihat"><i class="glyphicon glyphicon-folder"></i> Lihat</a>
				<a class="btn btn-sm btn-warning" href="update/'.$r->id_odp.'" title="Lihat"><i class="glyphicon glyphicon-folder"></i> Update</a>
				<a href="delete/'.$r->id_odp.'" class="btn btn-sm btn-danger" onclick="return confirm(\'Anda yakin untuk menghapus data ini?\');"><i class="fa fa-trash"></i>Hapus</a>
				';

				$data[] = $row;
          }

          $output = array(
               "draw" => $draw,
                 "recordsTotal" => $odp->num_rows(),
                 "recordsFiltered" => $odp->num_rows(),
                 "data" => $data
            );
          echo json_encode($output);
          exit();
    }

	function sto($sto = null){

		$data["odp"] = $this->masterodp->getBySTO($sto);
		$data["sto"] = $this->masterodp->ambil_sto($sto);		

		$this->load->view('sto_lihat', $data);
		// print_r($data); exit();



		// // Datatables Variables
		// $draw = intval($this->input->get("draw"));
		// $start = intval($this->input->get("start"));
		// $length = intval($this->input->get("length")); 

		// $odp = $this->masterodp->getBySTO($sto);
		// // print_r($odp);exit();

        //   $data = array();

        //   foreach($odp->result() as $r) {

		// 		$row = array();
		// 		$row[] = $r->datel;
		// 		$row[] = $r->sto;
		// 		$row[] = $r->odp;
		// 		$row[] = $r->latitude;
		// 		$row[] = $r->longitude;
		// 		$row[] = $r->alamat;
		// 		$row[] = $r->avai;
		// 		$row[] = $r->reserved;
		// 		$row[] = $r->in_service;
		// 		$row[] = $r->total;
		// 		$row[] = $r->tanggal_golive;
		// 		$row[] = $r->bulan;
		// 		$row[] = $r->tahun;
		// 		$row[] = $r->kelurahan;
		// 		$row[] = $r->kecamatan;
		// 		$row[] = $r->kota_kab;
		// 		$row[] = $r->qrcode_odp;
		// 		$row[] = $r->qrcode_port;
		// 		$row[] = $r->project;
		// 		$row[] = $r->vendor;
		// 		$row[] = $r->merk;
		// 		$row[] = $r->ip;

		// 		//add html for action
		// 		$row[] = '<a class="btn btn-sm btn-success" href="lihat/'.$r->id_odp.'" title="Lihat"><i class="glyphicon glyphicon-folder"></i> Lihat</a>
		// 		<a class="btn btn-sm btn-warning" href="update/'.$r->id_odp.'" title="Lihat"><i class="glyphicon glyphicon-folder"></i> Update</a>
		// 		<a href="delete/'.$r->id_odp.'" class="btn btn-sm btn-danger" onclick="return confirm(\'Anda yakin untuk menghapus data ini?\');"><i class="fa fa-trash"></i>Hapus</a>
		// 		';

		// 		$data[] = $row;
		//   }
		  

        //   $output = array(
        //        "draw" => $draw,
        //          "recordsTotal" => $odp->num_rows(),
        //          "recordsFiltered" => $odp->num_rows(),
        //          "data" => $data
        //     );
		//   echo json_encode($output);
		  
		  
	}
}