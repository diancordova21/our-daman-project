<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <meta name="description" content="">
  <meta name="author" content="">

  <title>Master ODP</title>

  <!-- Custom fonts for this template-->
  <link href="<?= base_url()?>asset/vendor/fontawesome-free/css/all.min.css" rel="stylesheet" type="text/css">
  <link href="<?= base_url()?>https://fonts.googleapis.com/css?family=Nunito:200,200i,300,300i,400,400i,600,600i,700,700i,800,800i,900,900i" rel="stylesheet">
  <link rel="icon" href="<?= base_url()?>asset/img/icon-telkom.ico" type="image/x-icon">

  <!-- Custom styles for this template-->
  <link href="<?= base_url()?>asset/css/sb-admin-2.min.css" rel="stylesheet">

</head>

<body id="page-top">

  <!-- Page Wrapper -->
  <div id="wrapper">
    <?php include_once("sidebar.php") ?>
    <!-- Content Wrapper -->
    <div id="content-wrapper" class="d-flex flex-column">

      <!-- Main Content -->
      <div id="content">
        <?php include_once("topbar.php") ?>

        <!-- Begin Page Content -->
        <div class="container-fluid">

        <!-- DataTales Example -->
        <div class="card shadow mb-4">
            <div class="card-header py-3">
              <h3 class="m-0 font-weight-bold text-primary">STO: <?php echo $sto->sto; ?></h3>
            </div>
            <div class="card-body">
              <div class="table-responsive">
              <table id="helo" class="ui table stripe" style="border-radius: 0px;">
                  <thead>
                    <tr>
                      <th>Datel</th>
                      <th>STO</th>
                      <th>ODP</th>
                      <th>Latitude</th>
                      <th>Longitude</th>
                      <th>Alamat</th>
                      <th>Available</th>
                      <th>Reserved</th>
                      <th>In Service</th>
                      <th>Total</th>
                      <th>Tanggal GoLive</th>
                      <th>Bulan</th>
                      <th>Tahun</th>
                      <th>Kelurahan</th>
                      <th>Kecamatan</th>
                      <th>Kota/Kab</th>
                      <th>QR Code ODP</th>
                      <th>QR Code Port</th>
                      <th>Project</th>
                      <th>Vendor</th>
                      <th>Merk</th>
                      <th>IP</th>
                      <th>Aksi</th>
                    </tr>
                  </thead>
                    <tbody>
                        <?php foreach ($odp as $data) : ?>
                            <tr>
                                <td><?= $data->datel ?></td>
                                <td><?= $data->sto ?></td>
                                <td><?= $data->odp ?></td>
                                <td><?= $data->latitude ?></td>
                                <td><?= $data->longitude ?></td>
                                <td><?= $data->alamat ?></td>
                                <td><?= $data->avai ?></td>
                                <td><?= $data->reserved ?></td>
                                <td><?= $data->in_service ?></td>
                                <td><?= $data->total ?></td>
                                <td><?= $data->tanggal_golive ?></td>
                                <td><?= $data->bulan ?></td>
                                <td><?= $data->tahun ?></td>
                                <td><?= $data->kelurahan ?></td>
                                <td><?= $data->kecamatan ?></td>
                                <td><?= $data->kota_kab ?></td>
                                <td><?= $data->qrcode_odp ?></td>
                                <td><?= $data->qrcode_port ?></td>
                                <td><?= $data->project ?></td>
                                <td><?= $data->vendor ?></td>
                                <td><?= $data->merk ?></td>
                                <td><?= $data->ip ?></td>
                                <td>
                                <div class="ui buttons mini">
                                    <a class="btn btn-sm btn-success" href="<?= site_url('table/lihat/' . $data->id_odp) ?>" title="Lihat"><i class="glyphicon glyphicon-folder"></i> Lihat</a>
                                    <a class="btn btn-sm btn-warning" href="<?= site_url('table/update/' . $data->id_odp) ?>" title="Lihat"><i class="glyphicon glyphicon-folder"></i> Update</a>
                                    <a href="<?= site_url('table/delete/' . $data->id_odp) ?>" class="btn btn-sm btn-danger" onclick="return confirm(\'Anda yakin untuk menghapus data ini?\');">Hapus</a>
                                </div>
                                </td>
                            </tr>
                        <?php endforeach; ?>
                    </tbody>
                </table>
              </div>
            </div>
          </div>
          
          
        </div>


      </div>
      <!-- End of Main Content -->

    </div>
    <!-- End of Content Wrapper -->

  </div>
  <!-- End of Page Wrapper -->

  <!-- Scroll to Top Button-->
  <a class="scroll-to-top rounded" href="<?= base_url()?>#page-top">
    <i class="fas fa-angle-up"></i>
  </a>

  <!-- Logout Modal-->
  <div class="modal fade" id="logoutModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title" id="exampleModalLabel">Ready to Leave?</h5>
          <button class="close" type="button" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">×</span>
          </button>
        </div>
        <div class="modal-body">Select "Logout" below if you are ready to end your current session.</div>
        <div class="modal-footer">
          <button class="btn btn-secondary" type="button" data-dismiss="modal">Cancel</button>
          <a class="btn btn-danger" href="<?= site_url('auth/admin/logout') ?>" class="item">Logout</a>
        </div>
      </div>
    </div>
  </div>



  <!-- Bootstrap core JavaScript-->
  <script src="<?= base_url()?>asset/vendor/jquery/jquery.min.js"></script>
  <script src="<?= base_url()?>asset/vendor/bootstrap/js/bootstrap.bundle.min.js"></script>

  <!-- Core plugin JavaScript-->
  <script src="<?= base_url()?>asset/vendor/jquery-easing/jquery.easing.min.js"></script>

  <!-- Custom scripts for all pages-->
  <script src="<?= base_url()?>asset/js/sb-admin-2.min.js"></script>
  <!-- Page level plugins -->
  <script src="<?= base_url()?>asset/vendor/datatables/jquery.dataTables.min.js"></script>
  <script src="<?= base_url()?>asset/vendor/datatables/dataTables.bootstrap4.min.js"></script>

  <!-- Page level custom scripts -->
  <script src="<?= base_url()?>asset/js/demo/datatables-demo.js"></script>
            
      <script type="text/javascript">
          $(document).ready(function() {
              $('#helo').DataTable();
          });
      </script>

</body>

</html>