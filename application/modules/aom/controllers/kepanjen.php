<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Kepanjen extends Modular {

	public function __construct(){
		authenticated();
        sessionAsAOM();
		parent::__construct();
		$this->load->model('Master_odp');
	}
	
	public function index()
	{	
		$this->load->helper('url');
		$Master_odp = $this->Master_odp;
		$data["jumlah_dampit"] = $Master_odp->jumlah_dampit();
		$data["jumlah_kepanjen"] = $Master_odp->jumlah_kepanjen();
		$data["jumlah_turen"] = $Master_odp->jumlah_turen();
		$data["jumlah_gondanglegi"] = $Master_odp->jumlah_gondanglegi();
		$data["jumlah_sumbermanjing"] = $Master_odp->jumlah_sumbermanjing();
		$data["jumlah_sumberpucung"] = $Master_odp->jumlah_sumberpucung();
		$data["jumlah_pagak"] = $Master_odp->jumlah_pagak();
		$data["jumlah_ampelgading"] = $Master_odp->jumlah_ampelgading();
		$data["jumlah_gunungkawi"] = $Master_odp->jumlah_gunungkawi();
		$data["jumlah_bantur"] = $Master_odp->jumlah_bantur();
		$data["jumlah_donomulyo"] = $Master_odp->jumlah_donomulyo();
		

		$this->load->view('datel_kepanjen', $data);
	}

	function sto($sto = null){

		$data["odp"] = $this->Master_odp->getBySTO($sto);
		$data["sto"] = $this->Master_odp->ambil_sto($sto);		

		// print_r($data['sto']);exit();
		$this->load->view('sto_lihat', $data);
	}
}