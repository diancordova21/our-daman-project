<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Table extends CI_Controller {

	public function __construct(){ 
		parent::__construct();
		$this->load->model('masterodp');
		$this->load->library('form_validation'); 
	}
	  
	public function index()
	{	
		$this->load->helper('url');
		// $data['pageName'] = "tables";
		// $this->load->view('index', $data);
		//    echo json_encode($output);
		$this->load->view('tables', array());
	}

	public function data_page()
    {
		$rev = array();
		$rev = $this->masterodp->get_data();

		// print_r($rev->result());exit();
		foreach($rev->result() as $r) {
			$row = array();
			$row[] = $r->date;
			$row[] = $r->odp_name;
			$row[] = $r->ip_address;
			$row[] = $r->slot_port;
			$row[] = $r->port_odp;
			$row[] = $r->status;
			$row[] = $r->inet_voice_customer;
			$row[] = $r->qr_code;
			$row[] = $r->description;
			$row[] = $r->update_uim;
			$row[] = $r->input_dava;
			$row[] = $r->port_uim;
			$row[] = $r->dc_dismantled;
			$row[] = $r->trouble;
			// $row[] = $r->trouble;
			$row[] = 
				'<a class="btn btn-sm btn-success" href="table/lihat/'.$r->id_revitalisasi.'" title="Lihat"><i class="glyphicon glyphicon-folder"></i> Lihat</a>
				<a class="btn btn-sm btn-warning" href="table/update/'.$r->id_revitalisasi.'" title="Lihat"><i class="glyphicon glyphicon-folder"></i> Update</a>
				<a href="table/delete/'.$r->id_revitalisasi.'" class="btn btn-sm btn-danger" onclick="return confirm(\'Anda yakin untuk menghapus data ini?\');"> Hapus</a>
				';

			//add html for action
			
			$data[] = $row;
	  	}

	  	$output = array(
		// "draw" => $draw,
		  "recordsTotal" => $rev->num_rows(),
		  "recordsFiltered" => $rev->num_rows(),
		  "data" => $data
	 	);
    	echo json_encode($output);
        exit();
	}
	
	public function port_page($id)
    {
		$rev = array();
		$rev = $this->masterodp->get_port($id);

		// print_r($rev->result());exit();
		foreach($rev->result() as $r) {
			$row = array();
			$row[] = $r->no_port;
			$row[] = $r->status;
			$row[] = $r->inet_voice_customer;
			$row[] = $r->qr_code;
			$row[] = $r->description;
			$row[] = $r->update_uim;
			$row[] = $r->input_dava;
			$row[] = $r->port_uim;
			$row[] = $r->dc_dismantled;
			$row[] = $r->trouble;
			// $row[] = $r->trouble;
			$row[] = '<a class="btn btn-sm btn-success" href="table/lihat/'.$r->id_port.'" title="Lihat"><i class="glyphicon glyphicon-folder"></i> Lihat</a>
				<a class="btn btn-sm btn-warning" href="table/update/'.$r->id_port.'" title="Lihat"><i class="glyphicon glyphicon-folder"></i> Update</a>
				<a href="table/delete/'.$r->id_port.'" class="btn btn-sm btn-danger" onclick="return confirm(\'Anda yakin untuk menghapus data ini?\');"> Hapus</a>
				';

			//add html for action
			$data[] = $row;
	  	}
	  	$output = array(
		// "draw" => $draw,
		  "recordsTotal" => $rev->num_rows(),
		  "recordsFiltered" => $rev->num_rows(),
		  "data" => $data
	 	);
        echo json_encode($output);
        exit();
	}
	

	//Lihat
	function update($id = null){
		if (!isset($id)) redirect('table/index');
		$masterodp = $this->masterodp;
		$data["odp"]=$masterodp->getById($id);
		$data["port_odp"]=$masterodp->getPortById($id);

		$this->load->view("tables_update", $data);
	}

	//Create
	function create(){

		$this->form_validation->set_rules(
			'odp_name', 'ODP',
			'required|is_unique[t_odp.odp_name]',
			array(
					'required'      => 'The %s field is required.',
					'is_unique'     => 'This %s already exists.'
			)
		);

		$this->form_validation->set_rules('date','Tanggal','required');
		// $this->form_validation->set_rules('odp_name','ODP','required|is_unique[t_odp.odp_name]');
		$this->form_validation->set_rules('ip_address','IP Address','required');
		$this->form_validation->set_rules('slot_port','Slot Port','required');

        if (isset($_POST['submit'])) {
			if ($this->form_validation->run() != false){
				$data["odp"] = $this->masterodp->create_odp();
				redirect('table/tambah_port/'.$data['odp']->id_odp);
			}			
			else{
				$data = array(
					'date' => $this->input->post('date'),//check this
					'odp_name' => $this->input->post('odp_name'),//check this
					'ip_address' => $this->input->post('ip_address'),//check this
					'slot_port' => $this->input->post('slot_port')//check this
				);	
				// print_r($data);exit();
				
				$this->load->view("tables_create", $data);
			}
		} 
		else {
			$this->load->view("tables_create");
		}
	}

	//Tambah Port
	function tambah_port($id = null){
		// print_r($this->input->post('id_odp'));exit();
		if (!isset($id)) redirect('table/index');
		$masterodp = $this->masterodp;
		$data["odp"] = $masterodp->getByODP($id);
		$data["port"] = $masterodp->get_port($id);
		// print_r($data);exit();
		// $this->load->view('tables_create_port', $data);
		
		if (isset($_POST['submit'])) {
			$this->masterodp->create_port();
			redirect('table/tambah_port/'.$data['odp']->id_odp);
		} else {
			// $this->load->view("tables_create");
		$this->load->view('tables_create_port', $data);

		}
        // if (isset($_POST['submit'])) {
		// 	$this->masterodp->create_port();
		// 	redirect('table/tambah_port');
		// } else {
		// 	$this->load->view("tables_create_port", $data);
		// }
	}

	//Update
	function update_table($fk){
		if (isset($_POST['submit'])){
				// print_r($this->input->post('nama_odp'));exit();
				// print_r($this->input->post('status'));exit();
				// print_r($this->input->post('id_port'));exit();
		$masterodp = $this->masterodp;

                $this->masterodp->UpdateTableById($fk);
				redirect('table/update/'.$fk);

			}
         else {
			if (!isset($fk)) redirect('table/update'.$fk);

        }
	
	}
	function lihat($id = null){
		if (isset($_POST['submit'])){
				// print_r($this->input->post('nama_odp'));exit();
		$masterodp = $this->masterodp;
				
				$data["odp"]=$masterodp->getById($id);
				$data["port_odp"]=$masterodp->getPortById($id);
				redirect('table/index');
			}
        else {
			if (!isset($id)) redirect('table/index');
			$masterodp = $this->masterodp;

			$data["odp"]=$masterodp->getById($id);
			$data["port_odp"]=$masterodp->getPortById($id);
			$data["masterodp"]=$masterodp->getById($id);
			
	
			$this->load->view("tables_lihat", $data);
        }
	
	}

	function delete($id=null)
	{
	    if (!isset($id)) show_404();

	    if ($this->masterodp->hapus($id)) {
		    redirect(site_url('table/index'));
	    }
	}


	public function view(){
		$search = $_POST['search']['value']; // Ambil data yang di ketik user pada textbox pencarian
		$limit = $_POST['length']; // Ambil data limit per page
		$start = $_POST['start']; // Ambil data start
		$order_index = $_POST['order'][0]['column']; // Untuk mengambil index yg menjadi acuan untuk sorting
		$order_field = $_POST['columns'][$order_index]['data']; // Untuk mengambil nama field yg menjadi acuan untuk sorting
		$order_ascdesc = $_POST['order'][0]['dir']; // Untuk menentukan order by "ASC" atau "DESC"
		$sql_total = $this->masterodp->count_all(); // Panggil fungsi count_all pada SiswaModel
		$sql_data = $this->masterodp->filter($search, $limit, $start, $order_field, $order_ascdesc); // Panggil fungsi filter pada SiswaModel
		$sql_filter = $this->masterodp->count_filter($search); // Panggil fungsi count_filter pada SiswaModel
		$callback = array(
			'draw'=>$_POST['draw'], // Ini dari datatablenya
			'recordsTotal'=>$sql_total,
			'recordsFiltered'=>$sql_filter,
			'data'=>$sql_data
		);
		header('Content-Type: application/json');
		echo json_encode($callback); // Convert array $callback ke json
	}


}