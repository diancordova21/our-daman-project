<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <meta name="description" content="">
  <meta name="author" content="">

  <title>Tambah ODP</title>

  <!-- Custom fonts for this template-->
  <link href="<?= base_url()?>asset/vendor/fontawesome-free/css/all.min.css" rel="stylesheet" type="text/css">
  <link href="<?= base_url()?>https://fonts.googleapis.com/css?family=Nunito:200,200i,300,300i,400,400i,600,600i,700,700i,800,800i,900,900i" rel="stylesheet">

  <!-- Custom styles for this template-->
  <link href="<?= base_url()?>asset/css/sb-admin-2.min.css" rel="stylesheet">

</head>

<body id="page-top">

  <!-- Page Wrapper -->
  <div id="wrapper">
    <?php include_once("sidebar.php") ?>
    <!-- Content Wrapper -->
    <div id="content-wrapper" class="d-flex flex-column">

      <!-- Main Content -->
      <div id="content">
        <?php include_once("topbar.php") ?>

        <!-- Begin Page Content -->
        <div class="container">
          
        
          <div class="card shadow mb-4">
              <div class="card-header py-3">
                <h3 class="m-0 font-weight-bold text-primary">Tambahkan ODP</h3>
              </div>
              <div class="card-body">
                <form method="post" action="<?= site_url('table/create') ?>" enctype="multipart/form-data"> 
                  <div class="row">
                  
                    <div class="col-xl-6">
                      <div class="p-7">
                        <!-- <div class="container"> -->
                            <div class="form-group row">
                              <div class="col-sm-10 mb-3 mb-sm-0">
                                <label>Tanggal</label>
                                <input type="date" class="form-control form-control-user" name="date" value="<?php echo set_value('date');?>">
                                <?php echo form_error('date','<p class="alert alert-danger">','</p>'); ?>
                              </div>
                            </div>
                            <div class="form-group row">
                              <div class="col-sm-10 mb-3 mb-sm-0">
                                <label>ODP</label>
                                <input type="text" class="form-control form-control-user" name="odp_name" value="<?php echo set_value('odp_name');?>">
                                <?php echo form_error('odp_name','<p class="alert alert-danger">','</p>'); ?>
                              </div>                  
                            </div>
                            <div class="form-group row">
                              <div class="col-sm-10 mb-3 mb-sm-0">
                                  <label>IP Address</label>
                                  <input type="text" class="form-control form-control-user" name="ip_address" value="<?php echo set_value('ip_address');?>">
                                  <?php echo form_error('ip_address','<p class="alert alert-danger">','</p>'); ?>
                              </div>
                            </div>
                            <div class="form-group row">
                              <div class="col-sm-10 mb-3 mb-sm-0">
                                  <label>Slot Port</label>
                                  <input type="text" class="form-control form-control-user" name="slot_port" value="<?php echo set_value('slot_port');?>">
                                  <?php echo form_error('slot_port','<p class="alert alert-danger">','</p>'); ?>
                              </div>
                            </div>
                        </div>                            
                      </div>
                    </div>
                  </div>
                    
                <div class="col-sm-10 mb-3 mb-sm-0" style="padding-bottom:2%">
                  <button name="submit" type="submit" class="btn btn-primary btn-user">Tambah</button>
                </div>
                       
                </form>
              </div>
            
            
          </div>
           
          <div class="card shadow mb-4" style="margin:3%">
              <div class="card-header py-3">
                <div class="row">
                <h3 class="m-0 font-weight-bold text-primary">Table Port</h3>
                  <!-- <button name="submit" type="submit" class="btn btn-success btn-user" style="margin-left:75%">Tambah</button> -->
                  </div>
              </div>
              <div class="card-body">
                <form method="post" action="<?= site_url('table/create') ?>" enctype="multipart/form-data"> 
                  <div class="row">
                  
                 
                  </div>
                </form>
              </div>
            
          </div>

        </div>


      </div>
      <!-- End of Main Content -->

    </div>
    <!-- End of Content Wrapper -->

  </div>
  <!-- Form Isian Dalam Modal-->
  <!-- 
     <div class="form-group row">
                              <div class="col-sm-10 mb-3 mb-sm-0">
                                  <label>Slot Port</label>
                                  <input type="text" class="form-control form-control-user" name="slot_port">
                              </div>
                            </div>
                            <div class="form-group row">
                              <div class="col-sm-10 mb-3 mb-sm-0">
                                  <label>No. Port</label>
                                  <input type="text" class="form-control form-control-user" name="port_odp">
                              </div>
                            </div>
                            <div class="form-group row">
                              <div class="col-sm-10 mb-3 mb-sm-0">
                                  <label>Status</label>
                                  <input type="text" class="form-control form-control-user" name="status">
                              </div>
                            </div>
                            <div class="form-group row">
                              <div class="col-sm-10 mb-3 mb-sm-0">
                                  <label>No Inet / Voice</label>
                                  <input type="text" class="form-control form-control-user" name="inet_voice_customer">
                              </div>
                            </div>
                            </div>
                          </div>
                          <div class="col">
                            <div class="form-group row">
                              <div class="col-sm-10 mb-3 mb-sm-0">
                                  <label>QR Code</label>
                                  <input type="text" class="form-control form-control-user" name="qr_code">
                              </div>
                            </div>
                            <div class="form-group row">
                              <div class="col-sm-10 mb-3 mb-sm-0">
                                  <label>Keterangan</label>
                                  <input type="text" class="form-control form-control-user" name="description">
                              </div>
                            </div>
                            <div class="form-group row">
                              <div class="col-sm-10 mb-3 mb-sm-0">
                                <label for="update_uim">Update UIM</label>
                                  <select class="form-control" id="update_uim" name="update_uim">
                                    <option value="Done">Done</option>
                                    <option value="Inputan">Inputan</option>
                                    <option value="Disconnect">Disconnect</option>
                                    <option value="Nomor Tidak Detek">Nomor Tidak Detek</option>
                                    <option value="Node B">Node B</option>
                                    <option value="CCAN">CCAN</option>
                                    <option value="Wifi ID">Wifi ID</option>
                                    <option value="ASTINET">ASTINET</option>
                                    <option value="VPN">VPN</option>
                                  </select>
                              </div>
                            </div>
                            <div class="form-group row">
                              <div class="col-sm-10 mb-3 mb-sm-0">
                                <label for="update_dava">Update Dava</label>
                                  <select class="form-control" id="update_dava" name="input_dava">
                                    <option value="Done">Done</option>
                                    <option value="Qrcode Not Found">Qrcode Not Found</option>
                                    <option value="Relabel">Relabel</option>
                                    <option value="Qrcode Sudah Terpakai">Qrcode Sudah Terpakai</option>
                                    <option value="Sudah Terlabel">Sudah Terlabel</option>
                                  </select>
                              </div>
                            </div>
                            <div class="form-group row">
                              <div class="col-sm-10 mb-3 mb-sm-0">
                                  <label>Port UIM</label>
                                  <input type="text" class="form-control form-control-user" name="port_uim">
                              </div>
                            </div>
                            <div class="form-group row">
                              <div class="col-sm-10 mb-3 mb-sm-0">
                                  <label>DC Dismantled</label>
                                  <input type="text" class="form-control form-control-user" name="dc_dismantled">
                              </div>
                            </div>
                            <div class="form-group row">
                              <div class="col-sm-10 mb-3 mb-sm-0">
                                <label for="trouble">Kendala</label>
                                  <select class="form-control" id="trouble" name="trouble">
                                    <option value="Disconnect">Disconnect</option>
                                    <option value="Service Tidak Detek">Service Tidak Detek</option>
                                    <option value="Service Stuck BI">Service Stuck BI</option>
                                    <option value="ODP Penuh">ODP Penuh</option>
                                    <option value="Node B">Node B</option>
                                    <option value="CCAN">CCAN</option>
                                    <option value="WIFI ID">WIFI ID</option>
                                    <option value="ASTINET">ASTINET</option>
                                    <option value="VPN">VPN</option>
                                  </select>
                              </div>
                            </div>
                            <div class="form-group row">
                              <div class="col-sm-10 mb-3 mb-sm-0">
                                <button name="submit" type="submit" class="btn btn-success btn-user">Submit</button>
                              </div>
                            </div>
   -->

  <!-- Scroll to Top Button-->
  <a class="scroll-to-top rounded" href="<?= base_url()?>#page-top">
    <i class="fas fa-angle-up"></i>
  </a>

  <!-- Logout Modal-->
  <div class="modal fade" id="logoutModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title" id="exampleModalLabel">Ready to Leave?</h5>
          <button class="close" type="button" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">×</span>
          </button>
        </div>
        <div class="modal-body">Select "Logout" below if you are ready to end your current session.</div>
        <div class="modal-footer">
          <button class="btn btn-secondary" type="button" data-dismiss="modal">Cancel</button>
          <a class="btn btn-primary" href="<?= base_url()?>login.html">Logout</a>
        </div>
      </div>
    </div>
  </div>


  <!-- Bootstrap core JavaScript-->
  <script src="<?= base_url()?>asset/vendor/jquery/jquery.min.js"></script>
  <script src="<?= base_url()?>asset/vendor/bootstrap/js/bootstrap.bundle.min.js"></script>

  <!-- Core plugin JavaScript-->
  <script src="<?= base_url()?>asset/vendor/jquery-easing/jquery.easing.min.js"></script>

  <!-- Custom scripts for all pages-->
  <script src="<?= base_url()?>asset/js/sb-admin-2.min.js"></script>
  <!-- Page level plugins -->
  <script src="<?= base_url()?>asset/vendor/datatables/jquery.dataTables.min.js"></script>
  <script src="<?= base_url()?>asset/vendor/datatables/dataTables.bootstrap4.min.js"></script>

  <!-- Page level custom scripts -->
  <script src="<?= base_url()?>asset/js/demo/datatables-demo.js"></script>
            
      <script type="text/javascript">
            $(document).ready(function() {
                $('#odp-table').DataTable({
                    
                    "ajax": {
                        url : "<?php echo site_url("table/odp_page") ?>",
                        type : 'GET'
                    },
                });
            });
            </script>

</body>

</html>