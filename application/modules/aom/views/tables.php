<!DOCTYPE html>
<html lang="en">

<head>
  <?php include_once("head.php"); ?>
</head>

<body id="page-top">

  <!-- Page Wrapper -->
  <div id="wrapper">
    <?php include_once("sidebar.php") ?>
    <!-- Content Wrapper -->
    <div id="content-wrapper" class="d-flex flex-column">

      <!-- Main Content -->
      <div id="content">
        <?php include_once("topbar.php") ?>

        <!-- Begin Page Content -->
        <div class="container-fluid">

          <!-- Page Heading -->
          <h1 class="h3 mb-2 text-gray-800">Tables</h1>

          <!-- DataTales Example -->
          <div class="card shadow mb-4">
            <div class="card-header py-3">
              <div class="row">
                <div class="col-lg-6 col-md-6">
                  <h6 class="m-0 font-weight-bold text-primary">Master ODP</h6>
                </div>
                <div class="col-lg-6 col-md-6">
                  <a class="btn btn-sm btn-success btn-left" href="table/create" title="Lihat"><i class="fa fa-plus" aria-hidden="true"></i>Add New</a>
                  <button type="button" class="btn btn-sm btn-success btn-right" data-toggle="modal" data-target="#importFile"><i class="fa fa-file-excel" aria-hidden="true"></i>  Import</button>
                </div>
              </div>
            </div>
            <div class="card-body">
              <div class="table-responsive">
                <table id="odp-table" class="table table-bordered table-hover" width="100%" cellspacing="0">
                  <thead>
                    <tr>
                      <th>Datel</th>
                      <th>STO</th>
                      <th>ODP</th>
                      <th>Latitude</th>
                      <th>Longitude</th>
                      <th>Alamat</th>
                      <th>Available</th>
                      <th>Reserved</th>
                      <th>In Service</th>
                      <th>Total</th>
                      <th>Tanggal GoLive</th>
                      <th>Bulan</th>
                      <th>Tahun</th>
                      <th>Kelurahan</th>
                      <th>Kecamatan</th>
                      <th>Kota/Kab</th>
                      <th>QR Code ODP</th>
                      <th>QR Code Port</th>
                      <th>Project</th>
                      <th>Vendor</th>
                      <th>Merk</th>
                      <th>IP</th>
                      <th>Aksi</th>
                    </tr>
                  </thead>

                  <tbody>
                  </tbody>
                </table>
              </div>
            </div>
          </div>
        </div>


      </div>
      <!-- End of Main Content -->

    </div>
    <!-- End of Content Wrapper -->

  </div>
  <!-- End of Page Wrapper -->

  <!-- Scroll to Top Button-->
  <a class="scroll-to-top rounded" href="<?= base_url()?>#page-top">
    <i class="fas fa-angle-up"></i>
  </a>

  <!-- Logout Modal-->
  <div class="modal fade" id="logoutModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title" id="exampleModalLabel">Ready to Leave?</h5>
          <button class="close" type="button" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">×</span>
          </button>
        </div>
        <div class="modal-body">Select "Logout" below if you are ready to end your current session.</div>
        <div class="modal-footer">
          <button class="btn btn-secondary" type="button" data-dismiss="modal">Cancel</button>
          <a class="btn btn-danger" href="<?= site_url('auth/admin/logout') ?>" class="item">Logout</a>
        </div>
      </div>
    </div>
  </div>

  <!-- Modal Import -->
  <form method="post" action="<?= site_url('aom/table/import');?>" enctype="multipart/form-data"> 
  <div class="row">
    <div class="modal fade" id="importFile" tabindex="" role="dialog" aria-labelledby="importFileTitle" aria-hidden="true">
      <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
          <div class="modal-header">
            <h5 class="modal-title" id="importFileTitle">Import File</h5>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
              <span aria-hidden="true">&times;</span>
            </button>
          </div>
          <div class="modal-body">
            <div class="row">
              <div class="col-md-4" style="margin-left:5%">
                <label class="custom-file-label" for="fileURL">Choose file...</label>
                <input type="file" class="custom-file-input" id="validatedCustomFile" name="fileURL">
              </div>
              <div class="col-md-2">
                <div id="loadergif" style="display:none">
                  <img src="<?= base_url()?>asset/img/loading.gif" style="height:30px;width:30px;" alt="">
                </div>
              </div>
              <div class="col-md-2">
                <button type="submit" name="import" class="btn btn-primary" id="submit">Import</button>
              </div>
            </div>
            
            <div class="row" style="margin-left:5%">
           
            <h6><br><br>NB : Format excel harus sesuai dengan format template yang sudah di sediakan</h6>

              <div class="col-md-2">
              </div>       
            </div>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancel</button>
        </div>
      </div>
    </div>
  </div>
  </form>

  <!-- Bootstrap core JavaScript-->
  <script src="<?= base_url()?>asset/vendor/jquery/jquery.min.js"></script>
  <script src="<?= base_url()?>asset/vendor/bootstrap/js/bootstrap.bundle.min.js"></script>

  <!-- Core plugin JavaScript-->
  <script src="<?= base_url()?>asset/vendor/jquery-easing/jquery.easing.min.js"></script>

  <!-- Custom scripts for all pages-->
  <script src="<?= base_url()?>asset/js/sb-admin-2.min.js"></script>
  <!-- Page level plugins -->
  <script src="<?= base_url()?>asset/vendor/datatables/jquery.dataTables.min.js"></script>
  <script src="<?= base_url()?>asset/vendor/datatables/dataTables.bootstrap4.min.js"></script>

  <!-- Page level custom scripts -->
  <script src="<?= base_url()?>asset/js/demo/datatables-demo.js"></script>
            
      <script type="text/javascript">
            $(document).ready(function() {
                $('#odp-table').DataTable({
                    
                    "ajax": {
                        url : "<?php echo site_url("aom/table/odp_page") ?>",
                        type : 'GET'
                    },
                });
            });
            $(".custom-file-input").on("change", function() {
            var fileName = $(this).val().split("\\").pop();
            $(this).siblings(".custom-file-label").addClass("selected").html(fileName);
          });
      </script>


</body>

</html>



