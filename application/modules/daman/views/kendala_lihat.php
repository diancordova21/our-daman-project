<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <meta name="description" content="">
  <meta name="author" content="">

  <title>Data Rinci Kendala ODP</title>

  <!-- Custom fonts for this template-->
  <link href="<?= base_url()?>asset/vendor/fontawesome-free/css/all.min.css" rel="stylesheet" type="text/css">
  <link href="<?= base_url()?>https://fonts.googleapis.com/css?family=Nunito:200,200i,300,300i,400,400i,600,600i,700,700i,800,800i,900,900i" rel="stylesheet">
  <link rel="icon" href="<?= base_url()?>asset/img/icon-telkom.ico" type="image/x-icon">

  <!-- Custom styles for this template-->
  <link href="<?= base_url()?>asset/css/sb-admin-2.min.css" rel="stylesheet">

</head>

<body id="page-top">

  <!-- Page Wrapper -->
  <div id="wrapper">
    <?php include_once("partials/sidebar.php") ?>
    <!-- Content Wrapper -->
    <div id="content-wrapper" class="d-flex flex-column">

      <!-- Main Content -->
      <div id="content">
        <?php include_once("partials/topbar.php") ?>

        <!-- Begin Page Content -->
        <div class="container-fluid">

        <!-- DataTales Example -->
        <div class="card shadow mb-4">
            <div class="card-header py-3">
              <h3 class="m-0 font-weight-bold text-primary">Kendala: <?php echo $port->trouble; ?></h3>
            </div>
            <div class="card-body">
              <div class="table-responsive">
              <table id="helo" class="ui table stripe" style="border-radius: 0px;">
                  <thead>
                    <tr>
                      <th>ODP</th>                      
                      <th>No Port</th>
                      <th>Status</th>
                      <th>No Inet / Voice</th>
                      <th>QR Code</th>
                      <th>Keterangan</th>
                      <th>Update UIM</th>
                      <th>Input Dava</th>
                      <th>Port UIM</th>
                      <th>DC Dismantled</th>
                      <th>Kendala</th>
                      <th>Aksi</th>
                    </tr>
                  </thead>
                    <tbody>
                        <?php foreach (@$trouble as $data) : ?>
                            <tr>
                                <td><?= $data->odp_name ?></td>
                                <td><?= $data->no_port ?></td>
                                <td><?= $data->status ?></td>
                                <td><?= $data->inet_voice_customer ?></td>
                                <td><?= $data->qr_code ?></td>
                                <td><?= $data->description ?></td>
                                <td><?= $data->update_uim ?></td>
                                <td><?= $data->input_dava ?></td>
                                <td><?= $data->port_uim ?></td>
                                <td><?= $data->dc_dismantled ?></td>
                                <td><?= $data->trouble ?></td>
                                <td>
                                <div class="ui buttons mini">
                                    <a class="btn btn-sm btn-warning" href="<?= site_url('daman/table/update/' . $data->id_revitalisasi_odp) ?>" title="Update"><i class="glyphicon glyphicon-folder"></i> Update</a>
                                    <a href="<?= site_url('table/delete/' . $data->id_port) ?>" class="btn btn-sm btn-danger" onclick="return confirm(\'Anda yakin untuk menghapus data ini?\');">Hapus</a>
                                </div>
                                </td>
                            </tr>
                        <?php endforeach; ?>
                    </tbody>
                </table>
              </div>
            </div>
          </div>
          
          
        </div>


      </div>
      <!-- End of Main Content -->

    </div>
    <!-- End of Content Wrapper -->

  </div>
  <!-- End of Page Wrapper -->

  <!-- Scroll to Top Button-->
  <a class="scroll-to-top rounded" href="<?= base_url()?>#page-top">
    <i class="fas fa-angle-up"></i>
  </a>

  <!-- Logout Modal-->
  <div class="modal fade" id="logoutModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title" id="exampleModalLabel">Ready to Leave?</h5>
          <button class="close" type="button" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">×</span>
          </button>
        </div>
        <div class="modal-body">Select "Logout" below if you are ready to end your current session.</div>
        <div class="modal-footer">
          <button class="btn btn-secondary" type="button" data-dismiss="modal">Cancel</button>
          <a class="btn btn-danger" href="<?= site_url('auth/admin/logout') ?>" class="item">Logout</a>
        </div>
      </div>
    </div>
  </div>


  <!-- Bootstrap core JavaScript-->
  <script src="<?= base_url()?>asset/vendor/jquery/jquery.min.js"></script>
  <script src="<?= base_url()?>asset/vendor/bootstrap/js/bootstrap.bundle.min.js"></script>

  <!-- Core plugin JavaScript-->
  <script src="<?= base_url()?>asset/vendor/jquery-easing/jquery.easing.min.js"></script>

  <!-- Custom scripts for all pages-->
  <script src="<?= base_url()?>asset/js/sb-admin-2.min.js"></script>
  <!-- Page level plugins -->
  <script src="<?= base_url()?>asset/vendor/datatables/jquery.dataTables.min.js"></script>
  <script src="<?= base_url()?>asset/vendor/datatables/dataTables.bootstrap4.min.js"></script>

  <!-- Page level custom scripts -->
  <script src="<?= base_url()?>asset/js/demo/datatables-demo.js"></script>
            
      <script type="text/javascript">
          $(document).ready(function() {
              $('#helo').DataTable();
          });
      </script>

</body>

</html>