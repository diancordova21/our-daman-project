<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">
    <title>Data Kabel Distribusi</title>
    <!-- Custom fonts for this template-->
    <link href="<?= base_url()?>asset/vendor/fontawesome-free/css/all.min.css" rel="stylesheet" type="text/css">
    <link href="<?= base_url()?>https://fonts.googleapis.com/css?family=Nunito:200,200i,300,300i,400,400i,600,600i,700,700i,800,800i,900,900i" rel="stylesheet">
    <link rel="icon" href="<?= base_url()?>asset/img/icon-telkom.ico" type="image/x-icon">
    <!-- Custom styles for this template-->
    <link href="<?= base_url()?>asset/css/sb-admin-2.min.css" rel="stylesheet">
  </head>
  <body id="page-top">
    <!-- Page Wrapper -->
    <div id="wrapper">
      <!-- Content Wrapper -->
      <div id="content-wrapper" class="d-flex flex-column">
        <!-- Main Content -->
        <div id="content">
          <?php include_once("partials/topbar.php") ?>
          <!-- Begin Page Content -->
          <div class="container-fluid">
            <!-- DataTales Example -->
            <div class="card shadow mb-4">
              <div class="card-header py-3">
                <div class="row">
                  <div class="col-lg-6 col-md-6">
                    <h3 class="m-0 font-weight-bold text-primary">Data Kabel Distribusi</h3>
                  </div>
                  <div class="col-lg-6 col-md-6">
                    <button type="button" class="btn btn-primary btn-user" style="margin-left:75%" data-toggle="modal" data-target="#exampleModalLong"><i class="fa fa-plus" ></i> Tambah</button>
                    <!-- <a class="btn btn-sm btn-primary btn-left" style="margin-left:75%"  href="table/create" title="Lihat" data-target="#exampleModalLong"><i class="fa fa-plus" aria-hidden="true"></i>  Add New</a> -->
                  </div>
                </div>
              </div>
              <div class="card-body">
                <div class="table-responsive">
                  <table id="helo" class="table table-bordered table-striped table-hover" style="width:100%">
                    <thead>
                      <tr>
                        <th>Dari ODC</th>
                        <th>Nama Kabel Distribusi</th>
                        <th>Kapasitas Core</th>
                        <th>Aksi</th>
                      </tr>
                    </thead>
                    <tbody>
                      <?php foreach ($distribusi as $data) : ?>
                      <tr>
                        <td><a href="<?= base_url('conectivity/odc/lihat/'.$data->fk_odc)?>"><?= $data->nama_odc ?></a></td>
                        <td><?= $data->nama_kabel_distribusi ?></td>
                        <td><?= $data->kap_core ?></td>
                        <td>
                          <div class="ui buttons mini">
                            <!-- <a class="btn btn-sm btn-success" href="<?= site_url('Conectivity/distribusi/lihat/' . $data->id_distribusi) ?>" title="Lihat"><i class="fa fa-eye"></i> Lihat</a> -->
                            <a class="btn btn-sm btn-warning" href="<?= site_url('Conectivity/distribusi/update/' . $data->id_distribusi) ?>" title="Lihat"><i class="fa fa-pen"></i> Update</a>
                            <a href="<?= site_url('Conectivity/distribusi/delete/' . $data->id_distribusi) ?>" class="btn btn-sm btn-danger" onclick="return confirm(\'Anda yakin untuk menghapus data ini?\');"><i class="fa fa-trash"></i> Hapus</a>
                          </div>
                        </td>
                      </tr>
                      <?php endforeach; ?>
                    </tbody>
                  </table>
                </div>
              </div>
            </div>
          </div>
        </div>
        <!-- End of Main Content -->
      </div>
      <!-- End of Content Wrapper -->
    </div>
    <!-- End of Page Wrapper -->
    <!-- Scroll to Top Button-->
    <a class="scroll-to-top rounded" href="<?= base_url()?>#page-top">
    <i class="fas fa-angle-up"></i>
    </a>
<!-- Modal Create  -->
<form method="post" action="<?= site_url('conectivity/distribusi/create') ?>" enctype="multipart/form-data">
  <div class="row">
    <!-- Modal -->
    <div class="modal fade" id="exampleModalLong" tabindex="-1" role="dialog" aria-labelledby="exampleModalLongTitle" aria-hidden="true">
      <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
          <div class="modal-header">
            <h5 class="modal-title" id="exampleModalLongTitle">Tambah Kabel Distribusi </h5>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
            </button>
          </div>
              <div class="modal-body">
                <div class="form-group row">
                  <div class="col-sm-10 mb-3 mb-sm-0">
                    <h6>Dari ODC : </h6>
                    <select class="ui fluid search dropdown" name="fk_odc">
                      <option value="0">-- Pilih ODC--</option>
                      <?php foreach ($odc as $list) : ?>
                      <option value="<?= $list->id_odc ?>"><?= $list->nama_odc ?></option>
                      <?php endforeach ?>
                    </select>
                  </div>
                </div>
                <div class="form-group row">
                  <div class="col-sm-10 mb-3 mb-sm-0">
                    <h6>Nama Kabel Distribusi: </h6>
                    <input type="text" class="form-control form-control-user" name="nama_kabel_distribusi" >
                  </div>
                </div>
                <div class="form-group row">
                  <div class="col-sm-10 mb-3 mb-sm-0">
                    <h6>Kapasitas Core: </h6>
                    <input type="text" class="form-control form-control-user" name="kap_core"  >
                  </div>
                </div>
                
                <div class="form-group row">
                  <div class="col-sm-10 mb-3 mb-sm-0">
                    <button name="submit" type="submit" class="btn btn-success btn-user">Submit</button>
                  </div>
                </div>
              </div>
        </div>
      </div>
    </div>   
  </div>
</form>
<!-- End Modal -->


    
    <!-- Logout Modal-->
    <div class="modal fade" id="logoutModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
      <div class="modal-dialog" role="document">
        <div class="modal-content">
          <div class="modal-header">
            <h5 class="modal-title" id="exampleModalLabel">Ready to Leave?</h5>
            <button class="close" type="button" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">×</span>
            </button>
          </div>
          <div class="modal-body">Select "Logout" below if you are ready to end your current session.</div>
          <div class="modal-footer">
            <button class="btn btn-secondary" type="button" data-dismiss="modal">Cancel</button>
            <a class="btn btn-danger" href="<?= site_url('auth/admin/logout') ?>" class="item">Logout</a>
          </div>
        </div>
      </div>
    </div>
    <!-- Bootstrap core JavaScript-->
    <script src="<?= base_url()?>asset/vendor/jquery/jquery.min.js"></script>
    <script src="<?= base_url()?>asset/vendor/bootstrap/js/bootstrap.bundle.min.js"></script>
    <!-- Core plugin JavaScript-->
    <script src="<?= base_url()?>asset/vendor/jquery-easing/jquery.easing.min.js"></script>
    <!-- Custom scripts for all pages-->
    <script src="<?= base_url()?>asset/js/sb-admin-2.min.js"></script>
    <!-- Page level plugins -->
    <script src="<?= base_url()?>asset/vendor/datatables/jquery.dataTables.min.js"></script>
    <script src="<?= base_url()?>asset/vendor/datatables/dataTables.bootstrap4.min.js"></script>
    <!-- Page level custom scripts -->
    <script src="<?= base_url()?>asset/js/demo/datatables-demo.js"></script>
    <script type="text/javascript">
      $(document).ready(function() {
          $('#helo').DataTable();
      });
    </script>
  </body>
</html>