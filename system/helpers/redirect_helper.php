<?php

if (!defined('BASEPATH')) exit('No direct script access allowed');

if (!function_exists('redirectIfAuthenticated')) {
    function redirectIfAuthenticated()
    {
        if (@$_SESSION['logged_in'] == TRUE) {
            if ($_SESSION['logged_in']['role'] == 'daman') {
                redirect('daman/dashboard');
            } else if ($_SESSION['logged_in']['role'] == 'aom') {
                redirect('aom/dashboard');
        }
    }
}

if (!function_exists('authenticated')) {
    function authenticated()
    {
        if (@$_SESSION == TRUE) {
            if (@$_SESSION['logged_in']) {
                return true;
            } else {
                redirect('auth/admin');
            }
        } else {
            redirect('auth/admin');
        }
    }
}

if (!function_exists('sessionAsDaman')) {
    function sessionAsDaman()
    {
        if (@$_SESSION['logged_in']['role'] == 'daman') {
            return true;
        } else {
            $ci =& get_instance();
            $ci->session->unset_userdata('logged_in');
            $ci->session->sess_destroy();
            redirect('_admin');
        }
    }
}

if (!function_exists('sessionAsAOM')) {
    function sessionAsAOM()
    {
        if (@$_SESSION['logged_in']['role'] == 'aom') {
            return true;
        } else {
            $ci =& get_instance();
            $ci->session->unset_userdata('logged_in');
            $ci->session->sess_destroy();
            redirect('_admin');
        }
    }
}

if (!function_exists('sessionAsRivara')) {
    function sessionAsRivara()
    {
        if (@$_SESSION['logged_in']['role'] == 'rivara') {
            return true;
        } else {
            $ci =& get_instance();
            $ci->session->unset_userdata('logged_in');
            $ci->session->sess_destroy();
            redirect('_admin');
        }
    }
}}