<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Bundlecore extends Modular {

	public function __construct(){
        authenticated();
        sessionAsRivara();
		parent::__construct();
		$this->load->library('form_validation'); 
		$this->load->model('Conectivity_models');
	}
	  
	public function index()
	{	
		$this->load->helper('url');
		$data['all_olt']=$this->Conectivity_models->get_olt();
		$data['all_bundlecore']=$this->Conectivity_models->get_bundlecore();
		// print_r($data);exit;
		$this->load->view('views_bundlecore', $data);
	}

	function create(){

		$this->form_validation->set_rules('fk_olt','Nama OLT','required');
		$this->form_validation->set_rules('nama_bundlecore','Nama Bundlecore','required');

        if (isset($_POST['submit'])) {
			if ($this->form_validation->run() != false){
				$this->Conectivity_models->create_bundlecore();
		// print_r("yayya");exit();

				redirect('conectivity/bundlecore');
			}			
			else{
				redirect('conectivity/bundlecore');
			}
		} 
		else {
			redirect('conectivity/bundlecore');
		}
	}
	
	public function lihat($id)
	{
		$data['details_olt']=$this->Conectivity_models->get_olt_detail($id);
		// print_r($data);exit();
		$this->load->view('views_olt_lihat', $data);
		
	}

	//Update
	function update($id = null){
		$Conectivity_models = $this->Conectivity_models;
		if (isset($_POST['submit'])){
			$this->Conectivity_models->UpdateByIdBundlecore($id);
			redirect('conectivity/bundlecore');
		}
	 	else {
		if (!isset($id)) redirect('conectivity/bundlecore');

		$data['all_olt']=$this->Conectivity_models->get_olt();
		$data['details_bundlecore']=$this->Conectivity_models->get_bundlecore_detail($id);

		// print_r($data);exit;
		$this->load->view('views_bundlecore_update', $data);
	 }
	}

	public function delete($id = null)
	{
		if (!isset($id)) show_404();

	    if ($this->Conectivity_models->hapus_bundlecore($id)) {
		    redirect(site_url('conectivity/bundlecore/index'));
	    }
	}

}
