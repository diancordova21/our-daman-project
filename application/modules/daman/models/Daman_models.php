<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Daman_models extends CI_Model{

    private $t_revitalisasi_odp = "t_revitalisasi_odp";
    private $t_port_odp= "t_port_odp";
    private $t_master_odp = "t_master_odp"; 

    public function __construct()
	{
	    parent::__construct();
	    $this->load->database();
	}


    public function get_data()
    {
        $this->db->select('t_revitalisasi_odp.id_revitalisasi_odp,t_revitalisasi_odp.odp_name,t_master_odp.datel,t_master_odp.sto,SUM(if(t_port_odp.status = "ON", 1, 0)) as total_on,SUM(if(t_port_odp.status="OFF",1,0)) as total_off,COUNT(t_port_odp.id_port) as total');
        $this->db->join('t_master_odp',' t_revitalisasi_odp.odp_name=t_master_odp.odp');
        $this->db->join('t_port_odp',' t_revitalisasi_odp.id_revitalisasi_odp=t_port_odp.fk_odp');
        $this->db->group_by('t_revitalisasi_odp.id_revitalisasi_odp');

        $hasil = $this->db->get('t_revitalisasi_odp');
	    return $hasil->result();

    }

    function getByODP($id)
	{
	    return $this->db->get_where($this->t_revitalisasi_odp, ["id_revitalisasi_odp" => $id])->row();
    }

    function get_port($id)
	{
	    return $this->db->get_where($this->t_port_odp, ["fk_odp" => $id])->result();
    }

    function getById($id)
	{
        $this->db->select('*');
        $this->db->where('id_revitalisasi_odp =', $id);
        $hasil = $this->db->get('t_revitalisasi_odp');
	    return $hasil->result();
    }
    function getPortById($id)
	{
        $this->db->select('*');
        $this->db->where('fk_odp =', $id);
        $hasil = $this->db->get('t_port_odp');
	    return $hasil->result();
    }

    function create_odp()
    {
        $data = array
            (
                'date' => $this->input->post('date'),
                'odp_name' => $this->input->post('odp_name'),
                'ip_address' => $this->input->post('ip_address'),
                'slot_port' => $this->input->post('slot_port')
            );
            // print_r($data);exit();
            $this->db->insert('t_revitalisasi_odp',$data);  
	    return $this->db->get_where($this->t_revitalisasi_odp, ["odp_name" => $this->input->post('odp_name')])->row();


    }

    function create_port()
    {
        $data = array
            (
                'fk_odp' => $this->input->post('id_odp'),
                'no_port' => $this->input->post('no_port'),
                'status' => $this->input->post('status'),
                'ownership' => $this->input->post('ownership'),
                'inet_voice_customer' => $this->input->post('inet_voice_customer'),
                'qr_code' => $this->input->post('qr_code'),
                'description' => $this->input->post('description'),
                'update_uim' => $this->input->post('update_uim'),
                'input_dava' => $this->input->post('input_dava'),
                'port_uim' => $this->input->post('port_uim'),
                'dc_dismantled' => $this->input->post('dc_dismantled'),
                'trouble' => $this->input->post('trouble')
            );
            // print_r($data);exit();
            $this->db->insert('t_port_odp',$data);            
    }

    
    public function UpdateTableById($fk)
    {
        for ($n=0; $n< COUNT($this->input->post('status')); $n++) {
            
            $data = array
                (
                    'status' => $this->input->post('status')[$n],
                    'inet_voice_customer' => $this->input->post('inet_voice_customer')[$n],
                    'update_uim' => $this->input->post('update_uim')[$n],
                    'ownership' => $this->input->post('ownership')[$n],
                    'input_dava' => $this->input->post('input_dava')[$n],
                    'port_uim' => $this->input->post('port_uim')[$n],
                    'dc_dismantled' => $this->input->post('dc_dismantled')[$n],
                    'trouble' => $this->input->post('trouble')[$n],
                    'description' => $this->input->post('description')[$n],
                    'qr_code' => $this->input->post('qr_code')[$n],
                );
                    $this->db->where('id_port',$this->input->post('id_port')[$n]);
                    $this->db->update('t_port_odp',$data);
                    // print_r($data);exit(); 
                    // $data = array();
               }
            //    return 0;    

    }
    // function UpdateById($id)
    // {
    //     $data = array
    //         (
    //             'date' => $this->input->post('date'),
    //             'odp_name' => $this->input->post('odp_name'),
    //             'ip_address' => $this->input->post('ip_address'),
    //             'slot_port' => $this->input->post('slot_port'),
    //             'port_revitalisasi_odp' => $this->input->post('port_revitalisasi_odp'),
    //             'status' => $this->input->post('status'),
    //             'inet_voice_customer' => $this->input->post('inet_voice_customer'),
    //             'qr_code' => $this->input->post('qr_code'),
    //             'description' => $this->input->post('description'),
    //             'update_uim' => $this->input->post('update_uim'),
    //             'input_dava' => $this->input->post('input_dava'),
    //             'port_uim' => $this->input->post('port_uim'),
    //             'dc_dismantled' => $this->input->post('dc_dismantled'),
    //             'trouble' => $this->input->post('trouble')
    //         );
    //         // print_r($data);exit();
    //             $this->db->where('id_revitalisasi',$id);
    //             $this->db->update('t_revitalisasi',$data);
            
    // }

    function hapus($id)
	{
		$hasil = $this->db->delete($this->table, array("id_revitalisasi" => $id));
		return $hasil;
    }
    
    function getBySTO($sto)
	{
       $this->db->select('t_revitalisasi_odp.id_revitalisasi_odp,t_revitalisasi_odp.odp_name,t_master_odp.qrcode_odp,t_master_odp.total,t_revitalisasi_odp.ip_address');
       $this->db->join('t_master_odp',' t_revitalisasi_odp.odp_name=t_master_odp.odp');
       $this->db->where('sto =', $sto);
       $this->db->group_by('odp');
       $hasil = $this->db->get('t_revitalisasi_odp');
	    return $hasil->result();
    }

    function ambil_sto($sto)
	{
        $this->db->where('sto', $sto);
        $hasil = $this->db->get('t_master_odp');
        return $hasil->row();        
    }

    function ambil_trouble()
	{
        // $this->db->where('trouble');
        $hasil = $this->db->get('t_port_odp');
        return $hasil->row();      
        // print_r($hasil->row());exit();

    }

    function ambil_disconnect()
	{
        $trouble = 'Disconnect';
        $this->db->join('t_port_odp','t_port_odp.fk_odp = t_revitalisasi_odp.id_revitalisasi_odp');
        $this->db->where('trouble', $trouble);
        $hasil = $this->db->get('t_revitalisasi_odp');
        // print_r($hasil->result());exit();
        // print_r($hasil);exit();

        return $hasil->result();      
        // print_r($hasil->result());exit();

    }

    function ambil_redti()
	{
        $trouble = 'Redaman Tinggi';
        $this->db->join('t_port_odp','t_port_odp.fk_odp = t_revitalisasi_odp.id_revitalisasi_odp');
        $this->db->where('trouble', $trouble);
        $hasil = $this->db->get('t_revitalisasi_odp');

        return $hasil->result();      
        // print_r($hasil->result());exit();

    }

    function ambil_stuckbi()
	{
        $trouble = 'Service Stuck BI';
        $this->db->join('t_port_odp','t_port_odp.fk_odp = t_revitalisasi_odp.id_revitalisasi_odp');
        $this->db->where('trouble', $trouble);
        $hasil = $this->db->get('t_revitalisasi_odp');

        return $hasil->result();      
        // print_r($hasil->result());exit();

    }

    function ambil_notdetek()
	{
        $trouble = 'Tidak Detek';
        $this->db->join('t_port_odp','t_port_odp.fk_odp = t_revitalisasi_odp.id_revitalisasi_odp');
        $this->db->where('trouble', $trouble);
        $hasil = $this->db->get('t_revitalisasi_odp');

        return $hasil->result();      
        // print_r($hasil->result());exit();

    }

    function ambil_splitter()
	{
        $trouble = 'Splitter';
        $this->db->join('t_port_odp','t_port_odp.fk_odp = t_revitalisasi_odp.id_revitalisasi_odp');
        $this->db->where('trouble', $trouble);
        $hasil = $this->db->get('t_revitalisasi_odp');

        return $hasil->result();      
        // print_r($hasil->result());exit();

    }

    function ambil_cantcreate()
	{
        $trouble = 'Tidak Bisa Create New Version';
        $this->db->join('t_port_odp','t_port_odp.fk_odp = t_revitalisasi_odp.id_revitalisasi_odp');
        $this->db->where('trouble', $trouble);
        $hasil = $this->db->get('t_revitalisasi_odp');

        return $hasil->result();      
        // print_r($hasil->result());exit();

    }

    function ambil_suspend()
	{
        $trouble = 'Service Suspend';
        $this->db->join('t_port_odp','t_port_odp.fk_odp = t_revitalisasi_odp.id_revitalisasi_odp');
        $this->db->where('trouble', $trouble);
        $hasil = $this->db->get('t_revitalisasi_odp');

        return $hasil->result();      
        // print_r($hasil->result());exit();

    }

    function ambil_canceled()
	{
        $trouble = 'Service Canceled';
        $this->db->join('t_port_odp','t_port_odp.fk_odp = t_revitalisasi_odp.id_revitalisasi_odp');
        $this->db->where('trouble', $trouble);
        $hasil = $this->db->get('t_revitalisasi_odp');

        return $hasil->result();      
        // print_r($hasil->result());exit();

    }

    function ambil_noservice()
	{
        $trouble = 'ONT Detect No Service';
        $this->db->join('t_port_odp','t_port_odp.fk_odp = t_revitalisasi_odp.id_revitalisasi_odp');
        $this->db->where('trouble', $trouble);
        $hasil = $this->db->get('t_revitalisasi_odp');

        return $hasil->result();      
        // print_r($hasil->result());exit();

    }

    function ambil_susahjangkau()
	{
        $trouble = 'ODP Susah Di Jangkau';
        $this->db->join('t_port_odp','t_port_odp.fk_odp = t_revitalisasi_odp.id_revitalisasi_odp');
        $this->db->where('trouble', $trouble);
        $hasil = $this->db->get('t_revitalisasi_odp');

        return $hasil->result();      
        // print_r($hasil->result());exit();

    }

    function ambil_kapasitas()
	{
        $trouble = 'Kapasitas';
        $this->db->join('t_port_odp','t_port_odp.fk_odp = t_revitalisasi_odp.id_revitalisasi_odp');
        $this->db->where('trouble', $trouble);
        $hasil = $this->db->get('t_revitalisasi_odp');

        return $hasil->result();      
        // print_r($hasil->result());exit();

    }

    function jumlah_data()
	{
        $query = $this->db->get('t_port_odp');
        return $query->num_rows();
        
    }

    
    function jumlah_input_dava()
	{
        $status = 'DONE';
        $this->db->select('input_dava');
        $this->db->where('input_dava =',$status);
        // $this->db->group_by('odp_name'); 
        // $this->db->order_by('total', 'desc'); 
        $hasil = $this->db->get('t_port_odp');
        return $hasil->num_rows();;
    }
    function jumlah_disconnect()
	{
        $status = 'Disconnect';
        $this->db->select('trouble');
        $this->db->where('trouble =',$status);
        // $this->db->order_by('total', 'desc'); 
        $hasil = $this->db->get('t_port_odp');
        return  $hasil->num_rows();
    }
    function jumlah_redaman_tinggi()
	{
        $status = 'Redaman Tinggi';
        $this->db->select('trouble');
        $this->db->where('trouble =',$status);
        // $this->db->order_by('total', 'desc'); 
        $hasil = $this->db->get('t_port_odp');
        return  $hasil->num_rows();
    }
    function jumlah_stuck_bi()
	{
        $status = 'Service Stuck BI';
        $this->db->select('trouble');
        $this->db->where('trouble =',$status);
        // $this->db->order_by('total', 'desc'); 
        $hasil = $this->db->get('t_port_odp');
        return  $hasil->num_rows();
    }
    function jumlah_tidak_detek()
	{
        $status = 'Tidak Detek Full';
        $status1 = 'Tidak Detek Sebagian';
        $this->db->select('trouble');
        $this->db->where('trouble =',$status);
        $this->db->where('trouble =',$status1);
        // $this->db->order_by('total', 'desc'); 
        $hasil = $this->db->get('t_port_odp');
        return  $hasil->num_rows();
    }

    function jumlah_splitter()
	{
        $status = 'Splitter';
        $this->db->select('trouble');
        $this->db->where('trouble =',$status);
        // $this->db->order_by('total', 'desc'); 
        $hasil = $this->db->get('t_port_odp');
        return  $hasil->num_rows();
    }

    function jumlah_kapasitas()
	{
        $status = 'Kapasitas';
        $this->db->select('trouble');
        $this->db->where('trouble =',$status);
        // $this->db->order_by('total', 'desc'); 
        $hasil = $this->db->get('t_port_odp');
        return  $hasil->num_rows();
    }

    function jumlah_tidak_bisa_create_new_version()
	{
        $status = 'Tidak Bisa Create New Version';
        $this->db->select('trouble');
        $this->db->where('trouble =',$status);
        // $this->db->order_by('total', 'desc'); 
        $hasil = $this->db->get('t_port_odp');
        return  $hasil->num_rows();
    }
    function jumlah_service_suspend()
	{
        $status = 'Service Suspend';
        $this->db->select('trouble');
        $this->db->where('trouble =',$status);
        // $this->db->order_by('total', 'desc'); 
        $hasil = $this->db->get('t_port_odp');
        return  $hasil->num_rows();
    }
    function jumlah_service_canceled()
	{
        $status = 'Service Canceled';
        $this->db->select('trouble');
        $this->db->where('trouble =',$status);
        // $this->db->order_by('total', 'desc'); 
        $hasil = $this->db->get('t_port_odp');
        return  $hasil->num_rows();
    }
    function jumlah_ont_detect_no_service()
	{
        $status = 'ONT Detect No Service';
        $this->db->select('trouble');
        $this->db->where('trouble =',$status);
        // $this->db->order_by('total', 'desc'); 
        $hasil = $this->db->get('t_port_odp');
        return  $hasil->num_rows();
    }

    function jumlah_odp_susah_di_jangkau()
	{
        $status = 'ODP Susah Di Jangkau';
        $this->db->select('trouble');
        $this->db->where('trouble =',$status);
        // $this->db->order_by('total', 'desc'); 
        $hasil = $this->db->get('t_port_odp');
        return  $hasil->num_rows();
    }

    
    
    function jumlah_odp_penuh()
	{
        $status = 'ODP Penuh';
        $this->db->select('trouble');
        $this->db->where('trouble =',$status);
        // $this->db->order_by('total', 'desc'); 
        $hasil = $this->db->get('t_port_odp');
        return  $hasil->num_rows();
    }
    function jumlah_node_b()
	{
        $status = 'Node B';
        $this->db->select('odp_name');
        $this->db->where('trouble =',$status);
        $hasil = $this->db->get('t_port_odp');
        return  $hasil->num_rows();
    }
    function jumlah_ccan()
	{
        $status = 'CCAN';
        $this->db->select('trouble');
        $this->db->where('trouble =',$status);
        // $this->db->order_by('total', 'desc'); 
        $hasil = $this->db->get('t_port_odp');
        return  $hasil->num_rows();
    }
    function jumlah_wifi_id()
	{
        $status = 'WIFI ID';
        $this->db->select('trouble');
        $this->db->where('trouble =',$status);
        // $this->db->order_by('total', 'desc'); 
        $hasil = $this->db->get('t_port_odp');
        return  $hasil->num_rows();
    }
    function jumlah_astinet()
	{
        $status = 'ASTINET';
        $this->db->select('trouble');
        $this->db->where('trouble =',$status);
        // $this->db->order_by('total', 'desc'); 
        $hasil = $this->db->get('t_port_odp');
        return  $hasil->num_rows();
    }
    function jumlah_vpn()
	{
        $status = 'VPN';
        $this->db->select('trouble');
        $this->db->where('trouble =',$status);
        // $this->db->order_by('total', 'desc'); 
        $hasil = $this->db->get('t_port_odp');
        return  $hasil->num_rows();
    }
    
    function jumlah_odp_persto()
    {
        $this->db->select('odp_name, COUNT(*) as jumlah_odp');
        $this->db->group_by('odp_name');
        // $this->db->order_by('jumlah_odp'); 
        // 
        $query = $this->db->get('t_port_odp');
        // print_r($query ->result());exit();
        
        return $query->result();


       

    }
    function jumlah_sto()
	{
        $this->db->select('trouble, COUNT(trouble) as total');
        $this->db->group_by('trouble'); 
        $this->db->order_by('total', 'desc'); 
        $hasil = $this->db->get('t_port_odp');
        return $hasil->num_rows();
    }
    
    function jumlah_update_uim()
	{
        $status = 'DONE';
        $this->db->select(' update_uim');
        $this->db->where('update_uim =', $status);
        // $this->db->group_by('update_uim'); 
        // $this->db->order_by('total', 'desc'); 
        $hasil = $this->db->get('t_port_odp');
        return $hasil->num_rows();
    }
    
    function jumlah_lawang()
	{
        $nama = 'LWG';
        $this->db->select('COUNT(t_master_odp.id_odp)');
        $this->db->join('t_master_odp',' t_revitalisasi_odp.odp_name=t_master_odp.odp');
        $this->db->where('sto =', $nama);
        $this->db->group_by('odp');
        $hasil = $this->db->get('t_revitalisasi_odp');
        return $hasil->num_rows();
    }

    function jumlah_singosari()
	{
        $nama = 'SGS';
        $this->db->select('COUNT(t_master_odp.id_odp)');
        $this->db->join('t_master_odp',' t_revitalisasi_odp.odp_name=t_master_odp.odp');
        $this->db->where('sto =', $nama);
        $this->db->group_by('odp');
        $hasil = $this->db->get('t_revitalisasi_odp');
        return $hasil->num_rows();
    }

    function jumlah_pakis()
	{
        $nama = 'PKS';
         $this->db->select('COUNT(t_master_odp.id_odp)');
        $this->db->join('t_master_odp',' t_revitalisasi_odp.odp_name=t_master_odp.odp');
        $this->db->where('sto =', $nama);
        $this->db->group_by('odp');
        $hasil = $this->db->get('t_revitalisasi_odp');
        return $hasil->num_rows();
    }

    function jumlah_sawojajar()
	{
        $nama = 'SWJ';
         $this->db->select('COUNT(t_master_odp.id_odp)');
        $this->db->join('t_master_odp',' t_revitalisasi_odp.odp_name=t_master_odp.odp');
        $this->db->where('sto =', $nama);
        $this->db->group_by('odp');
        $hasil = $this->db->get('t_revitalisasi_odp');
        return $hasil->num_rows();
    }

    function jumlah_tumpang()
	{
        $nama = 'TMP';
         $this->db->select('COUNT(t_master_odp.id_odp)');
        $this->db->join('t_master_odp',' t_revitalisasi_odp.odp_name=t_master_odp.odp');
        $this->db->where('sto =', $nama);
        $this->db->group_by('odp');
        $hasil = $this->db->get('t_revitalisasi_odp');
        return $hasil->num_rows();
    }

    function jumlah_blimbing()
	{
        $nama = 'BLB';
         $this->db->select('COUNT(t_master_odp.id_odp)');
        $this->db->join('t_master_odp',' t_revitalisasi_odp.odp_name=t_master_odp.odp');
        $this->db->where('sto =', $nama);
        $this->db->group_by('odp');
        $hasil = $this->db->get('t_revitalisasi_odp');
        return $hasil->num_rows();
    }

    function jumlah_klojen()
	{
        $nama = 'KLJ';
         $this->db->select('COUNT(t_master_odp.id_odp)');
        $this->db->join('t_master_odp',' t_revitalisasi_odp.odp_name=t_master_odp.odp');
        $this->db->where('sto =', $nama);
        $this->db->group_by('odp');
        $hasil = $this->db->get('t_revitalisasi_odp');
        return $hasil->num_rows();
    }

    function jumlah_malangkota()
	{
        $nama = 'MLG';
         $this->db->select('COUNT(t_master_odp.id_odp)');
        $this->db->join('t_master_odp',' t_revitalisasi_odp.odp_name=t_master_odp.odp');
        $this->db->where('sto =', $nama);
        $this->db->group_by('odp');
        $hasil = $this->db->get('t_revitalisasi_odp');
        return $hasil->num_rows();
    }

    function jumlah_gadang()
	{
        $nama = 'GDG';
         $this->db->select('COUNT(t_master_odp.id_odp)');
        $this->db->join('t_master_odp',' t_revitalisasi_odp.odp_name=t_master_odp.odp');
        $this->db->where('sto =', $nama);
        $this->db->group_by('odp');
        $hasil = $this->db->get('t_revitalisasi_odp');
        return $hasil->num_rows();
    }

    function jumlah_buring()
	{
        $nama = 'BRG';
         $this->db->select('COUNT(t_master_odp.id_odp)');
        $this->db->join('t_master_odp',' t_revitalisasi_odp.odp_name=t_master_odp.odp');
        $this->db->where('sto =', $nama);
        $this->db->group_by('odp');
        $hasil = $this->db->get('t_revitalisasi_odp');
        return $hasil->num_rows();
    }

    function jumlah_dampit()
	{
        $nama = 'DPT';
         $this->db->select('COUNT(t_master_odp.id_odp)');
        $this->db->join('t_master_odp',' t_revitalisasi_odp.odp_name=t_master_odp.odp');
        $this->db->where('sto =', $nama);
        $this->db->group_by('odp');
        $hasil = $this->db->get('t_revitalisasi_odp');
        return $hasil->num_rows();
    }

    function jumlah_kepanjen()
	{
        $nama = 'KEP';
         $this->db->select('COUNT(t_master_odp.id_odp)');
        $this->db->join('t_master_odp',' t_revitalisasi_odp.odp_name=t_master_odp.odp');
        $this->db->where('sto =', $nama);
        $this->db->group_by('odp');
        $hasil = $this->db->get('t_revitalisasi_odp');
        return $hasil->num_rows();
    }

    function jumlah_turen()
	{
        $nama = 'TUR';
         $this->db->select('COUNT(t_master_odp.id_odp)');
        $this->db->join('t_master_odp',' t_revitalisasi_odp.odp_name=t_master_odp.odp');
        $this->db->where('sto =', $nama);
        $this->db->group_by('odp');
        $hasil = $this->db->get('t_revitalisasi_odp');
        return $hasil->num_rows();
    }

    function jumlah_gondanglegi()
	{
        $nama = 'GDI';
         $this->db->select('COUNT(t_master_odp.id_odp)');
        $this->db->join('t_master_odp',' t_revitalisasi_odp.odp_name=t_master_odp.odp');
        $this->db->where('sto =', $nama);
        $this->db->group_by('odp');
        $hasil = $this->db->get('t_revitalisasi_odp');
        return $hasil->num_rows();
    }

    function jumlah_sumbermanjing()
	{
        $nama = 'SBM';
        $this->db->select('COUNT(t_master_odp.id_odp)');
        $this->db->join('t_master_odp',' t_revitalisasi_odp.odp_name=t_master_odp.odp');
        $this->db->where('sto =', $nama);
        $this->db->group_by('odp');
        $hasil = $this->db->get('t_revitalisasi_odp');
        return $hasil->num_rows();
    }

    function jumlah_sumberpucung()
	{
        $nama = 'SBP';
        $this->db->select('COUNT(t_master_odp.id_odp)');
        $this->db->join('t_master_odp',' t_revitalisasi_odp.odp_name=t_master_odp.odp');
        $this->db->where('sto =', $nama);
        $this->db->group_by('odp');
        $hasil = $this->db->get('t_revitalisasi_odp');
        return $hasil->num_rows();
    }

    function jumlah_pagak()
	{
        $nama = 'PGK';
        $this->db->select('COUNT(t_master_odp.id_odp)');
        $this->db->join('t_master_odp',' t_revitalisasi_odp.odp_name=t_master_odp.odp');
        $this->db->where('sto =', $nama);
        $this->db->group_by('odp');
        $hasil = $this->db->get('t_revitalisasi_odp');
        return $hasil->num_rows();
    }
    
    function jumlah_ampelgading()
	{
        $nama = 'APG';
        $this->db->select('COUNT(t_master_odp.id_odp)');
        $this->db->join('t_master_odp',' t_revitalisasi_odp.odp_name=t_master_odp.odp');
        $this->db->where('sto =', $nama);
        $this->db->group_by('odp');
        $hasil = $this->db->get('t_revitalisasi_odp');
        return $hasil->num_rows();
    }

    function jumlah_gunungkawi()
	{
        $nama = 'GKW';
        $this->db->select('COUNT(t_master_odp.id_odp)');
        $this->db->join('t_master_odp',' t_revitalisasi_odp.odp_name=t_master_odp.odp');
        $this->db->where('sto =', $nama);
        $this->db->group_by('odp');
        $hasil = $this->db->get('t_revitalisasi_odp');
        return $hasil->num_rows();
    }

    function jumlah_bantur()
	{
        $nama = 'BNR';
        $this->db->select('COUNT(t_master_odp.id_odp)');
        $this->db->join('t_master_odp',' t_revitalisasi_odp.odp_name=t_master_odp.odp');
        $this->db->where('sto =', $nama);
        $this->db->group_by('odp');
        $hasil = $this->db->get('t_revitalisasi_odp');
        return $hasil->num_rows();
    }

    function jumlah_donomulyo()
	{
        $nama = 'DNO';
        $this->db->select('COUNT(t_master_odp.id_odp)');
        $this->db->join('t_master_odp',' t_revitalisasi_odp.odp_name=t_master_odp.odp');
        $this->db->where('sto =', $nama);
        $this->db->group_by('odp');
        $hasil = $this->db->get('t_revitalisasi_odp');
        return $hasil->num_rows();
    }

    function jumlah_karangploso()
	{
        $nama = 'KPO';
        $this->db->select('COUNT(t_master_odp.id_odp)');
        $this->db->join('t_master_odp',' t_revitalisasi_odp.odp_name=t_master_odp.odp');
        $this->db->where('sto =', $nama);
        $this->db->group_by('odp');
        $hasil = $this->db->get('t_revitalisasi_odp');
        return $hasil->num_rows();
    }

    function jumlah_batu()
	{
        $nama = 'BTU';
        $this->db->select('COUNT(t_master_odp.id_odp)');
        $this->db->join('t_master_odp',' t_revitalisasi_odp.odp_name=t_master_odp.odp');
        $this->db->where('sto =', $nama);
        $this->db->group_by('odp');
        $hasil = $this->db->get('t_revitalisasi_odp');
        return $hasil->num_rows();
    }

    function jumlah_ngantang()
	{
        $nama = 'NTG';
        $this->db->select('COUNT(t_master_odp.id_odp)');
        $this->db->join('t_master_odp',' t_revitalisasi_odp.odp_name=t_master_odp.odp');
        $this->db->where('sto =', $nama);
        $this->db->group_by('odp');
        $hasil = $this->db->get('t_revitalisasi_odp');
        return $hasil->num_rows();
    }

    public function pie_chart_malang() {
        $malang = 'malang';
        $this->db->select('id_odp, datel, sto, COUNT(odp) as total');
        $this->db->from('t_master_odp');
        $this->db->where('datel =', $malang);
        $this->db->group_by('sto'); 
        $this->db->order_by('total', 'asc'); 
        
   
        $hasil = $query->result();

        $data = [];
        
        foreach($hasil as $row) {
              $data['label'][] = $row->sto;
              $data['data'][] = (int) $row->total;
        }

        return $hasil;
    }
    
    // check if odp already existed
    public function checkOdp($odp) {
        $this->db->select('id_odp');
        $this->db->from('t_odp');
        $this->db->where('odp_name', $odp);
        $query = $this->db->get();
        
        if ($query->num_rows() > 0) {
            return $query->row()->id_odp;
        } else {
            return false;
        }
    }

    // check odp port
    public function checkOdpPort($odp, $port, $data) {
        $this->db->select('id_port');
        $this->db->from('t_port_odp');
        $this->db->where('fk_odp', $odp);
        $query = $this->db->get();

        if ($query->num_rows() > 0) {
            return $query->row()->id_port;
        } else {
            return false;
        }
    }

    // import odp
    public function insertOdp($data){
        $this->db->insert('t_odp', $data);

        $insert_id = $this->db->insert_id();

        return  $insert_id;
    }

    //import odp port
    public function insertPort($data){
        $this->db->insert('t_port_odp', $data);
    }

    //import odp port
    public function updatePort($id, $data){
        $this->db->where('id_port',$id);
        $this->db->update('t_port_odp',$data);
    }
}
