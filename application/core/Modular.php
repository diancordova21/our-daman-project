<?php

if (!defined('BASEPATH')) exit('No direct script access allowed');

/* load the MX_Router class */
require APPPATH . "third_party/MX/Controller.php";

class Modular extends MX_Controller
{

    function __construct()
    {
        parent::__construct();

        date_default_timezone_set('Asia/Bangkok');

        if (version_compare(CI_VERSION, '2.1.0', '<')) {
            $this->load->library('security');
        }
    }

    protected function do_upload($field, $path, $type = '', $encrypt = false, $inject_config = [])
    {
        $config['upload_path'] = $path;
        $config['encrypt_name'] = $encrypt;
        switch ($type) {
            case 'image':
                $config['allowed_types'] = 'jpg|jpeg|png';
                break;
            case 'document':
                $config['allowed_types'] = 'docx|pdf';
                break;
            case 'video':
                $config['allowed_types'] = '3gp|mp4|mkv';
                break;
            case 'music':
                $config['allowed_types'] = 'mp3';
        }
        foreach ($inject_config as $key => $value) {
            $config[$key] = $value;
        }
        $this->load->library('upload', $config);
        $uploaded_filename = "";
        if (!$this->upload->do_upload($field)) {
            // $validation = $this->validator->make([], []);
            // $validation->errors()->add($field, strip_tags($this->upload->display_errors()));
            // $this->session->set_flashdata('errors', $validation->errors());
            // $this->session->set_flashdata('old', $this->input->post());
            //redirect('files/images', 'refresh');
            $error = array('error' => $this->upload->display_errors());
            print_r($error);
        } else {
            $upload_data = $this->upload->data();
            $uploaded_filename = $upload_data['file_name'];
        }
        return $uploaded_filename;
    }
}
