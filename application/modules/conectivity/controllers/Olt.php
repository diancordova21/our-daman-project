<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Olt extends Modular {

	public function __construct(){
        authenticated();
        sessionAsRivara();
		parent::__construct();
		$this->load->library('form_validation'); 
		$this->load->model('Conectivity_models');
	}
	  
	public function index()
	{	
        $this->load->helper('url');
		$data['all_olt']=$this->Conectivity_models->get_olt();
		$this->load->view('views_olt', $data);
	}

	function create(){


		$this->form_validation->set_rules('nama_olt','Nama OLT','required');
		// $this->form_validation->set_rules('odp_name','ODP','required|is_unique[t_odp.odp_name]');
		$this->form_validation->set_rules('ruangan_olt','Ruangan OLT','required');
		$this->form_validation->set_rules('ip_olt','IP Address OLT','required');
		$this->form_validation->set_rules('subrack_cart_port','Subrack/Cart/Port','required');

        if (isset($_POST['submit'])) {
			if ($this->form_validation->run() != false){
				$data["odp"] = $this->Conectivity_models->create_olt();
				redirect('conectivity/olt');
			}			
			else{
				redirect('conectivity/olt');
			}
		} 
		else {
			redirect('conectivity/olt');
		}
	}
	
	public function lihat($id)
	{
		$data['details_olt']=$this->Conectivity_models->get_olt_detail($id);
		// print_r($data);exit();
		$this->load->view('views_olt_lihat', $data);
		
	}

	//Update
	function update($id = null){
		$Conectivity_models = $this->Conectivity_models;
		if (isset($_POST['submit'])){
			$this->Conectivity_models->UpdateById($id);
			redirect('conectivity/olt');
		}
	 	else {
		if (!isset($id)) redirect('conectivity/olt');
	
		$data['details_olt']=$this->Conectivity_models->get_olt_detail($id);
		$this->load->view('views_olt_update', $data);
	 }
	}

	public function delete($id = null)
	{
		if (!isset($id)) show_404();

	    if ($this->Conectivity_models->hapus($id)) {
		    redirect(site_url('conectivity/olt/index'));
	    }
	}


	//Create


	//Tambah Port
	function tambah_port($id = null){
		// print_r($this->input->post('id_odp'));exit();
		// print_r($id);exit();
		if (!isset($id)) redirect('table/index');
		$Daman_models = $this->Daman_models;
		$data["odp"] = $Daman_models->getByODP($id);
		$data["port"] = $Daman_models->get_port($id);
		// print_r($data);exit();
		// $this->load->view('tables_create_port', $data);
		
		if (isset($_POST['submit'])) {
			// print_r($this->input->post('id_odp'));exit();
			$this->Daman_models->create_port();
			redirect('daman/table/lihat/'.$data['odp']->id_revitalisasi_odp);
		} else {
			// $this->load->view("tables_create");
		$this->load->view('tables_create_port', $data);

		}
        // if (isset($_POST['submit'])) {
		// 	$this->Daman_models->create_port();
		// 	redirect('table/tambah_port');
		// } else {
		// 	$this->load->view("tables_create_port", $data);
		// }
	}

	//Update
	function update_table($fk){
		if (isset($_POST['submit'])){
				// print_r($this->input->post('nama_odp'));exit();
				// print_r($this->input->post('status'));exit();
				// print_r($this->input->post('id_port'));exit();
				$Daman_models = $this->Daman_models;

                $this->Daman_models->UpdateTableById($fk);
				redirect('daman/table/update/'.$fk);

			}
         else {
			if (!isset($fk)) redirect('daman/table/update'.$fk);

        }
	
	}



	


	public function view(){
		$search = $_POST['search']['value']; // Ambil data yang di ketik user pada textbox pencarian
		$limit = $_POST['length']; // Ambil data limit per page
		$start = $_POST['start']; // Ambil data start
		$order_index = $_POST['order'][0]['column']; // Untuk mengambil index yg menjadi acuan untuk sorting
		$order_field = $_POST['columns'][$order_index]['data']; // Untuk mengambil nama field yg menjadi acuan untuk sorting
		$order_ascdesc = $_POST['order'][0]['dir']; // Untuk menentukan order by "ASC" atau "DESC"
		$sql_total = $this->Daman_models->count_all(); // Panggil fungsi count_all pada SiswaModel
		$sql_data = $this->Daman_models->filter($search, $limit, $start, $order_field, $order_ascdesc); // Panggil fungsi filter pada SiswaModel
		$sql_filter = $this->Daman_models->count_filter($search); // Panggil fungsi count_filter pada SiswaModel
		$callback = array(
			'draw'=>$_POST['draw'], // Ini dari datatablenya
			'recordsTotal'=>$sql_total,
			'recordsFiltered'=>$sql_filter,
			'data'=>$sql_data
		);
		header('Content-Type: application/json');
		echo json_encode($callback); // Convert array $callback ke json
	}

	public function import() {
        $data = array();
		$this->form_validation->set_rules('fileURL', 'Upload File', 'callback_checkFileValidation');
		
        // If file uploaded
		if(!empty($_FILES['fileURL']['name'])) { 
			// get file extension
			$extension = pathinfo($_FILES['fileURL']['name'], PATHINFO_EXTENSION);
			
			if($extension == 'csv'){
				$reader = new \PhpOffice\PhpSpreadsheet\Reader\Csv();
			} elseif($extension == 'xlsx') {
				$reader = new \PhpOffice\PhpSpreadsheet\Reader\Xlsx();
			} else {
				$reader = new \PhpOffice\PhpSpreadsheet\Reader\Xls();
			}
			// file path
			$spreadsheet = $reader->load($_FILES['fileURL']['tmp_name']);
			$allDataInSheet = $spreadsheet->getActiveSheet()->toArray(null, true, true, true);
		
			// array Count
			$arrayCount = count($allDataInSheet);
			
			$flag = 0;
			$createArray = array(
							'Tanggal', 'NAMA_ODP', 'IP', 'SLOT_PORT', 'PORT_ODP', 
							'STATUS', 'ownership', 'NO_INET', 'QR_CODE_DROPCORE', 'Keterangan', 
							'Update_UIM', 'Input_DAVA', 'Port_UIM', 'DC_Dismantled', 'Trouble'
			);
			$makeArray = array(
				'Tanggal'     	=> 'Tanggal',
				'NAMA_ODP'  => 'NAMA_ODP',
				'IP'  => 'IP',
				'SLOT_PORT' => 'SLOT_PORT',
				'PORT_ODP'    => 'PORT_ODP',
				'STATUS'      => 'STATUS',
				'ownership'		=> 'ownership',
				'NO_INET'  => 'NO_INET',
				'QR_CODE_DROPCORE'=> 'QR_CODE_DROPCORE',
				'Keterangan'     => 'Keterangan',
				'Update_UIM' => 'Update_UIM',
				'Input_DAVA'     => 'Input_DAVA',
				'Port_UIM'     => 'Port_UIM',
				'DC_Dismantled' => 'DC_Dismantled',
				'Trouble' => 'Trouble'             
				);
			
			$SheetDataKey = array();
			foreach ($allDataInSheet as $dataInSheet) {
				foreach ($dataInSheet as $key => $value) {
					if (in_array(trim($value), $createArray)) {
						$value = preg_replace('/\s+/', '', $value); 
						$SheetDataKey[trim($value)] = $key;
					} 
				}
				
			}
			
			$dataDiff = array_diff_key($makeArray, $SheetDataKey);
			
			if (empty($dataDiff)) {
				$flag = 1;
			}

			$date = str_replace('/', '-', $SheetDataKey['Tanggal']);
			
			// match excel sheet column
			if ($flag == 1) {
				for ($i = 2; $i <= $arrayCount; $i++) {
					$date = $SheetDataKey['Tanggal'];
					$odp_name = $SheetDataKey['NAMA_ODP'];
					$ip_address = $SheetDataKey['IP'];
					$slot_port = $SheetDataKey['SLOT_PORT'];
					$no_port = $SheetDataKey['PORT_ODP'];
					$status = $SheetDataKey['STATUS'];
					$ownership = $SheetDataKey['ownership'];
					$inet_voice_customer = $SheetDataKey['NO_INET'];
					$qr_code = $SheetDataKey['QR_CODE_DROPCORE'];
					$description = $SheetDataKey['Keterangan'];
					$update_uim = $SheetDataKey['Update_UIM'];
					$input_dava = $SheetDataKey['Input_DAVA'];
					$port_uim = $SheetDataKey['Port_UIM'];
					$dc_dismantled = $SheetDataKey['DC_Dismantled'];
					$trouble = $SheetDataKey['Trouble'];
					
					$date = filter_var(trim($allDataInSheet[$i][$date]), FILTER_SANITIZE_STRING);
					$odp_name = filter_var(trim($allDataInSheet[$i][$odp_name]), FILTER_SANITIZE_STRING);
					$ip_address = filter_var(trim($allDataInSheet[$i][$ip_address]), FILTER_SANITIZE_STRING);
					$slot_port = filter_var(trim($allDataInSheet[$i][$slot_port]), FILTER_SANITIZE_STRING);
					$no_port = filter_var(trim($allDataInSheet[$i][$no_port]), FILTER_SANITIZE_NUMBER_INT);
					$status = filter_var(trim($allDataInSheet[$i][$status]), FILTER_SANITIZE_STRING);
					$ownership = filter_var(trim($allDataInSheet[$i][$ownership]), FILTER_SANITIZE_STRING);
					$inet_voice_customer = filter_var(trim($allDataInSheet[$i][$inet_voice_customer]), FILTER_SANITIZE_STRING);
					$qr_code = filter_var(trim($allDataInSheet[$i][$qr_code]), FILTER_SANITIZE_STRING);
					$description = filter_var(trim($allDataInSheet[$i][$description]), FILTER_SANITIZE_STRING);
					$update_uim = filter_var(trim($allDataInSheet[$i][$update_uim]), FILTER_SANITIZE_STRING);
					$input_dava = filter_var(trim($allDataInSheet[$i][$input_dava]), FILTER_SANITIZE_STRING);
					$port_uim = filter_var(trim($allDataInSheet[$i][$port_uim]), FILTER_SANITIZE_STRING);
					$dc_dismantled = filter_var(trim($allDataInSheet[$i][$dc_dismantled]), FILTER_SANITIZE_STRING);
					$trouble = filter_var(trim($allDataInSheet[$i][$trouble]), FILTER_SANITIZE_STRING);
					
					$new_date = (new DateTime($date))->format('Y-m-d');

					$fetchDataODP = array(
						'date' => $new_date,
						'odp_name' => $odp_name,
						'ip_address' => $ip_address,
						'slot_port' => $slot_port
					);
					
					$checkOdp = $this->Daman_models->checkOdp($odp_name);

					$fetchDataPort = array(
						'fk_odp' => $checkOdp,
						'no_port' => $no_port,
						'status' => $status,
						'ownership' => $ownership,
						'inet_voice_customer' => $inet_voice_customer,
						'qr_code' => $qr_code,
						'description' => $description,
						'update_uim' => $update_uim,
						'input_dava' => $input_dava,
						'port_uim' => $port_uim,
						'dc_dismantled' => $dc_dismantled,
						'trouble' => $trouble
					);
					
					if($checkOdp){							
						$this->Daman_models->insertPort($fetchDataPort);
					}else{
						$id_odp = $this->Daman_models->insertOdp($fetchDataODP);

						$fetchDataPort['fk_odp'] = $id_odp;
						$this->Daman_models->insertPort($fetchDataPort);
					}
				}
				echo "Berhasil";
			} else {
				echo "Please import correct file, did not match excel sheet column";
			}
		}
    }
 
    // checkFileValidation
    public function checkFileValidation($string) {
      $file_mimes = array('text/x-comma-separated-values', 
        'text/comma-separated-values', 
        'application/octet-stream', 
        'application/vnd.ms-excel', 
        'application/x-csv', 
        'text/x-csv', 
        'text/csv', 
        'application/csv', 
        'application/excel', 
        'application/vnd.msexcel', 
        'text/plain', 
        'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet'
      );
      if(isset($_FILES['fileURL']['name'])) {
            $arr_file = explode('.', $_FILES['fileURL']['name']);
            $extension = end($arr_file);
            if(($extension == 'xlsx' || $extension == 'xls' || $extension == 'csv') && in_array($_FILES['fileURL']['type'], $file_mimes)){
                return true;
            }else{
                $this->form_validation->set_message('checkFileValidation', 'Please choose correct file.');
                return false;
            }
        }else{
            $this->form_validation->set_message('checkFileValidation', 'Please choose a file.');
            return false;
        }
    }
}