<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Kepanjen extends Modular {

	public function __construct(){
		authenticated();
        sessionAsDaman();
		parent::__construct();
		$this->load->model('Daman_models');
	}
	
	public function index()
	{	
		$this->load->helper('url');
		$Daman_models = $this->Daman_models;
		$data["total_dampit"] = $Daman_models->jumlah_dampit();
		$data["total_kepanjen"] = $Daman_models->jumlah_kepanjen();
		$data["total_turen"] = $Daman_models->jumlah_turen();
		$data["total_gondanglegi"] = $Daman_models->jumlah_gondanglegi();
		$data["total_sumbermanjing"] = $Daman_models->jumlah_sumbermanjing();
		$data["total_sumberpucung"] = $Daman_models->jumlah_sumberpucung();
		$data["total_pagak"] = $Daman_models->jumlah_pagak();
		$data["total_ampelgading"] = $Daman_models->jumlah_ampelgading();
		$data["total_gunungkawi"] = $Daman_models->jumlah_gunungkawi();
		$data["total_bantur"] = $Daman_models->jumlah_bantur();
		$data["total_donomulyo"] = $Daman_models->jumlah_donomulyo();
		

		$this->load->view('datel_kepanjen', $data);
	}

	function sto($sto = null){

		$data["odp"] = $this->Daman_models->getBySTO($sto);
		$data["sto"] = $this->Daman_models->ambil_sto($sto);		

		$this->load->view('sto_lihat', $data);
	}
}