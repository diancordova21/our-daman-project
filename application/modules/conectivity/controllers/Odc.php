<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Odc extends Modular
{
    public function __construct(){
        authenticated();
        sessionAsRivara();
		parent::__construct();
		$this->load->library('form_validation'); 
		$this->load->model('Conectivity_models');
    }

    public function index()
	{	
        $this->load->helper('url');
        $data['all_feeder'] =$this->Conectivity_models->get_feeder();
		$data['all_odc'] =$this->Conectivity_models->get_odc();
		// print_r($data['all_odc']);exit;
		$this->load->view('views_odc', $data);
    }

    function create(){
		$this->form_validation->set_rules('fk_feeder','Nama Feeder','required');
        $this->form_validation->set_rules('nama_odc','Nama ODC','required');
        $this->form_validation->set_rules('nama_location_odc','Lokasi','required');
        $this->form_validation->set_rules('nama_frame_odc','Frame','required');
        $this->form_validation->set_rules('panel_in_panel','Panel In Panel','required');
        $this->form_validation->set_rules('panel_in_port','Panel In Port','required');
        $this->form_validation->set_rules('panel_in_core','Panel In Core','required');
        $this->form_validation->set_rules('splitter_name','Splitter','required');
        $this->form_validation->set_rules('splitter_out','Splitter Out','required');
        $this->form_validation->set_rules('panel_out_panel','Panel Out Panel','required');
        $this->form_validation->set_rules('panel_out_port','Panel Out Port','required');
        $this->form_validation->set_rules('panel_out_core','Panel Out Core','required');
        $this->form_validation->set_rules('kap','Kap','required');
        $this->form_validation->set_rules('kordinat_long','Longitude','required');
        $this->form_validation->set_rules('kordinat_lang','Latitude','required');
        
        if (isset($_POST['submit'])) {
			if ($this->form_validation->run() != false){
				$this->Conectivity_models->create_odc();
				redirect('conectivity/odc');
			}			
			else{
				$this->Conectivity_models->create_odc();
				redirect('conectivity/odc');
			}
		} 
		else {
			$this->Conectivity_models->create_odc();
			redirect('conectivity/odc');
		}
    }
    
    public function lihat($id)
	{
		$data['details_odc']=$this->Conectivity_models->get_odc_detail($id);
		$this->load->view('views_odc_lihat', $data);
    }
    
    //Update
	function update($id = null){
		if (isset($_POST['submit'])){
			$this->Conectivity_models->UpdateByIdOdc($id);
			redirect('conectivity/odc');
		}
	 	else {
		if (!isset($id)) redirect('conectivity/odc');
		
		$data['details_odc']=$this->Conectivity_models->get_odc_detail($id);
		$this->load->view('views_odc_update', $data);
	 	}
    }
    
    public function delete($id = null)
	{
		if (!isset($id)) show_404();

	    if ($this->Conectivity_models->hapus_odc($id)) {
		    redirect(site_url('conectivity/odc'));
	    }
	}
}